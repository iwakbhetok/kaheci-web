-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 31, 2020 at 05:24 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `senyumu_kaheci_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

DROP TABLE IF EXISTS `about`;
CREATE TABLE IF NOT EXISTS `about` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `yt_video_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `txt_benefit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bg_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `description`, `yt_video_url`, `txt_benefit`, `bg_image`, `created_at`, `updated_at`) VALUES
(1, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 'https://www.youtube.com/embed/tgbNymZ7vqY', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` char(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `no_telp`, `created_at`, `updated_at`) VALUES
(1, 'Test', 'admin@demo.com', '$2y$10$Mnw/XLOlKibXLrMy7PVROeyNypHmUR5CRhAKtzkGCtLjxF1WFne96', '087829946140', '2019-10-12 01:32:59', '2019-10-12 01:32:59'),
(2, 'john joe', 'admin@admin.com', '$2y$10$4cDx69vaXVgjo8js4jjb1.h8nWlxM8ODnL2GK153G50en7gkMOwuK', '087829946140', '2019-10-13 20:09:21', '2019-10-13 20:09:21'),
(3, 'contoh', 'admin@admin2.com', '$2y$10$87d4C8fsMqkfi4XKFJav7epom2rtVZarK5gTuxGpnAby9.qB0FTgO', '087829946140', '2020-01-18 10:23:55', '2020-01-18 10:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '[]',
  `published` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `admin_id`, `blog_category_id`, `title`, `slug`, `image`, `description`, `tags`, `published`, `created_at`, `updated_at`) VALUES
(18, 2, 7, 'Tips Kecantikan Ala Kaheci', 'tips-kecantikan-ala-kaheci', '1580489920-IERMfK58keMsskNiYSQlyAUzQ8RHCcFW.jpg', '<p>Tips Kecantikan Ala Kaheci<br></p>\n', '[\"7\"]', '1', '2020-01-31 09:58:40', '2020-01-31 09:58:40'),
(19, 2, 8, 'Tips Diet Ala Millenials', 'tips-diet-ala-millenials', '1580490923-Af7ltCmvRvjta4GHvRD5IaW8ZaWESW9M.jpg', '<p>Tips Diet Ala Millenials<br></p>\n', '[\"7\",\"8\"]', '1', '2020-01-31 10:15:23', '2020-01-31 10:15:23'),
(20, 2, 7, 'Kecantikan Naturan Ala Kaheci', 'kecantikan-naturan-ala-kaheci', '1580491098-9yxE0hDJ9aRZER6WRNFzDlfd4IKfDIUE.jpg', '<p>Kecantikan Naturan Ala Kaheci<br></p>\n', '[\"7\"]', '1', '2020-01-31 10:18:18', '2020-01-31 10:18:18');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

DROP TABLE IF EXISTS `blog_categories`;
CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `slug`, `description`, `status`, `created_at`, `updated_at`) VALUES
(7, 'Kecantikan', 'kecantikan', 'Ini adalah kecantikan', '1', '2020-01-31 05:16:25', '2020-01-31 05:16:25'),
(8, 'Tips', 'tips', 'tips', '1', '2020-01-31 10:14:23', '2020-01-31 10:14:23');

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

DROP TABLE IF EXISTS `blog_tags`;
CREATE TABLE IF NOT EXISTS `blog_tags` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_tags`
--

INSERT INTO `blog_tags` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(7, 'Kecantikan', 'kecantikan', '1', '2020-01-31 09:58:18', '2020-01-31 09:58:18'),
(8, 'Diet', 'diet', '1', '2020-01-31 10:14:35', '2020-01-31 10:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('new','read','remove','spam','sent','draft') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `subject`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Jhon Doe', 'jhon@email.com', 'Berikut ini judul pesan', 'Berikut ini keterangan pesan', 'new', '2019-10-14 00:38:48', '2019-10-14 00:38:48'),
(2, 'Namaku', 'alamat@email.com', 'judul pesan saya adalah', 'isi pesan saya adalah', 'new', '2019-11-17 07:18:28', '2019-11-17 07:18:28'),
(3, 'Handsock', 'sularso.moch@gmail.com', 'Tanya', 'Hai', 'new', '2020-01-28 12:44:14', '2020-01-28 12:44:14'),
(4, 'Handsock', 'sularso.moch@gmail.com', 'Tanya', 'tes', 'new', '2020-01-28 12:45:44', '2020-01-28 12:45:44');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2019_10_04_221311_create_messages_table', 1),
(7, '2019_10_05_005146_create_product_categories_table', 1),
(8, '2019_10_05_005229_create_products_table', 1),
(9, '2019_10_05_005405_create_testimonials_table', 1),
(11, '2019_10_06_151104_create_blog_categories_table', 1),
(12, '2019_10_06_151748_create_blog_tags_table', 1),
(13, '2019_10_12_080142_create_admins_table', 1),
(14, '2019_11_05_024253_create_about_table', 2),
(15, '2019_10_06_151006_create_blogs_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `admin_id`, `name`, `slug`, `image`, `description`, `published`, `created_at`, `updated_at`) VALUES
(7, 2, 'Tentang Kami', 'tentang-kami', '1580486600-5PInUyfYhKwBtU3BcDPeog86NEFTo5dt.jpg', '<p style=\"text-align: justify; \"><span style=\"color: rgb(102, 102, 102); font-family: Roboto, sans-serif; background-color: rgb(255, 255, 255);\">Nama Kaheci sendiri diambil dari singkatan Kaos Kaki Henna Cantik, karena itu jadilah Kaheci menjadi pelopor kaos kaki motif Henna pertama di Indonesia</span><p style=\"text-align: justify; \"></p><div style=\"text-align: center;\"><img data-filename=\"logo.png\" style=\"color: rgb(0, 0, 0); font-size: 0.875rem; width: 152px;\" src=\"/images/pages/15804866000.png\"></div><br></p>\n', '1', '2020-01-31 09:03:20', '2020-01-31 09:03:20');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sku_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity_per_unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `unit_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `available_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `available_colors` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `unit_weight` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `unit_in_stock` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status_product_available` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `product_type` enum('new','sale','available','premium','coming soon') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `status` enum('new','read','remove','spam') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `no_telp_cs` char(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku_number`, `name`, `slug`, `description`, `main_image`, `image_1`, `image_2`, `image_3`, `image_4`, `thumbnail`, `category_id`, `quantity_per_unit`, `unit_price`, `available_size`, `available_colors`, `size`, `color`, `discount`, `unit_weight`, `unit_in_stock`, `status_product_available`, `product_type`, `status`, `no_telp_cs`, `created_at`, `updated_at`) VALUES
(19, '123handsock', 'Handsock', 'handsock', '$category = ProductCategory::where(\'status\',\'active\')->get()->toArray();\r\n        $tree = buildTree($category);', '1580181682-handsock.jpg', '1-handsock.jpg', NULL, NULL, NULL, 'thumb-handsock.jpg', '39', '0', '10000', '[\"xs\"]', '[\"#e66465\"]', NULL, NULL, '0', '0', '0', '1', 'sale', 'remove', '087829946140', '2020-01-27 20:21:22', '2020-01-30 14:40:45'),
(20, '123brukat', 'Brukat', 'brukat', 'ini adalah deskripsi', '1580420437-brukat.jpg', '1-brukat.jpg', NULL, NULL, NULL, 'thumb-brukat.jpg', '40', '0', '50000', '[\"xs\"]', '[\"#e66465\"]', NULL, NULL, '10', '0', '0', '1', 'new', 'new', '087829946140', '2020-01-30 14:40:38', '2020-01-30 14:40:38'),
(21, '123amoera', 'Kerudung Amoera', 'kerudung-amoera', 'ini adalah amoera', '1580469727-kerudung-amoera.jpg', NULL, NULL, NULL, NULL, 'thumb-kerudung-amoera.jpg', '40', '0', '400000', '[\"xs\"]', '[\"#e66465\",\"#e66465\"]', NULL, NULL, '20', '0', '0', '1', 'new', 'new', '087829946140', '2020-01-31 04:22:08', '2020-01-31 04:41:11');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

DROP TABLE IF EXISTS `product_categories`;
CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive','remove') COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(2) DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `slug`, `description`, `status`, `parent_id`, `image`, `created_at`, `updated_at`) VALUES
(39, 'Kerudung', 'kerudung', 'Siimply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to ma', 'active', NULL, '1580151152-41iCicfy2EJYTHTGnMNy4X9zw2A3PJRX.jpg', '2020-01-27 11:52:32', '2020-01-31 09:05:07'),
(40, 'Segi Empat', 'segi-empat', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to ma', 'active', 39, '1580151152-41iCicfy2EJYTHTGnMNy4X9zw2A3PJRX.jpg', '2020-01-27 11:52:32', '2020-01-31 09:05:27'),
(41, 'Jilbab Syar\'i', 'jilbab-syari', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to ma', 'active', NULL, '1580151152-41iCicfy2EJYTHTGnMNy4X9zw2A3PJRX.jpg', '2020-01-27 11:52:32', '2020-01-31 09:06:10'),
(42, 'Atasan', 'atasan', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to ma', 'active', 42, '1580151152-41iCicfy2EJYTHTGnMNy4X9zw2A3PJRX.jpg', '2020-01-27 11:52:32', '2020-01-31 09:05:54');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `description`, `slug`, `image`, `status`, `created_at`, `updated_at`) VALUES
(15, 'Kerudungnya Bagus', 'Kerudungnya Bagus', 'kerudungnya-bagus', '1580486936-3p3Pjuaoz4wiF2l9j5CqCqP1e7XZavku.jpg', '1', '2020-01-31 09:08:56', '2020-01-31 09:08:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
