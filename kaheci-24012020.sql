-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 23, 2020 at 09:06 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `senyumu_kaheci_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

DROP TABLE IF EXISTS `about`;
CREATE TABLE IF NOT EXISTS `about` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `yt_video_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `txt_benefit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bg_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `description`, `yt_video_url`, `txt_benefit`, `bg_image`, `created_at`, `updated_at`) VALUES
(1, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 'https://www.youtube.com/embed/tgbNymZ7vqY', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` char(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `no_telp`, `created_at`, `updated_at`) VALUES
(1, 'Test', 'admin@demo.com', '$2y$10$Mnw/XLOlKibXLrMy7PVROeyNypHmUR5CRhAKtzkGCtLjxF1WFne96', '087829946140', '2019-10-12 01:32:59', '2019-10-12 01:32:59'),
(2, 'john joe', 'admin@admin.com', '$2y$10$4cDx69vaXVgjo8js4jjb1.h8nWlxM8ODnL2GK153G50en7gkMOwuK', '087829946140', '2019-10-13 20:09:21', '2019-10-13 20:09:21'),
(3, 'contoh', 'admin@admin2.com', '$2y$10$87d4C8fsMqkfi4XKFJav7epom2rtVZarK5gTuxGpnAby9.qB0FTgO', '087829946140', '2020-01-18 10:23:55', '2020-01-18 10:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '[]',
  `published` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

DROP TABLE IF EXISTS `blog_categories`;
CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive','remove') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `slug`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'kategori 1', 'kategori-1', 'ini adalah keterangan kategori', 'active', '2019-11-05 20:43:25', '2019-11-05 20:43:25');

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

DROP TABLE IF EXISTS `blog_tags`;
CREATE TABLE IF NOT EXISTS `blog_tags` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive','remove') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_tags`
--

INSERT INTO `blog_tags` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'jual beli', 'jual-beli', 'active', '2019-11-05 21:43:24', '2019-11-05 21:43:24');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('new','read','remove','spam','sent','draft') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `subject`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Jhon Doe', 'jhon@email.com', 'Berikut ini judul pesan', 'Berikut ini keterangan pesan', 'new', '2019-10-14 00:38:48', '2019-10-14 00:38:48'),
(2, 'Namaku', 'alamat@email.com', 'judul pesan saya adalah', 'isi pesan saya adalah', 'new', '2019-11-17 07:18:28', '2019-11-17 07:18:28');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2019_10_04_221311_create_messages_table', 1),
(7, '2019_10_05_005146_create_product_categories_table', 1),
(8, '2019_10_05_005229_create_products_table', 1),
(9, '2019_10_05_005405_create_testimonials_table', 1),
(11, '2019_10_06_151104_create_blog_categories_table', 1),
(12, '2019_10_06_151748_create_blog_tags_table', 1),
(13, '2019_10_12_080142_create_admins_table', 1),
(14, '2019_11_05_024253_create_about_table', 2),
(15, '2019_10_06_151006_create_blogs_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sku_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity_per_unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `unit_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `available_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `available_colors` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `unit_weight` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `unit_in_stock` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status_product_available` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `product_type` enum('new','sale','available','premium','coming soon') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `status` enum('new','read','remove','spam') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `no_telp_cs` char(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku_number`, `name`, `slug`, `description`, `main_image`, `image_1`, `image_2`, `image_3`, `image_4`, `thumbnail`, `category_id`, `quantity_per_unit`, `unit_price`, `available_size`, `available_colors`, `size`, `color`, `discount`, `unit_weight`, `unit_in_stock`, `status_product_available`, `product_type`, `status`, `no_telp_cs`, `created_at`, `updated_at`) VALUES
(2, 'SKU123', 'Kaheci Motekar', 'kaheci-motekar', 'Aisyah Child', '1579745799-kaheci-motekar.jpg', NULL, NULL, NULL, NULL, 'thumb-kaheci-motekar.jpg', '23', '0', '50', 'null', 'null', NULL, NULL, '0', '0', '0', '1', 'sale', 'remove', '087829946140', '2019-10-20 07:59:22', '2020-01-23 08:07:50'),
(3, '2313Handsock Aisyah', 'Handsock Aisyah', 'handsock-aisyah', 'Handsock Aisyah', '1579131899-handsock-aisyah.jpg', NULL, NULL, NULL, NULL, 'thumb-handsock-aisyah.jpg', '23', '0', '40000', '[\"xs\"]', '[\"#ffedd5\",\"#e66465\"]', NULL, NULL, '0', '0', '0', '1', 'new', 'new', '087829946140', '2020-01-15 16:44:59', '2020-01-22 10:51:40'),
(5, 'hkjhkjh', 'kjhkjh', 'kjhkjh', 'khkjhk', '1579374069-kjhkjh.jpg', NULL, NULL, NULL, NULL, 'thumb-kjhkjh.jpg', '23', '0', '7897', '[\"xs\",\"s\"]', '[\"#82c88a\"]', NULL, NULL, '0', '0', '0', '1', 'new', 'new', '087829946140', '2020-01-18 12:01:09', '2020-01-22 12:38:30'),
(6, '2313Handsock Aisyah4', 'Handsock Aisyah2', 'handsock-aisyah2', 'kjhk', '1579441247-handsock-aisyah2.jpg', NULL, NULL, NULL, NULL, 'thumb-handsock-aisyah2.jpg', '23', '0', '90', '[\"s\"]', 'null', NULL, NULL, '0', '0', '0', '1', 'new', 'new', '087829946140', '2020-01-19 06:40:47', '2020-01-22 12:36:57'),
(7, 'HJ123', 'Hijabers Arabian', 'hijabers-arabian', 'Ini adalah produk tersedia', '1579722587-hijabers-arabian.jpg', NULL, NULL, NULL, NULL, 'thumb-hijabers-arabian.jpg', '23', '0', '2000', '[\"s\",\"l\"]', 'null', NULL, NULL, '0', '0', '0', '1', 'available', 'new', '087829946140', '2020-01-22 12:49:47', '2020-01-22 12:49:47'),
(8, 'KB123', 'Kerudung Baghdad', 'kerudung-baghdad', 'ini adalah deskripsi kerudung baghdad', '1579723094-kerudung-baghdad.jpg', NULL, NULL, NULL, NULL, 'thumb-kerudung-baghdad.jpg', '23', '0', '2000', '[\"xs\"]', 'null', NULL, NULL, '0', '0', '0', '1', 'new', 'new', '087829946140', '2020-01-22 12:58:14', '2020-01-22 12:58:14'),
(9, 'KA123', 'Kerudung Arab', 'kerudung-arab', 'ini adalah deskripsi kerudung arab', '1579724029-kerudung-arab.jpg', '1-kerudung-arab.jpg', '2-kerudung-arab.jpg', NULL, NULL, 'thumb-kerudung-arab.jpg', '23', '0', '2000', '[\"s\"]', 'null', NULL, NULL, '0', '0', '0', '1', 'new', 'new', '087829946140', '2020-01-22 13:13:49', '2020-01-22 13:13:49'),
(10, 'KAS1234', 'Kerudung Arab Songkok', 'kerudung-arab-songkok', 'ini adalah deskripsi', '1579725717-kerudung-arab-songkok.jpg', '1-kerudung-arab-songkok.jpg', NULL, NULL, NULL, 'thumb-kerudung-arab-songkok.jpg', '27', '0', '19000000', '[\"xs\"]', '[\"#008000\",\"#e66465\"]', NULL, NULL, '60000', '0', '0', '1', 'new', 'new', '087829946140', '2020-01-22 13:41:57', '2020-01-22 13:41:57');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

DROP TABLE IF EXISTS `product_categories`;
CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive','remove') COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_category` int(2) NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `slug`, `description`, `status`, `parent_category`, `image`, `thumbnail`, `created_at`, `updated_at`) VALUES
(22, 'Aisyah', 'aisyah', 'Aisyah', 'active', 0, '1579804173-oNnuYExM4ZkaWbKRgOXHcNlvnswrHrGt.jpg', 'aisyah.jpg', '2020-01-18 12:19:07', '2020-01-23 11:48:36'),
(23, 'Aisyah Child', 'aisyah-child', 'Aisyah Child', 'active', 22, '1579677223-AMjkIAstBac5E4DstqnfQx1NSkErfYvG.jpg', 'aisyah-child.jpg', '2020-01-22 00:13:43', '2020-01-23 12:28:42'),
(24, 'Brukat', 'brukat', 'Brukat', 'active', 0, '1579799554-LW8qwtx1HnXAEl8OhoPxFRzFOru3UmUz.jpg', 'brukat.jpg', '2020-01-23 10:12:35', '2020-01-23 12:28:42'),
(26, 'Brukat Baghdad', 'brukat-baghdad', 'Brukat Arabian 2', 'active', 24, '1579801847-k4w4QUjqMuAl3vOxbAJDjDZoQjJOzNkA.jpg', 'brukat-arabian-2.jpg', '2020-01-23 10:50:47', '2020-01-23 13:55:26'),
(27, 'Brukat Baghdad 2', 'brukat-baghdad-2', 'Brukat Arabian', 'active', 24, '1579813065-4ds42vwM0uy7R9yNbVcRYymA4vAs6vmK.jpg', 'brukat-baghdad-2.jpg', '2020-01-23 10:50:47', '2020-01-23 13:57:46');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimoni_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('like','sent_package') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'like',
  `status` enum('active','inactive','remove') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
