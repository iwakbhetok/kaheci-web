<div id="modal-container" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <form>
            <div class="uk-margin uk-animation-fade">
                <div class="uk-inline uk-width-1-1">
                    <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: search"></a>
                    <input class="uk-input" type="text" placeholder="Aku ingin mencari . . .">
                </div>
            </div>

            <!--fieldset class="uk-fieldset">

                <div class="uk-margin">
                    <input class="uk-input" type="text" placeholder="Input">
                </div>

                <div class="uk-margin">
                    <button class="uk-button uk-button-primary">Primary</button>
                </div>

            </fieldset-->
        </form>
    </div>
</div>