

<div uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar" style="z-index:999;">
    <div class="uk-container uk-container-expand" style="background-color:#fff;">
 
        <nav class="uk-navbar-container" uk-navbar="dropbar: true;" style="position: relative; z-index: 980;background-color:#fff;">
            <div class="uk-navbar-left">
                <a href="{{ url('/') }}" class="uk-navbar-item uk-logo">
                    <img alt="Kaheci" src="{{ !empty(GlobalVar::show_logo_header()) ? asset('images/logo/'.GlobalVar::show_logo_header()) : asset('images/logo.png') }}" width="152" height="101" class="uk-visible@m" alt="Logo Header">
                    <img alt="Kaheci" src="{{ !empty(GlobalVar::show_logo_header()) ? asset('images/logo/'.GlobalVar::show_logo_header()) : asset('images/logo.png') }}" width="100" class="uk-hidden@m" alt="Logo Header Mobile">
                </a>
            </div>
            <div class="uk-navbar-right">
                <ul class="uk-navbar-nav uk-visible@m">
                    @if(GlobalVar::show_menu('header',null)->count() > 0)
                        @foreach(GlobalVar::show_menu('header',null) as $items)
                        <li class="{{ url()->current() == $items->url ? 'uk-active' : '' }}">
                            <a href="{{ url($items->url) }}" class=" menu-item menu-item-type-post_type menu-item-object-page">{{ $items->name }}</a>
                        </li>
                        @endforeach
                    @else
                        <li class="{{ $active['index'] }}">
                            <a href="{{ url('/') }}" class=" menu-item menu-item-type-post_type menu-item-object-page">Beranda</a>
                        </li>                    
                    @endif                    
                </ul>
                <div class="uk-navbar-item uk-visible@m">
                    
                </div>
                <a uk-navbar-toggle-icon="" href="#offcanvas" uk-toggle="" class="uk-navbar-toggle uk-hidden@m uk-icon uk-navbar-toggle-icon">
                </a>
            </div>
        </nav>
    </div>
</div>
<div id="offcanvas" uk-offcanvas="mode: push; overlay: true" class="uk-offcanvas uk-offcanvas-overlay">
    <div class="uk-offcanvas-bar uk-offcanvas-bar-animation uk-offcanvas-push">
        <div class="uk-panel">
            <ul class="uk-nav uk-nav-default">
                @if(GlobalVar::show_menu('header',null)->count() > 0)
                    @foreach(GlobalVar::show_menu('header',null) as $items)
                    <li class="{{ $active['contact'] }}">
                        <a href="{{ url($items->url) }}" class=" menu-item menu-item-type-post_type menu-item-object-page">{{ $items->name }}</a>
                    </li>
                    @endforeach
                @else
                    <li class="{{ $active['index'] }}">
                        <a href="{{ url('/') }}" class=" menu-item menu-item-type-post_type menu-item-object-page">Beranda</a>
                    </li>                    
                @endif                    
                <!-- <li class="uk-nav-header">General</li> -->
            </ul>
        </div>
    </div>
</div>