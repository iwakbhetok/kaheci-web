<div class="uk-section-default uk-section uk-padding-remove-vertical" uk-scrollspy="cls:uk-animation-fade">

    <div class="uk-grid-margin uk-grid uk-grid-stack" uk-grid="">
        <div class="uk-width-1-1@m uk-first-column uk-visible@m">
            <div class="uk-background-cover uk-height-medium " style="background-image: url({{ asset('images/banner-highlight-page.jpg')}});">
                <div class="uk-text-center uk-padding" uk-grid>
                    <div class="uk-width-expand@m">
                        <div class="uk-card uk-card-body">
                            <p class="uk-h4 uk-margin">{{ $pageName }}</p>
                            <ul class="uk-breadcrumb uk-navbar-center">
                                <li><a href="{{ url('/') }}">Beranda</a></li>
                                <li><span>{{ $breadcrumb }}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-1-1@m uk-first-column uk-hidden@m">
            <div class="uk-background-cover uk-height-small " style="background-image: url({{ asset('images/banner-highlight-page.jpg')}});">
                <div class="uk-text-center uk-padding" uk-grid>
                    <div class="uk-width-expand@m">
                        <div class="uk-card uk-card-body">
                            <p class="uk-h4 uk-margin">{{ $pageName }}</p>
                            <ul class="uk-breadcrumb uk-navbar-center">
                                <li><a href="{{ url('/') }}">Beranda</a></li>
                                <li><span>{{ $breadcrumb }}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>