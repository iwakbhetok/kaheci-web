@if(isset($blogs_related))
<section class="uk-padding-remove-vertical uk-section uk-section-default">
    <div class="uk-container">
        <h2 class="uk-text-bold">Artikel lainnya</h2>
        <div data-uk-slider="velocity: 5">
            <div class="uk-position-relative">
                <div class="uk-slider-container">
                    <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m uk-grid uk-grid-medium">
                        @foreach($blogs_related as $items)
                        <li>
                            <!-- card -->
                            <div class="uk-card">
                                <a href="{{ url('/blog/'.$items->blog_url) }}">
                                <div class="blog-category uk-position-left uk-flex uk-flex-middle">
                                    <span class="uk-position-center">{{ $items->cat_name }}</span>
                                </div>
                                </a>
                                <a href="{{ url('/blog/'.$items->blog_url) }}">
                                <img src="{{ asset('images/blogs/'.$items->blog_url.'/'.$items->blog_thumbs) }}" alt="{{ $items->blog_name }}">
                                </a>
                                <p class="uk-article-meta">Written by {{ $blogs->admin_name }} on {{ \Carbon\Carbon::parse($blogs->blog_created)->format('d M Y')}}. Posted in <a href="#">Blog</a> </p>
                                <a href="{{ url('/blog/'.$items->blog_url) }}" class="link-title-blog">
                                    <p class="title-blog">{{ $items->blog_name }}</p>
                                </a>
                                <p>{{str_limit(strip_tags(str_replace('&nbsp;', ' ', $items->blog_desc)),200,'...')}}</p>
                            </div>
                            <a href="{{ url('/blog/'.$items->blog_url) }}" title="Read More" class="uk-button uk-button-default uk-button-small">READ MORE</a>
                            <!-- /card -->
                        </li>
                        @endforeach
                    </ul>
                </div>
                <br>
                <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin">
                    
                </ul>
                <div class="uk-hidden@m uk-light">
                    <a class="uk-position-center-left uk-position-small" href="#" data-uk-slidenav-previous data-uk-slider-item="previous"></a>
                    <a class="uk-position-center-right uk-position-small" href="#" data-uk-slidenav-next data-uk-slider-item="next"></a>
                </div>
                <div class="uk-visible@m">
                    <a class="uk-position-center-left-out uk-position-small" href="#" data-uk-slidenav-previous data-uk-slider-item="previous"></a>
                    <a class="uk-position-center-right-out uk-position-small" href="#" data-uk-slidenav-next data-uk-slider-item="next"></a>
                </div>
            </div>
            
        </div>
    </div>
</section>
@endif