<div class="uk-section-muted uk-section uk-section-small" uk-scrollspy="target: [uk-scrollspy-class]; cls: uk-animation-fade; delay: false;">

    <div class="uk-container">
        <div class="uk-grid-large uk-grid-margin-large uk-grid uk-visible@m" uk-grid>
            <div class="uk-width-expand@m uk-width-1-2@s uk-first-column">

                <div class="uk-margin uk-scrollspy-inview uk-animation-fade" uk-scrollspy-class="" style="">
                    <a class="el-link" href="#">
                        <img class="el-image" alt="Logo Footer" uk-img src="{{ !empty(GlobalVar::footer()->image) ? asset('images/logo/'.GlobalVar::footer()->image) : asset('images/logo.png') }}">
                    </a>

                </div>
                <div class="uk-margin uk-width-xlarge uk-scrollspy-class=" style="font-size:.875rem; text-align: justify;">
                    {{ !empty(GlobalVar::footer()->description) ? GlobalVar::footer()->description : "Nama Kaheci sendiri diambil dari singkatan Kaos Kaki Henna Cantik, karena itu jadilah Kaheci menjadi pelopor kaos kaki motif Henna pertama di Indonesia." }}</div>

            </div>

            <div class="uk-width-expand@m uk-width-1-2@s">

                <h3 class="uk-h5 uk-scrollspy-inview uk-animation-fade" uk-scrollspy-class="" style=""> Tentang Kami</h3>
                <ul class="uk-list" uk-scrollspy-class="" style="">
                @if(!empty(GlobalVar::show_menu('footer','1')))
                    @foreach(GlobalVar::show_menu('footer','1') as $items)
                    <li class="el-item">
                        <div class="uk-panel"><a href="{{ url($items->url) }}" class="uk-link-muted">{{ $items->name }}</a></div>
                    </li>
                    @endforeach
                @endif
                </ul>

            </div>

            <div class="uk-width-expand@m uk-width-1-2@s">

                <h3 class="uk-h5 uk-scrollspy-inview uk-animation-fade" uk-scrollspy-class="" style="">Produk</h3>
                <ul class="uk-list" uk-scrollspy-class="" style="">
                @if(!empty(GlobalVar::show_menu('footer','2')))
                    @foreach(GlobalVar::show_menu('footer','2') as $items)
                    <li class="el-item">
                        <div class="uk-panel"><a href="{{ url($items->url) }}" class="uk-link-muted">{{ $items->name }}</a></div>
                    </li>
                    @endforeach
                @endif
                </ul>

            </div>

            <div class="uk-width-expand@m uk-width-1-2@s">

                <h3 class="uk-h5 uk-scrollspy-inview uk-animation-fade" uk-scrollspy-class="" style="">Informasi</h3>
                <ul class="uk-list" uk-scrollspy-class="" style="">
                @if(!empty(GlobalVar::show_menu('footer','3')))
                    @foreach(GlobalVar::show_menu('footer','3') as $items)
                    <li class="el-item">
                        <div class="uk-panel"><a href="{{ url($items->url) }}" class="uk-link-muted">{{ $items->name }}</a></div>
                    </li>
                    @endforeach
                @endif
                </ul>

            </div>

            <div class="uk-width-expand@m uk-width-1-2@s">

                <h3 class="uk-h5 uk-scrollspy-inview uk-animation-fade" uk-scrollspy-class="" style="">Ikuti kami di</h3>
                <ul class="uk-list" uk-scrollspy-class="" style="">
                @if(!empty(GlobalVar::show_menu('footer','4')))
                    @foreach(GlobalVar::show_menu('footer','4') as $items)
                    <li class="el-item">
                        <div class="uk-panel"><a href="{{ url($items->url) }}" class="uk-link-muted">{{ $items->name }}</a></div>
                    </li>
                    @endforeach
                @endif
                </ul>

            </div>
        </div>

        <!-- mobile footer -->

        <div class=" uk-grid uk-hidden@m" uk-grid>
            
            <div class="uk-width-expand@m uk-width-1-2@s uk-first-column">
                <div class="uk-margin uk-scrollspy-inview uk-animation-fade" uk-scrollspy-class="" style="">
                    <a class="el-link" href="#">
                        <img class="el-image" alt="" uk-img src="{{ asset(isset(GlobalVar::footer()->image) ? 'images/logo/'.GlobalVar::footer()->image : "" ) }}" width="150"></a>
                </div>
                <div class="uk-margin uk-width-xlarge uk-scrollspy-class=" style="font-size:.875rem">{{ isset(GlobalVar::footer()->description) ? GlobalVar::footer()->description : "dsada" }}</div>
            </div>

            <div class="uk-child-width-1-2 uk-text-left" uk-grid>
                <div>
                    <div class="uk-card">
                        <h3 class="uk-h5 uk-scrollspy-inview uk-animation-fade" uk-scrollspy-class="" style=""> Tentang Kami</h3>
                        <ul class="uk-list" uk-scrollspy-class="" style="">
                        @if(!empty(GlobalVar::show_menu('footer','1')))
                            @foreach(GlobalVar::show_menu('footer','1') as $items)
                            <li class="el-item">
                                <div class="uk-panel"><a href="{{ url($items->url) }}" class="uk-link-muted">{{ $items->name }}</a></div>
                            </li>
                            @endforeach
                        @endif
                        </ul>
                    </div>
                </div>
                <div>
                    <div class="uk-card">
                        <h3 class="uk-h5 uk-scrollspy-inview uk-animation-fade" uk-scrollspy-class="" style="">Produk</h3>
                        <ul class="uk-list" uk-scrollspy-class="" style="">
                        @if(!empty(GlobalVar::show_menu('footer','2')))
                            @foreach(GlobalVar::show_menu('footer','2') as $items)
                            <li class="el-item">
                                <div class="uk-panel"><a href="{{ url($items->url) }}" class="uk-link-muted">{{ $items->name }}</a></div>
                            </li>
                            @endforeach
                        @endif
                        </ul>
                    </div>
                </div>
                <div>
                    <div class="uk-card">
                        <h3 class="uk-h5 uk-scrollspy-inview uk-animation-fade" uk-scrollspy-class="" style="">Informasi</h3>
                        <ul class="uk-list" uk-scrollspy-class="" style="">
                        @if(!empty(GlobalVar::show_menu('footer','3')))
                            @foreach(GlobalVar::show_menu('footer','3') as $items)
                            <li class="el-item">
                                <div class="uk-panel"><a href="{{ url($items->url) }}" class="uk-link-muted">{{ $items->name }}</a></div>
                            </li>
                            @endforeach
                        @endif
                        </ul>
                    </div>
                </div>
                <div>
                    <div class="uk-card">
                        <h3 class="uk-h5 uk-scrollspy-inview uk-animation-fade" uk-scrollspy-class="" style="">Ikuti kami di</h3>
                        <ul class="uk-list" uk-scrollspy-class="" style="">
                        @if(!empty(GlobalVar::show_menu('footer','4')))
                            @foreach(GlobalVar::show_menu('footer','4') as $items)
                            <li class="el-item">
                                <div class="uk-panel"><a href="{{ url($items->url) }}" class="uk-link-muted">{{ $items->name }}</a></div>
                            </li>
                            @endforeach
                        @endif
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- end mobile footer -->

{{--         <div class="uk-grid-large uk-grid-margin-large uk-grid" uk-grid>
        <div class="uk-width-expand@m">
            <div class="uk-card">
                <hr>
                TAGS: <br>
                <a href="#" class="uk-link-muted">Kaheci</a>  / <a href="#" class="uk-link-muted">Sarung Tangan Kaheci </a> / <a href="#" class="uk-link-muted">Kaos Kaki Kaheci</a>
                <hr>
                <div class="uk-text-center">Copyright &copy; 2019 Kaheci. All Right Reserved.</div>
            </div>
        </div>
        </div> --}}
    </div>

    <a href="https://api.whatsapp.com/send?phone={{ isset(GlobalVar::show_cs('wa')[0]->value) ? GlobalVar::show_cs('wa')[0]->value : "" }}&text=Saya%20tertarik%20dengan%20produk%20Kaheci" class="whatsapp_footer" target="_blank">
        <span uk-icon="icon: whatsapp; ratio: 2" class="whatsapp_footer_icon"></span>
    </a>

</div>