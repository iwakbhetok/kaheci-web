<div class="uk-section uk-section-medium uk-section-default">
    <div class="uk-container">

        <h3 class="uk-text-center text-title-section">Produk Terkait</h3>

        <div class="uk-grid-medium uk-child-width-1-1 uk-child-width-1-4@s uk-text-center uk-animation-fade" uk-grid>
            @foreach($relatedProduct as $product)
            <div>
                <div class="uk-card">
                    <div class="container">
                        <div class="product-highlight-new uk-position-left uk-flex uk-flex-middle" style="z-index: 2">
                            @if(strlen($product->product_type) > 4)
                            <span class="uk-position-center" style="font-size:.7rem;">{{ ucwords($product->product_type) }}</span>
                            @else
                            <span class="uk-position-center">{{ ucwords($product->product_type) }}</span>
                            @endif
                        </div>
                        <a href="{{ url('/produk-detail/'. $product->slug) }}">
                        <div class="container-prod">
                            <img src="{{ asset('files/products/'. $product->slug.'/thumbs/'.$product->main_image .'') }}" alt="Avatar" class="image-prod">
                            <div class="overlay-prod">
                                <div class="text-prod">View Detail</div>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                        <span class="uk-text-emphasis uk-margin-remove-top">{{ $product->category_name }} </span>
                        <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">{{ $product->name }}</p>
                        <p class="text-color-one uk-margin-remove-top">Rp. {{ number_format($product->unit_price, 0) }}</p>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- <div>
                <div class="uk-card">
                    <div class="container">
                        <div class="product-highlight-sale uk-position-left uk-flex uk-flex-middle">
                            <span class="uk-position-center">SALE</span>
                        </div>
                        <a href="#">
                            <img src="{{ asset('images/produk-unggulan/sarung-tangan.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="text">View Detail</div>
                            </div>
                        </a>
                    </div>
                    <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                        <span class="uk-text-emphasis uk-margin-remove-top">Handsock &amp; Manset</span>
                        <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">Handsock Aisyah Jersey Maroon </p>
                        <p class="text-color-one uk-margin-remove-top">Rp. 85.000</p>
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-card">
                    <div class="container">
                        <div class="product-highlight-new uk-position-left uk-flex uk-flex-middle">
                            <span class="uk-position-center">NEW</span>
                        </div>
                        <a href="#">
                            <img src="{{ asset('images/produk-unggulan/kaos-kaki.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="text">View Detail</div>
                            </div>
                        </a>
                    </div>
                    <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                        <span class="uk-text-emphasis uk-margin-remove-top">Handsock &amp; Manset</span>
                        <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">Handsock Aisyah Jersey Maroon </p>
                        <p class="text-color-one uk-margin-remove-top">Rp. 85.000</p>
                    </div>
                </div>
            </div>
            <div>   
                <div class="uk-card">
                    <div class="container">
                        <div class="product-highlight-sale uk-position-left uk-flex uk-flex-middle">
                            <span class="uk-position-center">SALE</span>
                        </div>
                        <a href="#">
                            <img src="{{ asset('images/produk-unggulan/handsock-manset-2.jpg') }}" alt="">
                            <div class="overlay">
                                <div class="text">View Detail</div>
                            </div>
                        </a>
                    </div>
                    <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                        <span class="uk-text-emphasis uk-margin-remove-top">Handsock &amp; Manset</span>
                        <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">Handsock Aisyah Jersey Maroon </p>
                        <p class="text-color-one uk-margin-remove-top">Rp. 85.000</p>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>