@if(!empty($banner[0]))
<div class="uk-section-default uk-section uk-padding-remove-vertical uk-visible@m" uk-scrollspy="cls:uk-animation-fade">
    <div class="uk-grid-margin uk-grid uk-grid-stack" uk-grid="">
        <div class="uk-width-1-1@m uk-first-column">

            <div uk-slideshow class="uk-margin uk-text-left uk-slideshow">
                <div class="uk-position-relative">
                    <ul class="uk-slideshow-items" uk-height-viewport="offset-top: true; minHeight: 300;" style="min-height: calc(100vh - 80px);">
                        <li class="el-item" tabindex="-1" style="">

                            <div class="uk-position-cover" uk-slideshow-parallax="scale: 1,1,1" style="transform: scale(1.2);">
                            <img class="el-image uk-cover" alt="" uk-img="target: !.uk-slideshow-items" uk-cover src="{{ asset('images/banner/desktop/'.$banner[0]->image)}}">
                            </div>
                            <div class="uk-position-cover" uk-slideshow-parallax="opacity: 0.5,0,0; backgroundColor: #000,#000"></div>

                            <div class="uk-position-cover uk-flex uk-flex-left uk-flex-middle uk-container uk-section">
                                <div class="el-overlay uk-panel uk-width-xlarge  uk-margin-remove-first-child" uk-slideshow-parallax="x: 600,0,-600;" style="transform: translateX(-600px);">

                                    <h1 class="el-title uk-heading-small uk-margin-top uk-margin-remove-bottom text-highlight-slide" style="color:#c58f49"> {{ ucfirst($banner[0]->name) }} </h3>
                                    <br>
                                    <span class="uk-text-emphasis" style="text-align: justify;font-size: 1em">
                                       <p> {{str_limit(strip_tags(str_replace('&nbsp;', ' ', $banner[0]->description)),350,'...')}} </p>
                                    </span><br><br>
                                    <a class="uk-button button-cta-category uk-text-capitalize" href="{{ $banner[0]->url }}">{{ $banner[0]->text_url }} <span class="arrow-category" uk-icon="chevron-right"></span></a>

                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>
@endif
<!-- end desktop slider -->

@if(!empty($banner[0]))
<div class="uk-section-default uk-section uk-padding-remove-vertical uk-hidden@m" uk-scrollspy="cls:uk-animation-fade">

    <div class="uk-grid-margin uk-grid uk-grid-stack" uk-grid="">
        <div class="uk-width-1-1@m uk-first-column">

            <div uk-slideshow class="uk-margin uk-text-left uk-slideshow">
                <div class="uk-position-relative">

                    <ul class="uk-slideshow-items" uk-height-viewport="offset-top: true; minHeight: 300;" style="min-height: calc(100vh - 80px);">
                        <li class="el-item" tabindex="-1" style="">

                            <div class="uk-position-cover" uk-slideshow-parallax="scale: 1,1,1" style="transform: scale(1.2);">
                                <img class="el-image uk-cover" alt="" uk-img="target: !.uk-slideshow-items" uk-cover src="{{ asset('images/banner/mobile/'.$banner[0]->image_mobile)}}">
                            </div>
                            
                            <div class="uk-position-cover uk-container uk-section banner-mobile">
                                <div class="el-overlay uk-panel uk-margin-remove-first-child" uk-slideshow-parallax="x: 600,0,-600;" style="transform: translateX(-600px);">
                                    <h3 class="el-title uk-heading-small uk-margin-remove-top text-highlight-slide-mobile" style="color:#c4996c;"> {{ ucfirst($banner[0]->name) }} </h3>
                                    <span class="description-slider">
                                        {{str_limit(strip_tags(str_replace('&nbsp;', ' ', $banner[0]->description)),200,'...')}} 
                                    </span><br><br>
                                    <a class="uk-button button-cta-category uk-text-capitalize" href="{{ $banner[0]->url }}">{{ $banner[0]->text_url }} <span class="arrow-category" uk-icon="chevron-right"></span></a>

                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>

        </div>
    </div>

</div>
@endif