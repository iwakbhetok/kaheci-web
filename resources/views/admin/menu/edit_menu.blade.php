@extends('layouts.admin')
@section('content')

<div class="page-hero page-container" id="page-hero">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Tambah Menu</h2><small class="text-muted">tambahkan menu</small></div>
            <div class="flex"></div>
            <div><a href="{{ url('/admin/menu') }}" class="btn btn-md btn-danger"><i data-feather="arrow-left"></i><span class="d-none d-sm-inline mx-1">Batal</span></a></div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        @if ($errors->any())
            <div class="row alert alert-danger">
                <div class="col-sm-10">
                    <ul style="margin: auto;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-2">                
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif

        <form action="{{ url('/admin/menu/update') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="text-left pt-2">
                                <p style="color: red"><b>Note:</b> Kolom dengan tanda (<span style="color: red">*</span>) tidak boleh kosong.</p>
                            </div>                            
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <input type="hidden" name="id" value="{{ $menu->id }}">
                                    <label>Nama Menu<span style="color: red">*</span></label>
                                    <input autocomplete="off" type="text" name="title" class="form-control" placeholder="title in here..." required value="{{ $menu->name }}">
                                </div>
                                <div class="form-group col-sm-12">
                                    <input type="hidden" name="location" value="footer">
                                    <label>URL<span style="color: red">*</span></label>
                                    <input autocomplete="off" type="text" name="url" class="form-control" placeholder="url in here..." required value="{{ $menu->url }}">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Lokasi Menu<span style="color: red">*</span></label>  
                                    <select class="custom-select" name="location" id="location" required>
                                        <option value="">Pilih lokasi</option>
                                        <option value="header" {{ ($menu->location == "header") ? "selected='selected'" : "" }}>Header</option>
                                        <option value="footer" {{ ($menu->location == "footer") ? "selected='selected'" : "" }}>Footer</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Baris Grup<span style="color: red">*</span></label>  
                                    <select class="custom-select" name="row" id="row" {!! ($menu->location == 'header') ? "disabled='true'" : '' !!} required>
                                        <option value="">Pilih Grup</option>
                                        <option value="1" {{ ($menu->row == "1") ? "selected='selected'" : "" }}>Tentang Kami</option>
                                        <option value="2" {{ ($menu->row == "2") ? "selected='selected'" : "" }}>Produk</option>
                                        <option value="3" {{ ($menu->row == "3") ? "selected='selected'" : "" }}>Informasi</option>
                                        <option value="4" {{ ($menu->row == "4") ? "selected='selected'" : "" }}>Sosial Media</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Status<span style="color: red">*</span></label>  
                                    <select class="custom-select" name="status" required>
                                        <option value="">Pilih status</option>
                                        <option value="1" {{ ($menu->status == 1) ? "selected='selected'" : "" }}>Aktif</option>
                                        <option value="0" {{ ($menu->status == 0) ? "selected='selected'" : "" }}>Tidak Aktif</option>
                                    </select>
                                </div>
                            </div>
                            <div class="text-right pt-2">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection

@section('plugin_js')
    <script type="text/javascript">
        $(function(){
            $('#location').on('change', function() {
                if (this.value === 'header') {
                    $('#row').attr('disabled',true);        
                } else {
                    $('#row').attr('disabled',false);
                }
            });
        });
    </script>
@endsection
