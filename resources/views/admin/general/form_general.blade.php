@extends('layouts.admin')
@section('content')


<div class="page-hero page-container" id="page-hero" data-plugin="general">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Setting General</h2><small class="text-muted">Konfigurasi General</small></div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        @if ($errors->any())
            <div class="row alert alert-danger">
                <div class="col-sm-10">
                    <ul style="margin: auto;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-2">                
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif
        <div id="accordion">
            <p class="text-muted"><strong>Banner</strong></p>
            <div class="card">

                <div class="d-flex align-items-center px-4 py-3 b-t pointer" data-toggle="collapse" data-parent="#accordion" data-target="#c_1"><i data-feather="airplay"></i>
                    <div class="px-3">
                        <div>Banner Header</div>
                    </div>
                    <div class="flex"></div>
                    <div><i data-feather="chevron-right"></i></div>
                </div>
                <div class="collapse p-4" id="c_1">
                    <p style="color: red"><b>Note:</b> Kolom dengan tanda (<span style="color: red">*</span>) tidak boleh kosong.</p>
                    <form role="form" action="{{ url('/admin/general/save_banner') }}" method="POST" enctype="multipart/form-data">
                     @csrf
                        <div class="form-group">
                            <label>Judul<span style="color: red">*</span></label>
                            <input type="hidden" name="id" value="{{ isset($top->id) ? $top->id :'' }}">
                            <input type="hidden" name="type" value="top">
                            <input type="hidden" name="curr_img_desktop" value="{{ isset($top->image) ? $top->image :'' }}">
                            <input type="hidden" name="curr_img_mobile" value="{{ isset($top->image_mobile) ? $top->image_mobile :'' }}">
                            <input autocomplete="off" type="text" name="name" class="form-control" placeholder="title in here..." value="{{ isset($top->name) ? $top->name :'' }}">
                        </div>
                        <div class="form-group">
                            <label>Deskripsi<span style="color: red">*</span></label>
                            <textarea name="description" name="description" class="form-control" rows="6" data-minwords="6" placeholder="description in here...">{{ isset($top->description) ? $top->description :'' }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Text Link<span style="color: red">*</span></label>
                            <input autocomplete="off" name="text_url" type="text" class="form-control" placeholder="Lihat Kategori" value="{{ isset($top->text_url) ? $top->text_url :'Lihat' }}">
                        </div>
                        <div class="form-group">
                            <label>Link<span style="color: red">*</span></label>
                            <input autocomplete="off" name="url" type="text" class="form-control" placeholder="http://www.example.com" value="{{ isset($top->url) ? $top->url :'' }}">
                        </div>
                        <div class="form-group">
                            <label>Gambar Versi Desktop<span style="color: red">*</span></label>
                            <div class="col-sm-12 desktop">
                                <center>
                                    <div class="dropzone white b-a b-3x b-dashed b-primary p-a rounded p-5 text-center banner_header" style="width: 50%;height: 50%">
                                        <button data-toggle="tooltip" class="btn btn-md btn-raised btn-wave btn-icon btn-rounded mb-2 green text-white btnCloseDesktop" style="display:flex;"><i data-feather="x"></i></button>

                                        <div class="dz-message image">
                                            <div class="preview">
                                                @if(!isset($top->image) or ($top->image == null))
                                                    <img width="400" height="auto" src="{{ asset('files/default.png') }}">
                                                @else
                                                    <img width="400" height="auto" src="{{ asset('images/banner/desktop/'.$top->image) }}">
                                                @endif
                                            </div>
                                            <div class="upload-btn-wrapper" style="display:none;">
                                                <button class="btn w-sm mb-1 btn-outline-primary">Upload Image</button>
                                                <input type="file" name="image" id="image_header_banner" accept="image/*"/>
                                                <br>
                                                <p>ukuran minimal: 600px x 1200px</p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Gambar Versi Mobile<span style="color: red">*</span></label>
                            <div class="col-sm-12 mobile">
                                <center>
                                    <div class="dropzone white b-a b-3x b-dashed b-primary p-a rounded p-5 text-center banner_header" style="width: 50%;height: 50%">
                                        <button data-toggle="tooltip" class="btn btn-md btn-raised btn-wave btn-icon btn-rounded mb-2 green text-white btnCloseMobile" style="display:flex;"><i data-feather="x"></i></button>

                                        <div class="dz-message image">
                                            <div class="preview">
                                                @if(!isset($top->image_mobile) or ($top->image_mobile == null))
                                                    <img width="300" height="auto" src="{{ asset('files/default.png') }}">
                                                @else
                                                    <img width="300" height="auto" src="{{ asset('images/banner/mobile/'.$top->image_mobile) }}">
                                                @endif
                                            </div>
                                            <div class="upload-btn-wrapper" style="display:none;">
                                                <button class="btn w-sm mb-1 btn-outline-primary">Upload Image</button>
                                                <input type="file" name="image_mobile" id="image_header_banner_mobile" accept="image/*"/>
                                                <br>
                                                <p>ukuran minimal: 400px x 600px</p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-2">Update</button>
                    </form>
                </div>

                <div class="d-flex align-items-center px-4 py-3 b-t pointer" data-toggle="collapse" data-parent="#accordion" data-target="#c_2"><i data-feather="airplay"></i>
                    <div class="px-3">
                        <div>Banner Product Coming Soon</div>
                    </div>
                    <div class="flex"></div>
                    <div><i data-feather="chevron-right"></i></div>
                </div>
                <div class="collapse p-4" id="c_2">
                    <form role="form" action="{{ url('/admin/general/save_prod_banner') }}" method="POST" enctype="multipart/form-data">
                     @csrf
                        <div class="form-group">
                            <label>Judul<span style="color: red">*</span></label>
                            <input type="hidden" name="id" value="{{ isset($coming_soon->id) ? $coming_soon->id :'' }}">
                            <input type="hidden" name="type" value="coming_soon">
                            <input type="hidden" name="curr_img_prod_bann_desktop" value="{{ isset($coming_soon->image) ? $coming_soon->image :'' }}">
                            <input type="hidden" name="curr_img_prod_bann_mobile" value="{{ isset($coming_soon->image_mobile) ? $coming_soon->image_mobile :'' }}">
                            <input autocomplete="off" type="text" name="name" class="form-control" placeholder="title in here..." value="{{ isset($coming_soon->name) ? $coming_soon->name :'' }}">
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="description" name="description" class="form-control" rows="6" data-minwords="6" placeholder="description in here...">{{ isset($coming_soon->description) ? $coming_soon->description :'' }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Text Link<span style="color: red">*</span></label>
                            <input autocomplete="off" name="text_url" type="text" class="form-control" placeholder="Lihat Kategori" value="{{ isset($coming_soon->text_url) ? $coming_soon->text_url :'Lihat' }}">
                        </div>
                        <div class="form-group">
                            <label>Link<span style="color: red">*</span></label>
                            <input autocomplete="off" name="url" type="text" class="form-control" placeholder="www.example.com" value="{{ isset($coming_soon->url) ? $coming_soon->url :'' }}">
                        </div>
                        <div class="form-group">
                            <label>Gambar Versi Desktop<span style="color: red">*</span></label>
                            <div class="col-sm-12 desktop">
                                <center>
                                    <div class="dropzone white b-a b-3x b-dashed b-primary p-a rounded p-5 text-center prod_banner" style="width: 50%;height: 50%">
                                        <button data-toggle="tooltip" class="btn btn-md btn-raised btn-wave btn-icon btn-rounded mb-2 green text-white btnCloseProdBannDesktop" style="display:flex;"><i data-feather="x"></i></button>

                                        <div class="dz-message image">
                                            <div class="preview">
                                                @if(!isset($coming_soon->image) or (empty($coming_soon->image)))
                                                    <img width="400" height="auto" src="{{ asset('files/default.png') }}">
                                                @else
                                                    <img width="400" height="auto" src="{{ asset('images/banner/desktop/'.$coming_soon->image) }}">
                                                @endif
                                            </div>
                                            <div class="upload-btn-wrapper" style="display:none;">
                                                <button class="btn w-sm mb-1 btn-outline-primary">Upload Image</button>
                                                <input type="file" name="image_prod_banner_desktop" id="image_prod_banner_desktop" accept="image/*"/>
                                                <br>
                                                <p>ukuran minimal: 600px x 1200px</p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Gambar Versi Mobile<span style="color: red">*</span></label>
                            <div class="col-sm-12 mobile">
                                <center>
                                    <div class="dropzone white b-a b-3x b-dashed b-primary p-a rounded p-5 text-center prod_banner" style="width: 50%;height: 50%">
                                        <button data-toggle="tooltip" class="btn btn-md btn-raised btn-wave btn-icon btn-rounded mb-2 green text-white btnCloseProdBannMobile" style="display:flex;"><i data-feather="x"></i></button>

                                        <div class="dz-message image">
                                            <div class="preview">
                                                @if(!isset($coming_soon->image_mobile) or (empty($coming_soon->image_mobile)))
                                                    <img width="300" height="auto" src="{{ asset('files/default.png') }}">
                                                @else
                                                    <img width="300" height="auto" src="{{ asset('images/banner/mobile/'.$coming_soon->image_mobile) }}">
                                                @endif
                                            </div>
                                            <div class="upload-btn-wrapper" style="display:none;">
                                                <button class="btn w-sm mb-1 btn-outline-primary">Upload Image</button>
                                                <input type="file" name="image_prod_banner_mobile" id="image_prod_banner_mobile" accept="image/*"/>
                                                <br>
                                                <p>ukuran minimal: 400px x 600px</p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-2">Update</button>
                    </form>
                </div>
            </div>
        </div>
        <div id="accordion">
            <p class="text-muted"><strong>Logo</strong></p>
            <div class="card">

                <div class="d-flex align-items-center px-4 py-3 b-t pointer" data-toggle="collapse" data-parent="#accordion" data-target="#c_3"><i data-feather="image"></i>
                    <div class="px-3">
                        <div>Logo Header</div>
                    </div>
                    <div class="flex"></div>
                    <div><i data-feather="chevron-right"></i></div>
                </div>
                <div class="collapse p-4" id="c_3">
                    <form role="form" action="{{ url('/admin/general/save_logo_header') }}" method="POST" enctype="multipart/form-data">
                     @csrf
                        <div class="form-group">
                            <label>Logo</label>
                            <div class="col-sm-12 header">
                                <center>
                                <input type="hidden" name="id" value="{{ isset($header[0]->id) ? $header[0]->id : "" }}">                               
                                <input type="hidden" name="location" value="header">
                                <input type="hidden" name="current_image_header" value="{{ isset($header[0]->image) ? $header[0]->image : "" }}">                                   
                                <div class="dropzone white b-a b-3x b-dashed b-primary p-a rounded p-5 text-center" style="width: 50%;height: 50%">
                                        <button data-toggle="tooltip" class="btn btn-md btn-raised btn-wave btn-icon btn-rounded mb-2 green text-white btnLogoHeaderClose" style="display:flex;"><i data-feather="x"></i></button>

                                        <div class="dz-message image">
                                            <div class="preview">
                                                @if(!isset($header[0]->image) or (empty($header[0]->image)))
                                                    <img width="400" height="auto" src="{{ asset('files/default.png') }}">
                                                @else
                                                    <img width="400" height="auto" src="{{ asset('images/logo/'.$header[0]->image) }}">
                                                @endif
                                            </div>
                                            <div class="upload-btn-wrapper" style="display:none;">
                                                <button class="btn w-sm mb-1 btn-outline-primary">Upload Image</button>
                                                <input type="file" name="image_logo_header" id="image_logo_header" accept="image/*"/>
                                                <br>
                                                <p>ukuran minimal: 100px x 150px</p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-2">Update</button>
                    </form>
                </div>

                <div class="d-flex align-items-center px-4 py-3 b-t pointer" data-toggle="collapse" data-parent="#accordion" data-target="#c_4"><i data-feather="image"></i>
                    <div class="px-3">
                        <div>Logo Footer</div>
                    </div>
                    <div class="flex"></div>
                    <div><i data-feather="chevron-right"></i></div>
                </div>
                <div class="collapse p-4" id="c_4">
                    <form role="form" action="{{ url('/admin/general/save_logo_footer') }}" method="POST" enctype="multipart/form-data">
                     @csrf
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <input type="hidden" name="id" value="{{ isset($footer[0]->id) ? $footer[0]->id: "" }}">
                            <input type="hidden" name="location" value="footer">
                            <input type="hidden" name="current_image_footer" value="{{ isset($footer[0]->image) ? $footer[0]->image : "" }}">
                            <textarea name="description" name="description" class="form-control" rows="6" data-minwords="6" placeholder="description in here...">{{ isset($footer[0]->description) ? $footer[0]->description :'' }}</textarea>
                        </div>

                        <div class="form-group">
                            <label>Logo</label>
                            <div class="col-sm-12 footer">
                                <center>
                                    <div class="dropzone white b-a b-3x b-dashed b-primary p-a rounded p-5 text-center" style="width: 50%;height: 50%">
                                        <button data-toggle="tooltip" class="btn btn-md btn-raised btn-wave btn-icon btn-rounded mb-2 green text-white btnLogoFooterClose" style="display:flex;"><i data-feather="x"></i></button>

                                        <div class="dz-message image">
                                            <div class="preview">
                                                @if(!isset($footer[0]->image) or (empty($footer[0]->image)))
                                                    <img width="400" height="auto" src="{{ asset('files/default.png') }}">
                                                @else
                                                    <img width="400" height="auto" src="{{ asset('images/logo/'.$footer[0]->image) }}">
                                                @endif
                                            </div>
                                            <div class="upload-btn-wrapper" style="display:none;">
                                                <button class="btn w-sm mb-1 btn-outline-primary">Upload Image</button>
                                                <input type="file" name="image_logo_footer" id="image_logo_footer" accept="image/*"/>
                                                <br>
                                                <p>ukuran minimal: 100px x 150px</p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-2">Update</button>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>

@endsection


@section('plugin_js')
    <script type="text/javascript">
        $('#image_header_banner').bind('change', function() {
          let filesize = this.files[0].size // On older browsers this can return NULL.
          let filesizeMB = (filesize / (1024*1024)).toFixed(2);
          if(filesizeMB > 2) {
              $('#image_header_banner').val('');
              alert('File gambar harus dibawah 2MB.');
          }
        });        
        $('#image_header_banner_mobile').bind('change', function() {
          let filesize = this.files[0].size // On older browsers this can return NULL.
          let filesizeMB = (filesize / (1024*1024)).toFixed(2);
          if(filesizeMB > 2) {
              $('#image_header_banner_mobile').val('');
              alert('File gambar harus dibawah 2MB.');
          }
        });        
        $('#image_prod_banner_desktop').bind('change', function() {
          let filesize = this.files[0].size // On older browsers this can return NULL.
          let filesizeMB = (filesize / (1024*1024)).toFixed(2);
          if(filesizeMB > 2) {
              $('#image_prod_banner_desktop').val('');
              alert('File gambar harus dibawah 2MB.');
          }
        });        
        $('#image_prod_banner_mobile').bind('change', function() {
          let filesize = this.files[0].size // On older browsers this can return NULL.
          let filesizeMB = (filesize / (1024*1024)).toFixed(2);
          if(filesizeMB > 2) {
              $('#image_prod_banner_mobile').val('');
              alert('File gambar harus dibawah 2MB.');
          }
        });        
        $('#image_logo_header').bind('change', function() {
          let filesize = this.files[0].size // On older browsers this can return NULL.
          let filesizeMB = (filesize / (1024*1024)).toFixed(2);
          if(filesizeMB > 2) {
              $('#image_logo_header').val('');
              alert('File gambar harus dibawah 2MB.');
          }
        });        
        $('#image_logo_footer').bind('change', function() {
          let filesize = this.files[0].size // On older browsers this can return NULL.
          let filesizeMB = (filesize / (1024*1024)).toFixed(2);
          if(filesizeMB > 2) {
              $('#image_logo_footer').val('');
              alert('File gambar harus dibawah 2MB.');
          }
        });        
    </script>
@endsection

