@extends('layouts.admin')
@section('content')


<div id="messages"></div>
<div class="page-hero page-container" id="page-hero" data-plugin="admins">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Admins</h2><small class="text-muted">List Admin</small></div>
            <div class="flex"></div>
            <div><a href="{{ route('admin.create') }}" class="btn btn-md btn-primary"><span class="d-none d-sm-inline mx-1">Tambah Admin</span> <i data-feather="arrow-right"></i></a></div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        <div class="mb-5">
            <div class="toolbar">
                
            </div>
            <div class="table-responsive">
                <table class="table table-theme table-row v-middle">
                    <thead>
                        <tr>
                            <th style="width:20px">
                                <label class="ui-check m-0">
                                    <input type="checkbox" id="select-all" ><i></i></label>
                            </th>
                            <th class="text-muted" data-toggle-class="asc">Nama</th>
                            <th class="text-muted"><span class="d-none d-sm-block">Email</span></th>
                            <th class="text-muted"><span class="d-none d-sm-block">Status</span></th>
                            <th style="width:50px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($admins as $item)
                        <tr class="v-middle" data-id="{{ $item->id }}">
                            <td>
                                <label class="ui-check m-0">
                                <input type="checkbox" name="id[]" value="{{ $item->id }}"> <i></i></label>
                            </td>
                            <td class="flex"><a href="{{ url('admin/detail/'.$item->id) }}" class="item-title text-color">{!! substr($item->name, 0, 50) !!}</a>
                            </td>
                            <td><span class="item-amount d-none d-sm-block text-sm [object Object]">{{ $item->email }}</span></td>
                            <td><span class="item-amount d-none d-sm-block text-sm [object Object]">{{ ucfirst($item->status) }}</span></td>
                            <td>
                                <div class="item-action dropdown">
                                    <a href="#" data-toggle="dropdown" class="text-muted">
                                        <i data-feather="more-vertical"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                        <a class="dropdown-item edit" href="{{ route('admin.edit', $item->id) }}">Edit</a>
                                        <a class="dropdown-item trash" href="{{ route('admin.remove', $item->id) }}" onclick="return confirm('Anda yakin ingin menghapus pengguna ini?');" >Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-flex">
            {!! $admins->render() !!}
            <small class="text-muted py-2 mx-2">Total <span id="count">{{ number_format($admins->total(), 0) }}</span> items</small></div>
        </div>
    </div>
</div>
@endsection
