@extends('layouts.admin')
@section('content')

<div class="page-hero page-container" id="page-hero">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Tambah Pengguna CMS</h2><small class="text-muted">tambahkan pengguna</small></div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        @if ($errors->any())
            <div class="row alert alert-danger">
                <div class="col-sm-10">
                    <ul style="margin: auto;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-2">                
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif
        <form action="{{ route('admin.update') }}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="admin_id" value="{{ $admin->id }}">
        @csrf
        <div class="row">
            <div class="col-sm-8">
                    <div class="card">
                        <div class="card-header"><strong>Pengguna Baru</strong></div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label>Nama</label>
                                    <input type="text" name="nama" class="form-control" value="{{ $admin->name }}" required>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>No Telp</label>
                                    <input type="text" name="phone_number" class="form-control" value="{{ $admin->no_telp }}">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Email</label>
                                    <input type="text" name="email" class="form-control" value="{{ $admin->email }}" required>
                                </div>

                            </div>
                            
                            <div class="text-left pt-2">
                            <a href="{{ route('admin.admins') }}" class="btn btn-light"><i data-feather="arrow-left"></i><span class="d-none d-sm-inline mx-1">Kembali</span></a>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <div class="text-right pt-2">
                                
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection
