@extends('layouts.admin')
@section('content')

<div class="page-hero page-container" id="page-hero">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Profile</h2><small class="text-muted">Your personal information</small></div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding sr">
        <div class="card">
            <div class="card-header bg-dark bg-img p-0 no-border" data-stellar-background-ratio="0.1" style="background-image:url(../assets/img/b1.jpg)" data-plugin="stellar">
                <div class="bg-dark-overlay r-2x no-r-b">
                    <div class="d-md-flex">
                        <div class="p-4">
                            <div class="d-flex"><a href="page.profile.html#"><span class="avatar w-64"><img src="../assets/img/a6.jpg" alt="."> <i class="on"></i></span></a>
                                <div class="mx-3">
                                    <h5 class="mt-2">
                                    @if(Auth::guard('admin'))
                                        {{ ucwords(Auth::guard('admin')->user()->name) }}
                                    @else
                                        Anonym
                                    @endif</h5>
                                    <div class="text-fade text-sm"><span class="m-r">Senior Industrial Designer</span> <small><i class="fa fa-map-marker mr-2"></i>London, UK</small></div>
                                </div>
                            </div>
                        </div><span class="flex"></span>
                        <div class="align-items-center d-flex p-4">
                            <div class="toolbar"><a href="page.profile.html#" class="btn btn-sm bg-dark-overlay btn-rounded text-white bg-success active" data-toggle-class="bg-success"><span class="d-inline">Follow</span> <span class="d-none">Following</span> </a><a href="page.profile.html#" class="btn btn-sm btn-icon bg-dark-overlay btn-rounded"><i data-feather="phone" width="12" height="12" class="text-fade"></i> </a><a href="page.profile.html#" class="btn btn-sm btn-icon bg-dark-overlay btn-rounded"><i data-feather="more-vertical" width="12" height="12" class="text-fade"></i></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-3">
                <div class="d-flex">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="page.profile.html#" data-toggle="tab" data-target="#tab_1">Profile</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-7 col-lg-8">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab_1">
                        <div class="card">
                            <div class="px-2">
                                <div class="py-3">
                                    <ul class="nav flex-column">
                                        <li class="nav-item"><a class="nav-link"><span>Califorlia</span></a></li>
                                        <li class="nav-item"><a class="nav-link"><span>320-654-123</span></a></li>
                                        <li class="nav-item"><a class="nav-link"><span>July 10</span></a></li>
                                        <li class="nav-item"><a class="nav-link"><span>garret@gmail.com</span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="px-4 py-4">
                                <div class="row mb-2">
                                    <div class="col-6"><small class="text-muted">Cell Phone</small>
                                        <div class="my-2">1243 0303 0333</div>
                                    </div>
                                    <div class="col-6"><small class="text-muted">Family Phone</small>
                                        <div class="my-2">+32(0) 3003 234 543</div>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-6"><small class="text-muted">Reporter</small>
                                        <div class="my-2">Coch Jose</div>
                                    </div>
                                    <div class="col-6"><small class="text-muted">Manager</small>
                                        <div class="my-2">James Richo</div>
                                    </div>
                                </div>
                                <div><small class="text-muted">Bio</small>
                                    <div class="my-2">Sem tempus mauris imperdiet ac pellentesque magna tempus aenean aliquet egestas elementum ultrices massa sagittis magna bibendum laoreet quisque tincidunt arcu morbi vulputate in duis risus vulputate dolor pellentesque dui, ultricies ante habitasse eu, vel donec purus at nibh morbi scelerisque ac amet, posuere ridiculus sit tortor lacus mauris, eget pretium purus gravida ipsum tempus in morbi vitae quis sagittis</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5 col-lg-4">
                <div class="card sticky" style="z-index: 1">
                    <div class="card-header"><strong>You know these people?</strong></div>
                    <div class="list list-row">
                        <div class="list-item" data-id="17">
                            <div><a href="page.profile.html#"><span class="w-40 avatar gd-warning"><span class="avatar-status on b-white avatar-right"></span> H</span></a></div>
                            <div class="flex"><a href="page.profile.html#" class="item-author text-color">Helen Valdez</a> <a href="page.profile.html#" class="item-company text-muted h-1x">AI</a></div>
                            <div>
                                <div class="item-action dropdown"><a href="page.profile.html#" data-toggle="dropdown" class="text-muted"><i data-feather="more-vertical"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu"><a class="dropdown-item" href="page.profile.html#">See detail </a><a class="dropdown-item download">Download </a><a class="dropdown-item edit">Edit</a>
                                        <div class="dropdown-divider"></div><a class="dropdown-item trash">Delete item</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="list-item" data-id="9">
                            <div><a href="page.profile.html#"><span class="w-40 avatar gd-info"><span class="avatar-status on b-white avatar-right"></span> <img src="../assets/img/a9.jpg" alt="."></span></a></div>
                            <div class="flex"><a href="page.profile.html#" class="item-author text-color">Steven Cruz</a> <a href="page.profile.html#" class="item-company text-muted h-1x">HHH inc</a></div>
                            <div>
                                <div class="item-action dropdown"><a href="page.profile.html#" data-toggle="dropdown" class="text-muted"><i data-feather="more-vertical"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu"><a class="dropdown-item" href="page.profile.html#">See detail </a><a class="dropdown-item download">Download </a><a class="dropdown-item edit">Edit</a>
                                        <div class="dropdown-divider"></div><a class="dropdown-item trash">Delete item</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="list-item" data-id="12">
                            <div><a href="page.profile.html#"><span class="w-40 avatar gd-info"><span class="avatar-status off b-white avatar-right"></span> A</span></a></div>
                            <div class="flex"><a href="page.profile.html#" class="item-author text-color">Alan Richards</a> <a href="page.profile.html#" class="item-company text-muted h-1x">Drafty</a></div>
                            <div>
                                <div class="item-action dropdown"><a href="page.profile.html#" data-toggle="dropdown" class="text-muted"><i data-feather="more-vertical"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu"><a class="dropdown-item" href="page.profile.html#">See detail </a><a class="dropdown-item download">Download </a><a class="dropdown-item edit">Edit</a>
                                        <div class="dropdown-divider"></div><a class="dropdown-item trash">Delete item</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="list-item" data-id="19">
                            <div><a href="page.profile.html#"><span class="w-40 avatar gd-warning"><span class="avatar-status on b-white avatar-right"></span> T</span></a></div>
                            <div class="flex"><a href="page.profile.html#" class="item-author text-color">Tiffany Baker</a> <a href="page.profile.html#" class="item-company text-muted h-1x">AI</a></div>
                            <div>
                                <div class="item-action dropdown"><a href="page.profile.html#" data-toggle="dropdown" class="text-muted"><i data-feather="more-vertical"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu"><a class="dropdown-item" href="page.profile.html#">See detail </a><a class="dropdown-item download">Download </a><a class="dropdown-item edit">Edit</a>
                                        <div class="dropdown-divider"></div><a class="dropdown-item trash">Delete item</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="list-item" data-id="3">
                            <div><a href="page.profile.html#"><span class="w-40 avatar gd-primary"><span class="avatar-status away b-white avatar-right"></span> <img src="../assets/img/a3.jpg" alt="."></span></a></div>
                            <div class="flex"><a href="page.profile.html#" class="item-author text-color">Jordan Stephens</a> <a href="page.profile.html#" class="item-company text-muted h-1x">Wealth corp</a></div>
                            <div>
                                <div class="item-action dropdown"><a href="page.profile.html#" data-toggle="dropdown" class="text-muted"><i data-feather="more-vertical"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu"><a class="dropdown-item" href="page.profile.html#">See detail </a><a class="dropdown-item download">Download </a><a class="dropdown-item edit">Edit</a>
                                        <div class="dropdown-divider"></div><a class="dropdown-item trash">Delete item</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="list-item" data-id="15">
                            <div><a href="page.profile.html#"><span class="w-40 avatar gd-success"><span class="avatar-status on b-white avatar-right"></span> J</span></a></div>
                            <div class="flex"><a href="page.profile.html#" class="item-author text-color">Jean Armstrong</a> <a href="page.profile.html#" class="item-company text-muted h-1x">Google</a></div>
                            <div>
                                <div class="item-action dropdown"><a href="page.profile.html#" data-toggle="dropdown" class="text-muted"><i data-feather="more-vertical"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu"><a class="dropdown-item" href="page.profile.html#">See detail </a><a class="dropdown-item download">Download </a><a class="dropdown-item edit">Edit</a>
                                        <div class="dropdown-divider"></div><a class="dropdown-item trash">Delete item</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="list-item" data-id="11">
                            <div><a href="page.profile.html#"><span class="w-40 avatar gd-info"><span class="avatar-status on b-white avatar-right"></span> K</span></a></div>
                            <div class="flex"><a href="page.profile.html#" class="item-author text-color">Kenneth Pierce</a> <a href="page.profile.html#" class="item-company text-muted h-1x">KissKiss</a></div>
                            <div>
                                <div class="item-action dropdown"><a href="page.profile.html#" data-toggle="dropdown" class="text-muted"><i data-feather="more-vertical"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu"><a class="dropdown-item" href="page.profile.html#">See detail </a><a class="dropdown-item download">Download </a><a class="dropdown-item edit">Edit</a>
                                        <div class="dropdown-divider"></div><a class="dropdown-item trash">Delete item</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="list-item" data-id="16">
                            <div><a href="page.profile.html#"><span class="w-40 avatar gd-info"><span class="avatar-status on b-white avatar-right"></span> F</span></a></div>
                            <div class="flex"><a href="page.profile.html#" class="item-author text-color">Frances Stewart</a> <a href="page.profile.html#" class="item-company text-muted h-1x">Microsoft</a></div>
                            <div>
                                <div class="item-action dropdown"><a href="page.profile.html#" data-toggle="dropdown" class="text-muted"><i data-feather="more-vertical"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu"><a class="dropdown-item" href="page.profile.html#">See detail </a><a class="dropdown-item download">Download </a><a class="dropdown-item edit">Edit</a>
                                        <div class="dropdown-divider"></div><a class="dropdown-item trash">Delete item</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection