@extends('layouts.admin')
@section('content')

<div class="page-hero page-container" id="page-hero">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Message</h2><small class="text-muted">Detail Message</small>
        </div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        <div class="row">
            <div class="col-sm-8 col-md-12">
                <p><strong>Message content</strong></p>
                <div class="mb-4">
                    <div class="card mb-2">
                        <div class="card-header no-border"><a data-toggle="collapse" data-target="#c_1"><strong>Q:</strong> {{ $message->subject }}</a></div>
                        <div id="c_1" class="collapse in b-t">
                            <div class="card-body">
                                <div class="float-left mr-2">
                                    <span class="text-md w-32 avatar circle bg-success">
                                        @php
                                            $initial = $message->email;
                                            echo ucwords($initial[0]);
                                        @endphp
                                    </span>
                                </div>
                                <p class="text-muted" style="margin-top: 5px;">{{ $message->email }}</p>
                                <br>
                                <p class="text-muted">{{ $message->description }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <p><strong>Reply message</strong></p>
                <form>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label>Your name</label>
                            <input type="text" class="form-control" value="{{ ucwords(Auth::guard('admin')->user()->name) }}" placeholder="Name" required>
                        </div>
                        <div class="col-sm-6">
                            <label>Reply to</label>
                            <input type="email" class="form-control" value="{{ $message->email }}" placeholder="Enter email" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Subject</label>
                        <select class="form-control c-select">
                            <option></option>
                            <option>Website Errors</option>
                            <option>Product Services</option>
                            <option>Login/Signup Problem</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" rows="6" placeholder="hmm.."></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form> -->
                <a href="{{ url('/admin/messages') }}" class="btn btn-md btn-primary">Kembali</a>
            </div>
        </div>
    </div>
</div>

@endsection