@extends('layouts.admin')
@section('content')

<div class="page-hero page-container" id="page-hero" data-plugin="products">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Tambah Kategori Produk</h2><small class="text-muted">tambahkan kategori produk</small></div>
            <div class="flex"></div>
            <div><a href="{{ url('/admin/product/category') }}" class="btn btn-md btn-danger"><i data-feather="arrow-left"></i><span class="d-none d-sm-inline mx-1">Batal</span></a></div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        @if ($errors->any())
            <div class="row alert alert-danger">
                <div class="col-sm-10">
                    <ul style="margin: auto;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-2">                
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif
        <form action="{{ url('/admin/category/save') }}" method="POST" class="dropzone" enctype="multipart/form-data" id="create_category">
        @csrf
        <div class="row">
            <div class="col-sm-8">
                    <div class="card">
                        <div class="card-header">
                            <div class="text-left pt-2">
                                <p style="color: red"><b>Note:</b> Kolom dengan tanda (<span style="color: red">*</span>) tidak boleh kosong.</p>
                            </div>                            
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label>Nama Kategori Produk<span style="color: red">*</span></label>
                                    <input type="text" name="title" class="form-control" placeholder="title in here..." autocomplete="off" value="{{ old('title') }}" required>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Kategori</label>
                                    <select class="custom-select" name="category">
                                        <option value="">Jadikan kategori utama</option>
                                        {!! printTree($tree,0,old('category')) !!}
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Status<span style="color: red">*</span></label>
                                    <select class="custom-select" name="status" required>
                                        <option value="" selected="selected">Pilih status</option>
                                        <option value="active" {{ old('status') == 'active' ? 'selected="selected"' : '' }}>Aktif</option>
                                        <option value="inactive" {{ old('status') == 'inactive' ? 'selected="selected"' : '' }}>Tidak Aktif</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea name="description" class="form-control" rows="6" data-minwords="6" placeholder="description in here...">
                                    {{ old('description') }}
                                </textarea>
                            </div>
                            <div class="text-right pt-2">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-sm-4">
                <div class="dropzone white b-a b-3x b-dashed b-primary p-a rounded p-5 text-center">
                    <div class="dz-message">
                    <button data-toggle="tooltip" data-original-title="Remove image" class="btn btn-md btn-raised btn-wave btn-icon btn-rounded mb-2 green text-white btnClose" style="display:none;"><i data-feather="x"></i></button>
                        <div class="preview"></div>
                        <h4 class="my-4">Click to upload.</h4>
                        <div class="upload-btn-wrapper" style="display:inline-block;">
                            <button class="btn w-sm mb-1 btn-outline-primary">Upload Image</button>
                            <input type="file" name="image" id="image" accept="image/*" required />
                            <p>ukuran minimal: 337px x 220px</p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>

@endsection

@section('plugin_js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#image').on("change", function(){ readFile(this); });

            function readFile(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                    reader.onload = function (e) {
                    console.log(e.target.result)

                        $('.preview').attr("style", "display:inline-block;").html('<img width="200" src="' + e.target.result + '" />');
                        $('.upload-btn-wrapper').attr("style", "display:none;");      
                        $('.my-4').attr("style", "display:none;");
                        $(".upload-btn-wrapper").attr('display','none');
                        $('.btnClose').attr("style", "display:inline-block;");
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            
            $('.btnClose').on("click", function(e){
                e.preventDefault();
                $('.preview').attr("style", "display:none;");
                $('.btnClose').attr("style", "display:none;");
                $('.my-4').attr("style", "display:inline-block;");
                $('.dz-message p').attr("style", "display:inline-block;");
                $('.upload-btn-wrapper').attr("style", "display:inline-block;");
            });
        })
        
    </script>
@endsection