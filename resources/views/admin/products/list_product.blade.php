@extends('layouts.admin')
@section('content')


<div id="messages"></div>
<div class="page-hero page-container" id="page-hero" data-plugin="products">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Products</h2><small class="text-muted">Data Products</small></div>
            <div class="flex"></div>
            <div><a href="{{ url('/admin/product/add') }}" class="btn btn-md btn-primary"><span class="d-none d-sm-inline mx-1">Tambah Produk</span> <i data-feather="arrow-right"></i></a></div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        <div class="mb-5">
            <div class="toolbar">
                <form class="flex">
                    <div class="input-group">
                        <input type="text" autocomplete="off" name="q" class="form-control form-control-theme form-control-sm search" value="{{ app('request')->input('q') }}" placeholder="Search"> 
                        <span class="input-group-append">
                            <button class="btn btn-white no-border btn-sm" type="submit"><span class="d-flex text-muted"><i data-feather="search"></i></span></button>
                        </span>
                    </div>
                </form>
                <form onclick="return confirm('Anda yakin ingin menghapus item ini?');" action="{{ url('admin/product/removeall/') }}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="btn-group">
                        <button type="submit" class="btn btn-sm btn-icon btn-white" data-toggle="tooltip" title="Trash"><i data-feather="trash" class="text-muted"></i></button>
                    </div>
            </div>
            <div class="table-responsive">
                <table class="table table-theme table-row v-middle">
                    <thead>
                        <tr>
                            <th style="width:20px">
                                <label class="ui-check m-0">
                                    <input type="checkbox" id="select-all" ><i></i></label>
                            </th>
                            <th class="text-muted">Image</th>
                            <th class="text-muted" data-toggle-class="asc">Title</th>
                            <th class="text-muted"><span class="d-none d-sm-block">Price</span></th>
                            <th class="text-muted"><span class="d-none d-sm-block">Discount (%)</span></th>
                            <th class="text-muted"><span class="d-none d-sm-block">Product Type</span></th>
                            <th style="width:50px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $item)
                        <tr class="v-middle" data-id="{{ $item->id }}">
                            <td>
                                <label class="ui-check m-0">
                                    <input type="checkbox" name="id[]" value="{{ $item->id }}"> <i></i></label>
                            </td>
                            <td>
                                <div class="avatar-group">
                                    <a href="" target="_blank" class="avatar ajax w-32" data-toggle="tooltip" title="{{ $item->name }}"><img src="{{ asset('files/products/'.$item->slug.'/thumbs/'.$item->thumbnail) }}" alt="{{ $item->slug }}"></a>
                                </div>
                            </td>
                            <td class="flex"><a href="{{ url('/produk-detail/'.$item->slug) }}" target="_blank" class="item-title text-color">{!! substr($item->name, 0, 50) !!}</a>
                                <div class="item-except text-muted text-sm h-1x">{!! substr($item->description, 0, 50) !!} </div>
                            </td>
                            <td><span class="item-amount d-none d-sm-block text-sm">{{ number_format($item->unit_price) }} IDR</span></td>
                            <td><span class="item-amount d-none d-sm-block text-sm [object Object]">{{ empty($item->discount) ? 0 : $item->discount }}%</span></td>
                            <td><span class="item-amount d-none d-sm-block text-sm [object Object]">{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y')}}</span></td>
                            <td>
                            <td>
                                <div class="item-action dropdown">
                                    <a href="#" data-toggle="dropdown" class="text-muted">
                                        <i data-feather="more-vertical"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                        <a class="dropdown-item edit" href="{{ url('admin/product/edit/'.$item->id) }}">Edit</a>
                                        <a class="dropdown-item trash" href="{{ url('admin/product/remove/'.$item->id) }}" onclick="return confirm('Anda yakin ingin menghapus item ini?');" >Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-flex">
            {!! $products->render() !!}
            <small class="text-muted py-2 mx-2">Total <span id="count">{{ number_format($products->total(), 0) }}</span> items</small></div>
        </div>
    </div>
</div>
@endsection

@section('plugin_js')
    <script type="text/javascript">
        $(document).ready(function () {
            
            //action onclick checked all
            $('#select-all').click(function(event) {   
                if(this.checked) {
                    $(':checkbox').each(function() {
                        this.checked = true;                        
                    });
                } else {
                    $(':checkbox').each(function() {
                        this.checked = false;                       
                    });
                }
            });
        })
    </script>            
@endsection