@extends('layouts.admin')
@section('content')

<div class="page-hero page-container" id="page-hero" data-plugin="products">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Ubah Kategori Produk</h2><small class="text-muted">ubah kategori produk</small></div>
            <div class="flex"></div>
            <div><a href="{{ url('/admin/product/category') }}" class="btn btn-md btn-danger"><i data-feather="arrow-left"></i><span class="d-none d-sm-inline mx-1">Batal</span></a></div>
        </div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        @if ($errors->any())
            <div class="row alert alert-danger">
                <div class="col-sm-10">
                    <ul style="margin: auto;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-2">                
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif
        <form action="{{ url('/admin/category/update') }}" method="POST" class="dropzone" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{ $category->id }}">
        <input type="hidden" name="current_image" id="current_image" value="{{ $category->image }}">
        <input type="hidden" name="slug" value="{{ $category->slug }}">
        <div class="row">
            <div class="col-sm-8">
                    <div class="card">
                        <div class="card-header">
                            <div class="text-left pt-2">
                                <p style="color: red"><b>Note:</b> Kolom dengan tanda (<span style="color: red">*</span>) tidak boleh kosong.</p>
                            </div>                            
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label>Nama Kategori Produk<span style="color: red">*</span></label>
                                    <input type="text" name="title" value="{{ $category->name }}" class="form-control" placeholder="title in here..." required>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Kategori</label>
                                    <select class="custom-select" name="category">
                                        <option value="">Jadikan kategori utama</option>
                                        {!! printTree($tree,0,$category->parent_id) !!}
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Status<span style="color: red">*</span></label>
                                    <select class="custom-select" name="status" required>
                                        <option value="">Pilih status</option>
                                        <option value="active" {{ ($category->status == "active" ? "selected" : "") }}>Aktif</option>
                                        <option value="inactive" {{ ($category->status == "inactive" ? "selected" : "") }}>Tidak Aktif</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea name="description" class="form-control" rows="6" data-minwords="6">{{ $category->description }}</textarea>
                            </div>
                            <div class="text-right pt-2">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-sm-4">
                <div class="dropzone white b-a b-3x b-dashed b-primary p-a rounded p-5 text-center">
                    <div class="dz-message">
                        <button data-toggle="tooltip" data-original-title="Remove image" class="btn btn-md btn-raised btn-wave btn-icon btn-rounded mb-2 green text-white btnClose" style="display:inline-block;">
                            <i data-feather="x"></i>
                        </button>
                        <div class="preview">
                            <img width="100%" height="auto" src="{{ asset('files/category/'.$category->slug.'/'.$category->image) }}"> 
                        </div>

                        <h4 class="my-4" style="display:none;">Click to change image.</h4>
                        <div class="upload-btn-wrapper" style="display:none;">
                            <button class="btn w-sm mb-1 btn-outline-primary">Upload a file</button>
                            <input type="file" name="image" id="category_file" accept="image/*"/>
                        </div>
                        <p style="display:none;">ukuran disarankan: 280px x 280px</p>
                        
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection

@section('plugin_js')
    <script type="text/javascript">
        $(document).ready(function () {

            $('#category_file').on("change", function(){ readFile(this); });

            function readFile(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                    reader.onload = function (e) {
                    console.log(e.target.result)

                        $('.preview').attr("style", "display:inline-block;").html('<img width="200" src="' + e.target.result + '" />');
                        $('.upload-btn-wrapper').attr("style", "display:none;");      
                        $('.my-4').attr("style", "display:none;");
                        $(".upload-btn-wrapper").attr('display','none');
                        $('.btnClose').attr("style", "display:inline-block;");
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.btnClose').on("click", function(e){
                e.preventDefault();
                $('.preview').attr("style", "display:none;");
                $('.btnClose').attr("style", "display:none;");
                $('.my-4').attr("style", "display:inline-block;");
                $('.dz-message p').attr("style", "display:inline-block;");
                $('.upload-btn-wrapper').attr("style", "display:inline-block;");
            });

        })
        
    </script>
@endsection