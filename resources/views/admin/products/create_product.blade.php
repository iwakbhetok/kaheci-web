 @extends('layouts.admin')
@section('content')

<div class="page-hero page-container" id="page-hero" data-plugin="products">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Tambah Produk Baru</h2><small class="text-muted">tambahkan produk</small></div>
            <div class="flex"></div>
            <div><a href="{{ url('/admin/products') }}" class="btn btn-md btn-danger"><i data-feather="arrow-left"></i><span class="d-none d-sm-inline mx-1">Batal</span></a></div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        @if ($errors->any())
            <div class="row alert alert-danger">
                <div class="col-sm-10">
                    <ul style="margin: auto;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-2">                
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif

        <form action="{{ url('/admin/product/save') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-sm-8">
                    <div class="card">
                        <div class="card-header">
                            <div class="text-left pt-2">
                                <p style="color: red"><b>Note:</b> Kolom dengan tanda (<span style="color: red">*</span>) tidak boleh kosong.</p>
                            </div>                            
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <label>Nama Produk<span style="color: red">*</span></label>
                                    <input type="text" autocomplete="off" name="name" class="form-control" placeholder="Nama Produk" value="{{ old('name') }}" required>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Nomor SKU Produk</label>
                                    <input type="text" autocomplete="off" value="{{ old('sku') }}" name="sku" class="form-control" placeholder="Nomor SKU Produk">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Harga Produk (IDR)<span style="color: red">*</span></label>
                                    <input type="number" autocomplete="off" value="{{ old('harga') }}" name="harga" class="form-control" placeholder="100000" min="0" required>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Diskon Produk (%)</label>
                                    <input type="number" autocomplete="off" name="discount" value="{{ old('discount') }}" class="form-control" placeholder="min 0, max 100" min="0" max="100">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Berat Produk (Gram)</label>
                                    <input type="number" autocomplete="off" value="{{ old('berat') }}"  name="berat" class="form-control" min="0" placeholder="minimal 0">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Kategori<span style="color: red">*</span></label>
                                    <select class="custom-select" name="category" required>
                                        <option value="">Pilih kategori</option>
                                        {!! printTree($tree,0,old('category')) !!}
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Warna Tersedia<span style="color: red">*</span></label>
                                    <div class="row" id="addColor">
                                        <a href="#" class="btn btn-icon btn-rounded btn-primary ml-1 btnAddColor">
                                            <i data-feather="plus"></i>
                                        </a>
                                        <a href="#" class="btn btn-icon btn-rounded btn-danger ml-1 btnMinColor" style="display:none;">
                                            <i data-feather="x"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Ukuran Tersedia<span style="color: red">*</span></label> &nbsp;
                                    <label class="md-check p-3">
                                        <input type="checkbox" {{ (is_array(old('size')) and in_array('xs', old('size'))) ? ' checked' : '' }} name="size[]" value="xs"><i class="blue"></i>XS
                                    </label>
                                    <label class="md-check p-3">
                                        <input type="checkbox" {{ (is_array(old('size')) and in_array('s', old('size'))) ? ' checked' : '' }} name="size[]" value="s"><i class="blue"></i>S
                                    </label>
                                    <label class="md-check p-3">
                                        <input type="checkbox" {{ (is_array(old('size')) and in_array('m', old('size'))) ? ' checked' : '' }} name="size[]" value="m"><i class="blue"></i>M
                                    </label>
                                    <label class="md-check p-3">
                                        <input type="checkbox" {{ (is_array(old('size')) and in_array('l', old('size'))) ? ' checked' : '' }} name="size[]" value="l"><i class="blue"></i>L
                                    </label>
                                    <label class="md-check p-3">
                                        <input type="checkbox" {{ (is_array(old('size')) and in_array('xl', old('size'))) ? ' checked' : '' }} name="size[]" value="xl"><i class="blue"></i>XL
                                    </label>
                                    <label class="md-check p-3">
                                        <input type="checkbox" {{ (is_array(old('size')) and in_array('all', old('size'))) ? ' checked' : '' }} name="size[]" value="all"><i class="blue"></i>All Size
                                    </label>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Produk Tipe<span style="color: red">*</span></label> &nbsp;
                                    <label class="md-check">
                                        <input type="radio" name="product_type" {{ old('product_type') == 'new' ? 'checked' : ''}} value="new"> 
                                        <i class="blue"></i> Terbaru
                                    </label>&nbsp;
                                    <label class="md-check">
                                        <input type="radio" name="product_type" {{ old('product_type') == 'sale' ? 'checked' : ''}} value="sale"> 
                                        <i class="blue"></i> Dijual
                                    </label>&nbsp;
                                    <label class="md-check">
                                        <input type="radio" name="product_type" {{ old('product_type') == 'available' ? 'checked' : ''}} value="available"> <i class="blue"></i> Tersedia
                                    </label>&nbsp;
                                    <label class="md-check">
                                        <input type="radio" name="product_type" {{ old('product_type') == 'premium' ? 'checked' : ''}} value="premium"> <i class="blue"></i> Premium
                                    </label>&nbsp;
                                    <label class="md-check">
                                        <input type="radio" name="product_type" {{ old('product_type') == 'coming soon' ? 'checked' : ''}} value="coming soon"> <i class="blue"></i> Segera Hadir
                                    </label>&nbsp;
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Deskripsi Produk</label>
                                <textarea name="description" id="summernote" class="form-control" rows="6" data-minwords="6" placeholder="description in here...">{{ old('description') }}</textarea>
                            </div>
                            <div class="text-right pt-2">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="dropzone white b-a b-3x b-dashed b-primary p-a rounded p-5 text-center" style="width: 100%">
                        <button data-toggle="tooltip" data-original-title="Remove image" class="btn btn-md btn-raised btn-wave btn-icon btn-rounded mb-2 green text-white btnCloseMain" style="display:none;"><i data-feather="x"></i></button>
                        <div class="main-image">
                            <div class="preview"></div>
                            <h4 class="my-main-image">Main Image, Click to upload image..</h4>
                            <div class="upload-btn-wrapper-main">
                                <button class="btn w-sm mb-1 btn-outline-primary">Upload Image</button>
                                <input type="file" name="main_image" id="main_image" required accept="image/*"/>
                                <p>ukuran minimal: 300px x 300px</p>
                            </div>
                            
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="dropzone white b-a b-1x b-dashed b-primary p-a rounded p-2 text-center">
                            <div class="image_1">
                            <button data-toggle="tooltip" data-original-title="Remove image" class="btn btn-md btn-raised btn-wave btn-icon btn-rounded mb-2 green text-white btnCloseImg1" style="display:none;"><i data-feather="x"></i></button>
                                <div class="preview"></div>
                                <p class="my-image_1">Image 1, click to upload image..</p>
                                <div class="upload-btn-wrapper-image-1">
                                    <button class="btn w-sm mb-1 btn-outline-primary">Upload Image</button>
                                    <input type="file" name="image_1" id="image_1" accept="image/*"/>
                                    <p>300px x 300px</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="dropzone white b-a b-1x b-dashed b-primary p-a rounded p-2 text-center">
                            <div class="image_2">
                            <button data-toggle="tooltip" data-original-title="Remove image" class="btn btn-md btn-raised btn-wave btn-icon btn-rounded mb-2 green text-white btnCloseImg2" style="display:none;"><i data-feather="x"></i></button>
                                <div class="preview"></div>
                                <p class="my-image_2">Image 2, click to upload image..</p>
                                <div class="upload-btn-wrapper-image-2">
                                    <button class="btn w-sm mb-1 btn-outline-primary">Upload Image</button>
                                    <input type="file" name="image_2" id="image_2" accept="image/*"/>
                                    <p>300px x 300px</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="dropzone white b-a b-1x b-dashed b-primary p-a rounded p-2 text-center">
                            <div class="image_3">
                            <button data-toggle="tooltip" data-original-title="Remove image" class="btn btn-md btn-raised btn-wave btn-icon btn-rounded mb-2 green text-white btnCloseImg3" style="display:none;"><i data-feather="x"></i></button>
                                <div class="preview"></div>
                                <p class="my-image_3">Image 3, click to upload image..</p>
                                <div class="upload-btn-wrapper-image-3">
                                    <button class="btn w-sm mb-1 btn-outline-primary">Upload Image</button>
                                    <input type="file" name="image_3" id="image_3" accept="image/*"/>
                                    <p>300px x 300px</p>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-6">
                        <div class="dropzone white b-a b-1x b-dashed b-primary p-a rounded p-2 text-center">
                            <div class="image_4">
                            <button data-toggle="tooltip" data-original-title="Remove image" class="btn btn-md btn-raised btn-wave btn-icon btn-rounded mb-2 green text-white btnCloseImg4" style="display:none;"><i data-feather="x"></i></button>
                                <div class="preview"></div>
                                <p class="my-image_4">Image 4, click to upload image..</p>
                                <div class="upload-btn-wrapper-image-4">
                                    <button class="btn w-sm mb-1 btn-outline-primary">Upload Image</button>
                                    <input type="file" name="image_4" id="image_4" accept="image/*"/>
                                    <p>300px x 300px</p>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection

@section('plugin_js')
    <script type="text/javascript">
    $('#main_image').on("change", function(){ readFile(this); });

            function readFile(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                    reader.onload = function (e) {

                        $('.main-image .preview').attr("style", "display:inline-block;").html('<img width="200" src="' + e.target.result + '" />');
                        $('.my-main-image').attr("style", "display:none;");
                        $(".upload-btn-wrapper-main").attr("style", "display:none;");
                        $('.btnCloseMain').attr("style", "display:inline-block;");
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.btnCloseMain').on("click", function(e){
                e.preventDefault();
                $('.main-image .preview').attr("style", "display:none;");
                $('.my-main-image').attr("style", "display:inline-block;;");
                $(".upload-btn-wrapper-main").attr("style", "display:inline-block;;");
                $('.btnCloseMain').attr("style", "display:none;");
            });
            //end main image


            //image 1
            $('#image_1').on("change", function(){ readFileImg1(this); });

            function readFileImg1(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                    reader.onload = function (e) {

                        $('.image_1 .preview').attr("style", "display:inline-block;").html('<img width="100%" src="' + e.target.result + '" />');
                        $('.my-image_1').attr("style", "display:none;");
                        $(".upload-btn-wrapper-image-1").attr("style", "display:none;");
                        $('.btnCloseImg1').attr("style", "display:inline-block;");
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.btnCloseImg1').on("click", function(e){
                e.preventDefault();
                $('.image_1 .preview').attr("style", "display:none;");
                $('.my-image_1').attr("style", "display:inline-block;;");
                $(".upload-btn-wrapper-image-1").attr("style", "display:inline-block;;");
                $('.btnCloseImg1').attr("style", "display:none;");
            });
            //end image 1


            //image 2
            $('#image_2').on("change", function(){ readFileImg2(this); });

            function readFileImg2(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                    reader.onload = function (e) {

                        $('.image_2 .preview').attr("style", "display:inline-block;").html('<img width="100%" src="' + e.target.result + '" />');
                        $('.my-image_2').attr("style", "display:none;");
                        $(".upload-btn-wrapper-image-2").attr("style", "display:none;");
                        $('.btnCloseImg2').attr("style", "display:inline-block;");
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.btnCloseImg2').on("click", function(e){
                e.preventDefault();
                $('.image_2 .preview').attr("style", "display:none;");
                $('.my-image_2').attr("style", "display:inline-block;;");
                $(".upload-btn-wrapper-image-2").attr("style", "display:inline-block;;");
                $('.btnCloseImg2').attr("style", "display:none;");
            });
            //end image 2


            //image 3
            $('#image_3').on("change", function(){ readFileImg3(this); });

            function readFileImg3(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                    reader.onload = function (e) {

                        $('.image_3 .preview').attr("style", "display:inline-block;").html('<img width="100%" src="' + e.target.result + '" />');
                        $('.my-image_3').attr("style", "display:none;");
                        $(".upload-btn-wrapper-image-3").attr("style", "display:none;");
                        $('.btnCloseImg3').attr("style", "display:inline-block;");
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.btnCloseImg3').on("click", function(e){
                e.preventDefault();
                $('.image_3 .preview').attr("style", "display:none;");
                $('.my-image_3').attr("style", "display:inline-block;;");
                $(".upload-btn-wrapper-image-3").attr("style", "display:inline-block;;");
                $('.btnCloseImg3').attr("style", "display:none;");
            });
            //end image 3


            //image 4
            $('#image_4').on("change", function(){ readFileImg4(this); });

            function readFileImg4(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                    reader.onload = function (e) {

                        $('.image_4 .preview').attr("style", "display:inline-block;").html('<img width="100%" src="' + e.target.result + '" />');
                        $('.my-image_4').attr("style", "display:none;");
                        $(".upload-btn-wrapper-image-4").attr("style", "display:none;");
                        $('.btnCloseImg4').attr("style", "display:inline-block;");
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.btnCloseImg4').on("click", function(e){
                e.preventDefault();
                $('.image_4 .preview').attr("style", "display:none;");
                $('.my-image_4').attr("style", "display:inline-block;;");
                $(".upload-btn-wrapper-image-4").attr("style", "display:inline-block;;");
                $('.btnCloseImg4').attr("style", "display:none;");
            });
            //end image 3


            // Add color product input
            var i = 1;
            var countInput = $('.col-md-2').length;

            $('.btnAddColor').on("click", function(e){
                e.preventDefault();
                $(".btnMinColor").attr("style", "display:flex;");
                $("#addColor").append('<div class="col-md-2" id=element_"'+i+'"><div class="card p-1"><input type="color" id="color_'+i+'" name="color[]" class="form-control" value="#e66465"></div></div>');
                i++;
                if(i > 1){
                    $(".btnMinColor").attr("style", "display:flex;");
                }
            });

            $('.btnMinColor').on("click", function(e){
                e.preventDefault();
                $("#addColor").children().last().remove('.col-md-2');
                i--;
                if(i <= 1){
                    $(".btnMinColor").attr("style", "display:none;");
                }
            });        

    $('#summernote').summernote({
        placeholder: 'write here...',
        height: 300,
        popover: {
            image: [
            ],
            link: [
            ],
            air: []
        },
        toolbar: [
              ['font', ['bold', 'underline', 'clear']],
              ['color', ['color']],
              ['para', ['ul', 'ol']],
              ['table', ['table']],
              ['insert',['link']],
        ],
        styleTags: ['p', 'h1', 'h2', 'h3', 'h4', 'h5'],
    });
       
    </script>
@endsection
