<div id="aside" class="page-sidenav no-shrink bg-light nav-dropdown fade" aria-hidden="true" data-plugin="general">
        <div class="sidenav h-100 modal-dialog bg-light">
            <div class="navbar">
                <a href="{{ url('admin/dashboard') }}" class="navbar-brand">
                    <img src="{{ asset('images/logo.png') }}" alt="">
                <span class="hidden-folded d-inline l-s-n-1x">Kaheci</span></a>
            </div>
            <div class="flex scrollable hover">
                <div class="nav-active-text-primary" data-nav>
                    <ul class="nav bg">
                        <li class="nav-header hidden-folded">
                            <span class="text-muted">Main</span>
                        </li>
                        <li>
                            <a href="{{ url('admin/dashboard')}}">
                                <span class="nav-icon text-primary">
                                    <i data-feather="home"></i>
                                </span> 
                                <span class="nav-text">Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="">
                                <span class="nav-icon text-info">
                                    <i data-feather="package"></i>
                                </span> 
                                <span class="nav-text">Produk</span> 
                                <span class="nav-caret"></span>
                            </a>
                            <ul class="nav-sub nav-mega">
                                <li>
                                    <a href="{{ url('admin/products') }}" id="all_product_sidebar" class="">
                                        <span class="nav-text">Semua Produk</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/product/add') }}" class="" id="add_product_sidebar">
                                        <span class="nav-text">Tambah Produk</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/product/category') }}" class="" id="category_product_sidebar">
                                        <span class="nav-text">Kategori Produk</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="">
                                <span class="nav-icon text-info">
                                    <i data-feather="book"></i>
                                </span> 
                                <span class="nav-text">Blogs</span> 
                                <span class="nav-caret"></span>
                            </a>
                            <ul class="nav-sub nav-mega">
                                <li>
                                    <a href="{{ url('admin/blogs') }}" class="" id="all_blog_sidebar">
                                        <span class="nav-text">Semua Blog</span>
                                    </a>
                                </li>    
                                <li>
                                    <a href="{{ url('admin/blog/add') }}" class="" id="add_blog_sidebar">
                                        <span class="nav-text">Tambah Blog</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/blog/category') }}" class="" id="all_category_blog_sidebar">
                                        <span class="nav-text">Kategori Blog</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/blog/tag') }}" class="" id="all_tag_blog_sidebar">
                                        <span class="nav-text">Tags Blog</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="">
                                <span class="nav-icon text-info">
                                    <i data-feather="folder"></i>
                                </span> 
                                <span class="nav-text">Halaman</span> 
                                <span class="nav-caret"></span>
                            </a>
                            <ul class="nav-sub nav-mega">
                                <li>
                                    <a href="{{ url('admin/pages') }}" class="" id="all_page_sidebar">
                                        <span class="nav-text">Semua Halaman</span>
                                    </a>
                                </li>    
                                <li>
                                    <a href="{{ url('admin/pages/add') }}" class="" id="add_page_sidebar">
                                        <span class="nav-text">Tambah Halaman</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="">
                                <span class="nav-icon text-info">
                                    <i data-feather="book-open"></i>
                                </span> 
                                <span class="nav-text">Kontak Admin</span> 
                                <span class="nav-caret"></span>
                            </a>
                            <ul class="nav-sub nav-mega">
                                <li>
                                    <a href="{{ url('admin/contact_master') }}"  class="" id="all_contact_sidebar">
                                        <span class="nav-text">Semua Kontak</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/contact_master/add') }}" class="" id="add_contact_sidebar">
                                        <span class="nav-text">Tambah Kontak</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="">
                                <span class="nav-icon text-warning">
                                    <i data-feather="message-square"></i>
                                </span> 
                                <span class="nav-text">Testimonial</span> 
                                <span class="nav-caret"></span>
                            </a>
                            <ul class="nav-sub nav-mega">
                                <li>
                                    <a href="{{ url('admin/testimonial') }}" class="" id="all_testimonial_sidebar">
                                        <span class="nav-text">Semua Testimonial</span>
                                    </a>
                                </li>    
                                <li>
                                    <a href="{{ url('admin/testimonial/add') }}" class="" id="add_testimonial_sidebar">
                                        <span class="nav-text">Tambah Testimonial</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{ url('admin/messages')}}" id="all_message_sidebar">
                                <span class="nav-icon text-warning">
                                    <i data-feather="message-circle"></i>
                                </span> 
                                <span class="nav-text">Pesan</span> 
                                <span class="nav-badge">
                                    <b class="badge-circle xs text-warning"></b>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="">
                                <span class="nav-icon text-info">
                                    <i data-feather="settings"></i>
                                </span>                                 
                                <span class="nav-text">Pengaturan</span> 
                                <span class="nav-caret"></span>
                            </a>
                            <ul class="nav-sub nav-mega">
                                <li>
                                    <a href="{{ url('admin/admins') }}" class="" id="all_user_sidebar">
                                        <span class="nav-text">Pengguna</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/kontak-kami') }}" class="" id="all_user_sidebar">
                                        <span class="nav-text">Kontak Kami</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/general') }}" class="" id="all_general_sidebar">
                                        <span class="nav-text">Umum</span>
                                    </a>
                                </li>    
                                <li>
                                    <a href="{{ url('admin/menu') }}" class="" id="all_menu_sidebar">
                                        <span class="nav-text">Menu</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        <span class="nav-text">Sign Out</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>