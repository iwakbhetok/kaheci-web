<div id="header" class="page-header">
@if ($message = Session::get('success'))
<div class="alert alert-success" style="margin-left:20px;margin-right:20px;margin-top:20px;" role="alert"><i data-feather="check"></i> <span class="mx-2">{{ $message }}</span>
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
@endif
@if ($message = Session::get('error'))
<div class="alert alert-danger" style="margin-left:20px;margin-right:20px;margin-top:20px;" role="alert"><i data-feather="x"></i> <span class="mx-2">{{ $message }}</span>
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
@endif
@if ($message = Session::get('info'))
<div class="alert alert-info" style="margin-left:20px;margin-right:20px;margin-top:20px;" role="alert"><i data-feather="info"></i> <span class="mx-2">{{ $message }}</span>
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
@endif
    <div class="navbar navbar-expand-lg">
        <a href="{{ url('admin/dashboard') }}" class="navbar-brand d-lg-none">
        <img src="{{ asset('images/logo.png') }}" alt="">
         <span class="hidden-folded d-inline l-s-n-1x d-lg-none">Kaheci</span></a>
        <ul class="nav navbar-menu order-1 order-lg-2">
            <li class="nav-item d-none d-sm-block"><a class="nav-link px-2" data-toggle="fullscreen" data-plugin="fullscreen"><i data-feather="maximize"></i></a></li>
            <li class="nav-item dropdown"><a class="nav-link px-2" data-toggle="dropdown"><i data-feather="settings"></i></a>
                <div class="dropdown-menu dropdown-menu-center mt-3 w animate fadeIn">
                    <div class="setting px-3">
                        <div class="mb-2 text-muted"><strong>Setting:</strong></div>
                        <div class="mb-3" id="settingLayout">
                            <label class="ui-check ui-check-rounded my-1 d-block">
                                <input type="checkbox" name="stickyHeader"> <i></i> <small>Sticky header</small></label>
                            <label class="ui-check ui-check-rounded my-1 d-block">
                                <input type="checkbox" name="stickyAside"> <i></i> <small>Sticky aside</small></label>
                            <label class="ui-check ui-check-rounded my-1 d-block">
                                <input type="checkbox" name="foldedAside"> <i></i> <small>Folded Aside</small></label>
                            <label class="ui-check ui-check-rounded my-1 d-block">
                                <input type="checkbox" name="hideAside"> <i></i> <small>Hide Aside</small></label>
                        </div>
                        <div class="mb-2 text-muted"><strong>Color:</strong></div>
                        <div class="mb-2">
                            <label class="radio radio-inline ui-check ui-check-md">
                                <input type="radio" name="bg" value=""> <i></i></label>
                            <label class="radio radio-inline ui-check ui-check-color ui-check-md">
                                <input type="radio" name="bg" value="bg-dark"> <i class="bg-dark"></i></label>
                        </div>
                        
                    </div>
                </div>
            </li>
            <!--li class="nav-item dropdown"><a class="nav-link px-2 mr-lg-2" data-toggle="dropdown"><i data-feather="bell"></i> <span class="badge badge-pill badge-up bg-primary">8</span></a>
                <div class="dropdown-menu dropdown-menu-right mt-3 w-md animate fadeIn p-0">
                    <div class="scrollable hover" style="max-height: 250px">
                        <div class="list list-row">
                            <div class="list-item" data-id="10">
                                <div><a href="dashboard.html#"><span class="w-32 avatar gd-danger"><img src="{{ asset('admin/assets/img/a10.jpg') }}" alt="."></span></a></div>
                                <div class="flex">
                                    <div class="item-feed h-2x">Developers of <a href="dashboard.html#">@iAI</a>, the AI assistant based on Free Software</div>
                                </div>
                            </div>
                            <div class="list-item" data-id="6">
                                <div><a href="dashboard.html#"><span class="w-32 avatar gd-danger"><img src="{{ asset('admin/assets/img/a6.jpg') }}" alt="."></span></a></div>
                                <div class="flex">
                                    <div class="item-feed h-2x">Just saw this on the <a href="dashboard.html#">@eBay</a> dashboard, dude is an absolute unit.</div>
                                </div>
                            </div>
                            <div class="list-item" data-id="16">
                                <div><a href="dashboard.html#"><span class="w-32 avatar gd-info">F</span></a></div>
                                <div class="flex">
                                    <div class="item-feed h-2x">What if AI Could Uber the Health Care Industry?</div>
                                </div>
                            </div>
                            <div class="list-item" data-id="12">
                                <div><a href="dashboard.html#"><span class="w-32 avatar gd-info">A</span></a></div>
                                <div class="flex">
                                    <div class="item-feed h-2x"><a href="dashboard.html#">Support</a> team updated the status</div>
                                </div>
                            </div>
                            <div class="list-item" data-id="20">
                                <div><a href="dashboard.html#"><span class="w-32 avatar gd-warning">G</span></a></div>
                                <div class="flex">
                                    <div class="item-feed h-2x"><a href="dashboard.html#">@Netflix</a> hackathon</div>
                                </div>
                            </div>
                            <div class="list-item" data-id="4">
                                <div><a href="dashboard.html#"><span class="w-32 avatar gd-success"><img src="{{ asset('admin/assets/img/a4.jpg') }}" alt="."></span></a></div>
                                <div class="flex">
                                    <div class="item-feed h-2x">Big News! Introducing <a href="dashboard.html#">NextUX</a> Enterprise 2.1 - additional #Windows Server support</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex px-3 py-2 b-t">
                        <div class="flex"><span>6 Notifications</span></div><a href="page.setting.html">See all <i class="fa fa-angle-right text-muted"></i></a></div>
                </div>
            </li-->
            <li class="nav-item dropdown"><a href="#" data-toggle="dropdown" class="nav-link d-flex align-items-center px-2 text-color"><span class="avatar w-24" style="margin: -2px"><img src="{{ asset('admin/assets/img/avatar.jpg') }}" alt="..."></span></a>
                <div class="dropdown-menu dropdown-menu-right w mt-3 animate fadeIn"><a class="dropdown-item" href="#"><span>
                @if(Auth::guard('admin'))
                    {{ ucwords(Auth::guard('admin')->user()->name) }}
                @else
                    Anonym
                @endif
                </span> </a>
                    
                    <a class="dropdown-item" href="{{ route('admin.logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Sign out') }}
                        </a>

                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                </div>
            </li>
            <li class="nav-item d-lg-none"><a href="#" class="nav-link px-2" data-toggle="collapse" data-toggle-class data-target="#navbarToggler"><i data-feather="search"></i></a></li>
            <li class="nav-item d-lg-none"><a class="nav-link px-1" data-toggle="modal" data-target="#aside"><i data-feather="menu"></i></a></li>
        </ul>
    </div>
</div>