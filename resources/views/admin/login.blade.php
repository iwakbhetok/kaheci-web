<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Login | {{ config('app.name', 'Kaheci - Web Application') }}</title>
    <meta name="description" content="Responsive, Bootstrap, BS4">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <link rel="stylesheet" href="{{ asset('admin/assets/css/site.min.css') }}">
</head>

<body class="layout-row">
    <div class="flex">
        <div class="w-xl w-auto-sm mx-auto py-5">
            <div class="p-4 d-flex flex-column h-100">
                <a href="{{ url('/') }}" class="navbar-brand align-self-center">
                    <img src="{{ asset('images/logo.png') }}" alt="">
                    <span class="hidden-folded d-inline l-s-n-1x align-self-center">Kaheci</span></a>
            </div>
            <div class="card">
                <div id="content-body">
                    <div class="p-3 p-md-5">
                        <h5>Welcome back</h5>
                        <p><small class="text-muted">Login to manage your account</small></p>
                        <form class="" role="form" method="POST" action="{{ route('admin.login.submit') }}">
                            @csrf
                            <div class="form-group">
                                <label>Email</label>
                                <!--input type="email" class="form-control" placeholder="Enter email"-->
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <!--input type="password" class="form-control" placeholder="Password"-->
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                                <!--div class="my-3 text-right"><a href="forgot-password.html" class="text-muted">Forgot password?</a></div-->

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="checkbox mb-3">
                                <label class="ui-check">
                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}><i></i> Remember me</label>
                            </div>
                            <button type="submit" class="btn btn-primary mb-4">Sign in</button>
                            <!--div>Do not have an account? <a href="signup.1.html" class="text-primary">Sign up</a></div-->
                        </form>
                    </div>
                </div>
            </div>
            <div class="text-center text-muted">2019 &copy; Copyright. Kaheci</div>
        </div>
    </div>
    <script src="{{ asset('admin/assets/js/site.min.js') }}"></script>
</body>

</html>