@extends('layouts.admin')
@section('content')

<div class="page-hero page-container" id="page-hero">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Tentang Kami</h2><small class="text-muted">Format data into a table</small></div>
            <div class="flex"></div>
            <div><a href="{{ url('/admin/blog/add') }}" class="btn btn-md btn-primary"><span class="d-none d-sm-inline mx-1">Ubah Tentang Kami</span> <i data-feather="arrow-right"></i></a></div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Tentang Kami</h5>
                    </div>
                    <div class="card-body">
                        <p class="card-text">{{ $about->description }}</p>
                        <object width="400" 
                        data="{{ $about->yt_video_url }}">
                        </object>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection