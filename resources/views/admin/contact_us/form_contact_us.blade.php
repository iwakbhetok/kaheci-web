@extends('layouts.admin')
@section('content')


<div class="page-hero page-container" id="page-hero" data-plugin="general">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Setting Kontak Kami</h2><small class="text-muted">Konfigurasi Info Kontak Kami</small></div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        @if ($errors->any())
            <div class="row alert alert-danger">
                <div class="col-sm-10">
                    <ul style="margin: auto;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-2">                
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif
        <div id="accordion">
            <p class="text-muted"><strong>Info Kontak Kami</strong></p>
            <div class="card">

                <div class="d-flex align-items-center px-4 py-3 b-t pointer" data-toggle="collapse" data-parent="#accordion" data-target="#c_1"><i data-feather="airplay"></i>
                    <div class="px-3">
                        <div>Kontak Kami</div>
                    </div>
                    <div class="flex"></div>
                    <div><i data-feather="chevron-right"></i></div>
                </div>
                <div class="collapse p-4" id="c_1">
                    <p style="color: red"><b>Note:</b> Kolom dengan tanda (<span style="color: red">*</span>) tidak boleh kosong.</p>
                    <form role="form" action="{{ url('/admin/kontak-kami/save') }}" method="POST" enctype="multipart/form-data">
                     @csrf
                        <div class="form-group">
                            <label>Alamat Lengkap</label>
                            <input type="hidden" name="id" value="{{ isset($items->id) ? $items->id :'' }}">
                            <textarea name="address" name="address" class="form-control" rows="6" data-minwords="6" placeholder="Alamat lengkap">{{ isset($items->address) ? $items->address : '' }}</textarea>

                        </div>
                        <div class="form-group">
                            <label>No Telp</label>
                            <input autocomplete="off" type="text" name="phone" class="form-control" placeholder="087899999922" value="{{ isset($items->phone) ? $items->phone :'' }}">
                        </div>
                        <div class="form-group">
                            <label>E-mail</label>
                            <input autocomplete="off" name="email" type="email" class="form-control" placeholder="admin@gmail.com" value="{{ isset($items->email) ? $items->email : '' }}">
                        </div>
                        <div class="form-group">
                            <label>Info Toko</label>
                            <input autocomplete="off" name="open_store" type="text" class="form-control" placeholder="Senin - Jumat : 09.00 - 17.00" value="{{ isset($items->open_store) ? $items->open_store : '' }}">
                        </div>
                        <button type="submit" class="btn btn-primary mt-2">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
