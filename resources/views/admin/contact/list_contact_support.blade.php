@extends('layouts.admin')
@section('content')
<div class="page-hero page-container" id="page-hero" data-plugin="blogs">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Kontak Whatsapp/Line</h2><small class="text-muted">Data Kontak Whatsapp/Line</small></div>
            <div class="flex"></div>
            <div><a href="{{ url('/admin/contact_master/add') }}" class="btn btn-md btn-primary"><span class="d-none d-sm-inline mx-1">Tambah Kontak Whatsapp/Line</span> <i data-feather="arrow-right"></i></a></div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        <div class="mb-5">
            <div class="toolbar">
                <form class="flex">
                    <div class="input-group">
                        <input autocomplete="off" type="text" class="form-control form-control-theme form-control-sm search" placeholder="Search" name="q" value="{{ app('request')->input('q') }}" > <span class="input-group-append"><button class="btn btn-white no-border btn-sm" type="submit"><span class="d-flex text-muted"><i data-feather="search"></i></span></button>
                        </span>
                    </div>
                </form>
                <form onclick="return confirm('Anda yakin ingin menghapus item ini?');" action="{{ url('admin/contact_master/removeall/') }}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="btn-group">
                        <button class="btn btn-sm btn-icon btn-white" data-toggle="tooltip" title="Trash"><i data-feather="trash" class="text-muted"></i></button>
                    </div>
            </div>
            <div class="table-responsive">
                <table class="table table-theme table-row v-middle">
                    <thead>
                        <tr>
                            <th style="width:20px">
                                <label class="ui-check m-0">
                                    <input type="checkbox" id="select-all"><i></i></label>
                            </th>
                            <th class="text-muted" data-toggle-class="asc">Nama</th>
                            <th class="text-muted"><span class="d-none d-sm-block">WA/Line</span></th>
                            <th class="text-muted"><span class="d-none d-sm-block">Tipe</span></th>
                            <th class="text-muted"><span class="d-none d-sm-block">Status</span></th>
                            <th class="text-muted"><span class="d-none d-sm-block">Created</span></th>
                            <th style="width:50px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ContactSupport as $item)
                        <tr class="v-middle" data-id="{{ $item->id }}">
                            <td>
                                <label class="ui-check m-0">
                                    <input type="checkbox" name="id[]" value="{{ $item->id }}"> <i></i></label>
                            </td>
                            <td class="flex"><a href="#" class="item-title text-color">{{ $item->name }}</a>
                            </td>
                            <td class="flex"><a href="#" class="item-title text-color">{{ $item->value }}</a>
                            </td>
                            <td><span class="item-amount d-none d-sm-block text-sm">{{ ($item->type == "wa") ? "Whatsapp" : "Line" }}</span></td>
                            <td><span class="item-amount d-none d-sm-block text-sm">{{ ($item->status == "1") ? "Aktif" : "Tidak Aktif" }}</span></td>
                            <td><span class="item-amount d-none d-sm-block text-sm [object Object]">{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y')}}</span></td>
                            <td>
                                <div class="item-action dropdown">
                                    <a href="#" data-toggle="dropdown" class="text-muted">
                                        <i data-feather="more-vertical"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                        <a class="dropdown-item edit" href="{{ url('admin/contact_master/edit/'.$item->id) }}">Edit </a>
                                        <a class="dropdown-item trash" href="{{ url('admin/contact_master/remove/'.$item->id) }}" onclick="return confirm('Anda yakin ingin menghapus item ini?');">Delete</a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </form>
            <div class="d-flex">
            {!! $ContactSupport->render() !!}
            <small class="text-muted py-2 mx-2">Total <span id="count">{{ number_format($ContactSupport->total(), 0) }}</span> items</small></div>
        </div>
    </div>
</div>
@endsection

@section('plugin_js')
    <script type="text/javascript">
        //action onclick checked all
        $('#select-all').click(function(event) {
            if(this.checked) {
                $(':checkbox').each(function() {
                    this.checked = true;                        
                });
            } else {
                $(':checkbox').each(function() {
                    this.checked = false;                       
                });
            }
        });        
    </script>
@endsection
