@extends('layouts.admin')
@section('content')

<div class="page-hero page-container" id="page-hero">
    <div class="padding d-flex">
        <div class="page-title">
            <h2 class="text-md text-highlight">Tambah Kontak</h2><small class="text-muted">tambahkan kontak</small></div>
            <div class="flex"></div>
            <div><a href="{{ url('/admin/contact_master') }}" class="btn btn-md btn-danger"><i data-feather="arrow-left"></i><span class="d-none d-sm-inline mx-1">Batal</span></a></div>
    </div>
</div>
<div class="page-content page-container" id="page-content">
    <div class="padding">
        @if ($errors->any())
            <div class="row alert alert-danger">
                <div class="col-sm-10">
                    <ul style="margin: auto;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-2">                
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif

        <form action="{{ url('/admin/contact_master/update') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="text-left pt-2">
                                <p style="color: red"><b>Note:</b> Kolom dengan tanda (<span style="color: red">*</span>) tidak boleh kosong.</p>
                            </div>                            
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <input type="hidden" name="id" value="{{ $ContactSupport->id }}">
                                    <label>Nama<span style="color: red">*</span></label>
                                    <input autocomplete="off" type="text" name="title" class="form-control" placeholder="title in here..." required value="{{ $ContactSupport->name }}">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>No Whatsapp/Line<span style="color: red">*</span></label>
                                    <input autocomplete="off" type="text" name="value" class="form-control" value="{{ $ContactSupport->value }}" required>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Tipe<span style="color: red">*</span></label>  
                                    <select class="custom-select" name="type" required>
                                        <option value="">Pilih tipe</option>
                                        <option value="wa" {{ ($ContactSupport->type == "wa") ? "selected='selected'" : "" }}>Whatsapp</option>
                                        <option value="line" {{ ($ContactSupport->type == "line") ? "selected='selected'" : "" }}>Line</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Status<span style="color: red">*</span></label>  
                                    <select class="custom-select" name="status" required>
                                        <option value="">Pilih status</option>
                                        <option value="1" {{ ($ContactSupport->status == 1) ? "selected='selected'" : "" }}>Aktif</option>
                                        <option value="0" {{ ($ContactSupport->status == 0) ? "selected='selected'" : "" }}>Tidak Aktif</option>
                                    </select>
                                </div>

                            </div>
                            <div class="text-right pt-2">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection

