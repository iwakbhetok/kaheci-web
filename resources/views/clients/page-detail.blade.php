@extends('layouts.layout')
@section('content')
@include('components.highlight-page')

<div class="uk-text-center" uk-grid>
    
    <div class="uk-width-1-3@m uk-visible@m">
        <div class="uk-card">
            <img class="lazy" src="{{ asset('images/left-image-about.png') }}" alt="">
        </div>
    </div>
    <div class="uk-width-1-3@m uk-hidden@m">
        <div class="uk-card">
            <img class="lazy" src="{{ asset('images/bg-about-us.jpg') }}" alt="">
        </div>
    </div>
    <div class="uk-grid-small uk-width-expand@m uk-visible@m">
        <div class="uk-card uk-card-body uk-text-justify">
            <br><br>
            <h1>{{ $pages->name }}</h1>
            {!! $pages->description !!}
        </div>
    </div>

    <div class="uk-grid-small uk-width-expand@m uk-hidden@m">
        <div class="uk-card uk-card-body uk-text-justify">
            <h4>{{ $pages->name }}</h4>
            {!! $pages->description !!}
        </div>
    </div>
</div>

@endsection