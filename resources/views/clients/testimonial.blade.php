@extends('layouts.layout')
@section('content')
@include('components.highlight-page')

@if(isset($testimoni))
<div class="uk-container uk-padding-large">
    <ul class="uk-flex-center" uk-tab>
        <li><a href="#">Testimoni Pembeli</a></li>
    </ul>

    <ul class="uk-switcher uk-margin">
        <li>
            <div class="uk-child-width-1-4@m uk-child-width-1-2 uk-text-center" uk-grid>
                @foreach($testimoni as $items)
                <div>
                    <div class="uk-card">
                        <img src="{{ asset('images/testimoni/'.$items->slug.'/'.$items->image) }}" alt="{{ $items->name }}">
                    </div>
                </div>
                @endforeach
            </div>
        </li>
{{--         <li>
            <div class="uk-child-width-1-4@m uk-child-width-1-2 uk-text-center" uk-grid>
                @foreach($testimoni as $items)
                <div>
                    <div class="uk-card">
                        <img src="{{ asset('images/testimoni/'.$items->slug.'/'.$items->image) }}" alt="{{ $items->name }}">
                    </div>
                </div>
                @endforeach
            </div>
        </li> --}}
    </ul>

</div>
 
<div class="uk-section uk-section-default uk-text-center uk-flex uk-flex-center">
    <div class="uk-container">


        <div class="uk-child-width-1-1@m uk-child-width-1-1" uk-grid>
            <div>
            @if ($testimoni->lastPage() > 1)
            <ul class="uk-pagination">
                <li class="{{ ($testimoni->currentPage() == 1) ? 'disabled' : '' }}">
                    <a href="{{ $testimoni->url(1) }}">Previous</a>
                </li>
                @for ($i = 1; $i <= $testimoni->lastPage(); $i++)
                    <li class="{{ ($testimoni->currentPage() == $i) ? 'uk-active' : '' }}">
                        <a href="{{ $testimoni->url($i) }}">{{ $i }}</a>
                    </li>
                @endfor
                <li class="{{ ($testimoni->currentPage() == $testimoni->lastPage()) ? 'disabled' : '' }}">
                    <a href="{{ $testimoni->url($testimoni->currentPage()+1) }}" >Next</a>
                </li>
            </ul>
            @endif

            </div>
        </div>

    </div>
</div>

@endif

@endsection