@extends('layouts.layout')
@section('content')
@include('components.highlight-page')

<div class="uk-section uk-section-default">
    <div class="uk-container">

<div class="uk-text-center" uk-grid>
    <div class="uk-width-1-3@m uk-visible@m">
        <div style="border:1px solid #eaeaea;margin:30px;">
            <div class="uk-card uk-padding">
                <h4 class="uk-text-left">Search</h4>
                <div class="uk-margin">
                    <form method="POST" id="searchProduct" action="{{ route('search.product') }}">
                    {{ csrf_field() }}
                        <div class="uk-inline">
                            <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: search"></span>
                            <input class="uk-input" type="text" name="keyword" placeholder="Search here...">
                        </div>
                    </form>
                </div>
                <hr>
                <h4 class="uk-text-left">Shop by Categories</h4>
                <div class="uk-width-1-1@s uk-width-1-1@m uk-text-left">
                    <ul class="uk-nav-default uk-nav-parent-icon" uk-nav="multiple: true" style="font-size:1em;">
                    {!! create_menu_category($category) !!}
                    </ul>
                </div>
                <hr>
                <h4 class="uk-text-left">Refine by</h4>
                <div class="uk-text-center" uk-grid>
                    <div class="uk-width-expand@m">
                        <div class="uk-card uk-text-left">
                            <label><input class=" premium" name="types" type="radio" value="premium"> Unggulan</label><br><br>
                            <label><input class=" sale" name="types" type="radio" value="sale"> Dijual</label><br><br>
                            <label><input class="new" name="types" type="radio" value="new"> Terbaru</label><br><br>
                            <label><input class=" available" name="types" type="radio" value="available"> Tersedia</label><br><br>
                            <label><input class="coming_soon" name="types" type="radio" value="coming soon"> Segera</label>
                        </div>
                    </div>
                    <div class="uk-width-auto@m">
                        <div class="uk-card">
                            <span class="uk-badge">{{ $get_product_type['premium'] }}</span><br><br>    
                            <span class="uk-badge">{{ $get_product_type['sale'] }}</span><br><br>
                            <span class="uk-badge">{{ $get_product_type['new'] }}</span><br><br>
                            <span class="uk-badge">{{ $get_product_type['available'] }}</span><br><br>
                            <span class="uk-badge">{{ $get_product_type['coming_soon'] }}</span><br>
                        </div>
                    </div>
                </div>
                <hr>
                <h4 class="uk-text-left">Colour</h4>
                <div class="color_wrapper_container">
                @foreach($get_product_color as $key => $color)
                    <div class="color_wrapper">
                        <a href="#" data-id="{{ $key }}" class="uk-margin-right color-palette">
                            <div class="color_item" style="background-color:{{ $key }}; height: 30px; width:30px; border: solid 1px #eaeaea;"></div>
                        </a>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
    
    <div class="uk-width-expand@m" style="padding-left:15px;">
        <div class="uk-card uk-card-body">
            <!-- SORT ENGINE -->
            <div class="uk-text-center" uk-grid>
                <div class="uk-width-auto@m uk-visible@m">
                    <div class="uk-card">
                        <a href="#" id="gridViewDesktop" uk-icon="icon: thumbnails"></a>
                        <a href="#" id="listViewDesktop" uk-icon="icon: list"></a>
                    </div>
                </div>
                <div class="uk-width-expand@m uk-visible@m">
                    <div class="uk-card uk-text-small">
                        <div class="uk-position-center-left"><br>
                        @if($products->total() == 0)
                            Tak ada hasil pencarian
                        @else
                            Menampilkan <span id="from">{{ $products->firstItem() }}</span>  - <span id="to">{{ $products->total() }}</span>  dari <span id="total">{{ $products->total() }}</span>  hasil
                        @endif
                        </div>
                    </div>
                </div>
                @if($products->total() == 0)
                @elseif($products->total() > 25)
                <div class="uk-width-1-5@m uk-visible@m">
                    <div class="uk-card">
                        <div class="uk-child-width-expand@s uk-text-center" uk-grid>
                            <div>
                                <div class="uk-card uk-text-right uk-text-small uk-position-center-left">Lihat</div>
                            </div>
                            <div>
                                <div class="uk-card uk-text-small">
                                    <select class="uk-select" id="form-horizontal-select">
                                        <option>25</option>
                                        <option>50</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                @endif
                <div class="uk-width-1-3@m">
                    <div class="uk-card">
                        <div class="uk-child-width-expand@s uk-text-center uk-visible@m" uk-grid>
                            <div>
                                <div class="uk-card uk-text-center uk-text-small uk-position-center-left">Sortir:</div>
                            </div>
                            <div>
                                <div class="uk-card">
                                    <select id="sort" class="uk-select uk-text-small" id="form-horizontal-select">
                                        <option>Choose</option>
                                        <option value="a_z">A-Z</option>
                                        <option value="z_a">Z-A</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="uk-child-width-expand@s uk-hidden@m" uk-grid>
                            <div>
                            <div class="uk-card">
                                <form method="POST" id="searchProduct" action="{{ route('search.product') }}">
                                {{ csrf_field() }}
                                    <div class="">
                                        <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: search"></span>
                                        <input class="uk-input" type="text" name="keyword" placeholder="Search here...">
                                    </div>
                                </form>
                            </div>
                            </div>
                            <div>
                                <div class="uk-card">
                                    <select class="uk-select listCategory">
                                        <option value="">Shop by Categories</option>
                                    @foreach($category as $item)
                                        <option id="categoryItem" value="{{ $item->slug }}">{{ $item->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div>
                                <div class="uk-card uk-text-left uk-padding-small label-show-result-mobile">
                                <span uk-icon="icon: grid"></span>
                                Menampilkan <span id="from">{{ $products->firstItem() }}</span>  - <span id="to">{{ $products->total() }}</span>  dari <span id="total">{{ $products->total() }}</span>  hasil
                                </div>
                            </div>
                        </div>
                        
  

                    </div>
                </div>
            </div>
            <!-- END SORT ENGINE -->
            
        <!-- PRODUCT LIST-->
        <div id="listContainer">
            <div class="product_list_container">
                <ul>
                    @foreach($products as $product)
                    <li data-sort="{{ $product->name }}">
                    <div class="product_item_wrapper">
                        <div class="product_image_wrapper">
                            <a href="{{ url('/produk-detail/'. $product->slug) }}">
                                <img src="{{ asset('files/products/'. $product->slug.'/thumbs/'.$product->main_image .'') }}" alt="">
                            </a>
                        </div>
                        <div class="product_description_wrapper">
                            <div class="product_name">
                                {{ $product->name }}
                            </div>
                            <div class="product_category">
                                {{ $product->category_name }}
                            </div>
                            <div class="product_price">
                                Rp. {{ number_format($product->unit_price, 0) }}
                            </div>
                        </div>
                        <div class="product_detail_wrapper">
                            <a class="uk-button btn-viewDetail" href="{{ url('/produk-detail/'. $product->slug) }}">View Detail</a>
                        </div>
                    </div>
                    </li>
                    @endforeach
                </ul>
                
            </div>
        </div>
        

        <div id="gridContainer">
            <div class="products_grid_container">
                <ul>
                @foreach($products as $product)
                    <li data-sort="{{ $product->name }}">
                        <div class="product_grid_item_wrapper">
                            <div class="product_grid_image_wrapper">
                            <a href="{{ url('/produk-detail/'. $product->slug) }}">
                            <div class="container-prod">
                                <img src="{{ asset('files/products/'. $product->slug.'/thumbs/'.$product->main_image .'') }}">
                                <div class="overlay_wrapper">
                                    <div class="overlay-prod">
                                        <div class="text-prod">View Detail</div>
                                    </div>
                                </div>
                            </div>
                            </a>
                            </div>
                            <div class="product_grid_description_wrapper">
                                <div class="prod_grid_category_name">
                                    <span class="uk-text-emphasis uk-margin-remove-top">{{ $product->category_name }}</span>
                                </div>
                                <div class="prod_grid_name">
                                    <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">{{ $product->name }} </p>
                                </div>
                                <div class="prod_grid_price">
                                    <p class="text-color-one uk-margin-remove-top">Rp. {{ number_format($product->unit_price, 0) }}</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>

            
            <div class="" uk-grid>
                <div class="uk-width-expand@m">
                    <div class="uk-card uk-pagination uk-flex-center">
                    {{ $products->links() }}
                    </div>
                </div>
                <div class="uk-width-auto@m">
                    <div class="uk-card" uk-scrollspy="cls: uk-animation-slide-right; repeat: true">
                        <div class="circle-totop">
                            <a href="#" uk-icon="arrow-up" uk-scroll></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

</div>
</div>

@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        $('.listCategory').on('change', function(e){
            window.location.href = "{{ URL::to('category')}}/" + $(this).val()
        }); 

        var defaultList = $('.product_list_container ul > li').get();
        var defaultGrid = $('.products_grid_container ul > li').get();
        
        $("#gridContainer").css({'display': 'block'});
        $("#listContainer").css({'display': 'none'});

        // choose type view
        $("#gridViewDesktop").click(function(){
            $("#gridContainer").css({'display': 'block'});
            $("#listContainer").css({'display': 'none'});
            return false;
        });

        $("#listViewDesktop").click(function(){
            $("#listContainer").css({'display': 'block'});
            $("#gridContainer").css({'display': 'none'});
            return false;
        });


        var input = document.getElementById("searchProduct");

        // Execute a function when the user releases a key on the keyboard
        input.addEventListener("keyup", function(event) {
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) {
            // Cancel the default action, if needed
            event.preventDefault();
            // Trigger the button element with a click
            $(input).submit(); // your form has an id="form"
        }
        });
    });


$(".premium").click(function(e){
    $(".products_grid_container").html("");
    $(".product_list_container").html("");
    $.ajax({
        type: 'GET',
        url: "{{ route('ajax.premium') }}",
        success: function (data) {
            $("#from").html('1');
            $("#to").html(data.products.length);
            $("#total").html(data.products.length);
        
            var content_html = "<ul>";
            $.each(data.products, function (key, val) {
                content_html += '<li data-sort="' + val.name + '">'
                content_html += '<div class="product_grid_item_wrapper">' 
                content_html += '<div class="product_grid_image_wrapper">'
                content_html += '<a href="{{ url("produk-detail/") }}/' + val.slug+ '">'
                content_html += '<div class="container-prod">'
                content_html += '<img src="{{ asset("files/products") }}/'+ val.slug + '/thumbs/' + val.main_image + '" >'
                content_html += '<div class="overlay-prod">'
                content_html += '<div class="text-prod">View Detail</div>'
                content_html += ' </div>'
                content_html += '</div>'
                content_html += '</a>'
                content_html += '</div>'
                content_html += '<div class="product_grid_description_wrapper">'
                content_html += '<div class="prod_grid_category_name">'
                content_html += '<span class="uk-text-emphasis uk-margin-remove-top">' + val.category_name +'</span>'
                content_html += '</div>'
                content_html += '<div class="prod_grid_name">'
                content_html += '<p class="product-name uk-margin-remove-top uk-margin-remove-bottom">'+ val.name +' </p>'
                content_html += '</div>'
                content_html += '<div class="prod_grid_price">'
                content_html += '<p class="text-color-one uk-margin-remove-top">Rp.'+ formatRupiah(val.unit_price) + '</p>'
                content_html += '</div>'
                content_html += '</div>'
                content_html += '</div>'
                content_html += '</li>'
            });
            content_html += "</ul>";
            $(".products_grid_container").append(content_html); //// For Append

            var list_product_html = "<ul>";
            $.each(data.products, function (key, val) {
                list_product_html += '<li>'
                list_product_html += '<div class="product_item_wrapper">'
                list_product_html += '<div class="product_image_wrapper">'
                list_product_html += '<a href="{{ url("produk-detail/") }}/' + val.slug+ '">'
                list_product_html += '<img src="{{ asset("files/products") }}/'+ val.slug + '/thumbs/' + val.main_image + '" class="">'
                list_product_html += '</a>'
                list_product_html += '</div>'
                list_product_html += '<div class="product_description_wrapper">'
                list_product_html += '<div class="product_name">' + val.name
                list_product_html += '</div>'
                list_product_html += '<div class="product_category">' + val.category_name 
                list_product_html += '</div>'
                list_product_html += '<div class="product_price"> Rp. ' + formatRupiah(val.unit_price)
                list_product_html += '</div>'
                list_product_html += '</div>'
                list_product_html += '<div class="product_detail_wrapper">'
                list_product_html += '<a class="uk-button btn-viewDetail" href="{{ url('/produk-detail') }}/' + val.slug + '">View Detail</a>'
                list_product_html += '</div>'
                list_product_html += '</div>'
                list_product_html += '</li>'
            });
            list_product_html += "</ul>";
            $(".product_list_container").append(list_product_html);
            var defaultGrid = $('.products_grid_container ul > li').get();
  
        },
        error: function() { 
            console.log(data);
        }
    });
});

$(".sale").click(function(e){
    $(".products_grid_container").html("");
    $(".product_list_container").html("");
    $.ajax({
        type: 'GET',
        url: "{{ route('ajax.sale') }}",
        success: function (data) {
            $("#from").html('1');
            $("#to").html(data.products.length);
            $("#total").html(data.products.length);
            
            var content_html = "<ul>";
            $.each(data.products, function (key, val) {
                content_html += '<li data-sort="' + val.name + '">'
                content_html += '<div class="product_grid_item_wrapper">' 
                content_html += '<div class="product_grid_image_wrapper">'
                content_html += '<a href="{{ url("produk-detail/") }}/' + val.slug+ '">'
                content_html += '<div class="container-prod">'
                content_html += '<img src="{{ asset("files/products") }}/'+ val.slug + '/thumbs/' + val.main_image + '" >'
                content_html += '<div class="overlay-prod">'
                content_html += '<div class="text-prod">View Detail</div>'
                content_html += ' </div>'
                content_html += '</div>'
                content_html += '</a>'
                content_html += '</div>'
                content_html += '<div class="product_grid_description_wrapper">'
                content_html += '<div class="prod_grid_category_name">'
                content_html += '<span class="uk-text-emphasis uk-margin-remove-top">' + val.category_name +'</span>'
                content_html += '</div>'
                content_html += '<div class="prod_grid_name">'
                content_html += '<p class="product-name uk-margin-remove-top uk-margin-remove-bottom">'+ val.name +' </p>'
                content_html += '</div>'
                content_html += '<div class="prod_grid_price">'
                content_html += '<p class="text-color-one uk-margin-remove-top">Rp.'+ formatRupiah(val.unit_price) + '</p>'
                content_html += '</div>'
                content_html += '</div>'
                content_html += '</div>'
                content_html += '</li>'
            });
            content_html += "</ul>";
            $(".products_grid_container").append(content_html); //// For Append

            var list_product_html = "<ul>";
            $.each(data.products, function (key, val) {
                list_product_html += '<li>'
                list_product_html += '<div class="product_item_wrapper">'
                list_product_html += '<div class="product_image_wrapper">'
                list_product_html += '<a href="{{ url("produk-detail/") }}/' + val.slug+ '">'
                list_product_html += '<img src="{{ asset("files/products") }}/'+ val.slug + '/thumbs/' + val.main_image + '" class="">'
                list_product_html += '</a>'
                list_product_html += '</div>'
                list_product_html += '<div class="product_description_wrapper">'
                list_product_html += '<div class="product_name">' + val.name
                list_product_html += '</div>'
                list_product_html += '<div class="product_category">' + val.category_name 
                list_product_html += '</div>'
                list_product_html += '<div class="product_price"> Rp. ' + formatRupiah(val.unit_price)
                list_product_html += '</div>'
                list_product_html += '</div>'
                list_product_html += '<div class="product_detail_wrapper">'
                list_product_html += '<a class="uk-button btn-viewDetail" href="{{ url('/produk-detail') }}/' + val.slug + '">View Detail</a>'
                list_product_html += '</div>'
                list_product_html += '</div>'
                list_product_html += '</li>'
            });
            list_product_html += "</ul>";
            $(".product_list_container").append(list_product_html);
            var defaultGrid = $('.products_grid_container ul > li').get();
        },
        error: function() { 
            console.log(data);
        }
    });
});

$(".new").click(function(e){
    $(".products_grid_container").html("");
    $(".product_list_container").html("");
    $.ajax({
        type: 'GET',
        url: "{{ route('ajax.new') }}",
        success: function (data) {
            $("#from").html('1');
            $("#to").html(data.products.length);
            $("#total").html(data.products.length);
            
            var content_html = "<ul>";
            $.each(data.products, function (key, val) {
                content_html += '<li data-sort="' + val.name + '">'
                content_html += '<div class="product_grid_item_wrapper">' 
                content_html += '<div class="product_grid_image_wrapper">'
                content_html += '<a href="{{ url("produk-detail/") }}/' + val.slug+ '">'
                content_html += '<div class="container-prod">'
                content_html += '<img src="{{ asset("files/products") }}/'+ val.slug + '/thumbs/' + val.main_image + '" >'
                content_html += '<div class="overlay-prod">'
                content_html += '<div class="text-prod">View Detail</div>'
                content_html += ' </div>'
                content_html += '</div>'
                content_html += '</a>'
                content_html += '</div>'
                content_html += '<div class="product_grid_description_wrapper">'
                content_html += '<div class="prod_grid_category_name">'
                content_html += '<span class="uk-text-emphasis uk-margin-remove-top">' + val.category_name +'</span>'
                content_html += '</div>'
                content_html += '<div class="prod_grid_name">'
                content_html += '<p class="product-name uk-margin-remove-top uk-margin-remove-bottom">'+ val.name +' </p>'
                content_html += '</div>'
                content_html += '<div class="prod_grid_price">'
                content_html += '<p class="text-color-one uk-margin-remove-top">Rp.'+ formatRupiah(val.unit_price) + '</p>'
                content_html += '</div>'
                content_html += '</div>'
                content_html += '</div>'
                content_html += '</li>'
            });
            content_html += "</ul>";
            $(".products_grid_container").append(content_html); //// For Append

            var list_product_html = "<ul>";
            $.each(data.products, function (key, val) {
                list_product_html += '<li>'
                list_product_html += '<div class="product_item_wrapper">'
                list_product_html += '<div class="product_image_wrapper">'
                list_product_html += '<a href="{{ url("produk-detail/") }}/' + val.slug+ '">'
                list_product_html += '<img src="{{ asset("files/products") }}/'+ val.slug + '/thumbs/' + val.main_image + '" class="">'
                list_product_html += '</a>'
                list_product_html += '</div>'
                list_product_html += '<div class="product_description_wrapper">'
                list_product_html += '<div class="product_name">' + val.name
                list_product_html += '</div>'
                list_product_html += '<div class="product_category">' + val.category_name 
                list_product_html += '</div>'
                list_product_html += '<div class="product_price"> Rp. ' + formatRupiah(val.unit_price)
                list_product_html += '</div>'
                list_product_html += '</div>'
                list_product_html += '<div class="product_detail_wrapper">'
                list_product_html += '<a class="uk-button btn-viewDetail" href="{{ url('/produk-detail') }}/' + val.slug + '">View Detail</a>'
                list_product_html += '</div>'
                list_product_html += '</div>'
                list_product_html += '</li>'
            });
            list_product_html += "</ul>";
            $(".product_list_container").append(list_product_html);
            var defaultGrid = $('.products_grid_container ul > li').get();
        },
        error: function() { 
            console.log(data);
        }
    });
});

$(".available").click(function(e){
    $(".products_grid_container").html("");
    $(".product_list_container").html("");
    $.ajax({
        type: 'GET',
        url: "{{ route('ajax.available') }}",
        success: function (data) {
            $("#from").html('1');
            $("#to").html(data.products.length);
            $("#total").html(data.products.length);
            
            var content_html = "<ul>";
            $.each(data.products, function (key, val) {
                content_html += '<li data-sort="' + val.name + '">'
                content_html += '<div class="product_grid_item_wrapper">' 
                content_html += '<div class="product_grid_image_wrapper">'
                content_html += '<a href="{{ url("produk-detail/") }}/' + val.slug+ '">'
                content_html += '<div class="container-prod">'
                content_html += '<img src="{{ asset("files/products") }}/'+ val.slug + '/thumbs/' + val.main_image + '" >'
                content_html += '<div class="overlay-prod">'
                content_html += '<div class="text-prod">View Detail</div>'
                content_html += ' </div>'
                content_html += '</div>'
                content_html += '</a>'
                content_html += '</div>'
                content_html += '<div class="product_grid_description_wrapper">'
                content_html += '<div class="prod_grid_category_name">'
                content_html += '<span class="uk-text-emphasis uk-margin-remove-top">' + val.category_name +'</span>'
                content_html += '</div>'
                content_html += '<div class="prod_grid_name">'
                content_html += '<p class="product-name uk-margin-remove-top uk-margin-remove-bottom">'+ val.name +' </p>'
                content_html += '</div>'
                content_html += '<div class="prod_grid_price">'
                content_html += '<p class="text-color-one uk-margin-remove-top">Rp.'+ formatRupiah(val.unit_price) + '</p>'
                content_html += '</div>'
                content_html += '</div>'
                content_html += '</div>'
                content_html += '</li>'
            });
            content_html += "</ul>";
            $(".products_grid_container").append(content_html); //// For Append

            var list_product_html = "<ul>";
            $.each(data.products, function (key, val) {
                list_product_html += '<li>'
                list_product_html += '<div class="product_item_wrapper">'
                list_product_html += '<div class="product_image_wrapper">'
                list_product_html += '<a href="{{ url("produk-detail/") }}/' + val.slug+ '">'
                list_product_html += '<img src="{{ asset("files/products") }}/'+ val.slug + '/thumbs/' + val.main_image + '" class="">'
                list_product_html += '</a>'
                list_product_html += '</div>'
                list_product_html += '<div class="product_description_wrapper">'
                list_product_html += '<div class="product_name">' + val.name
                list_product_html += '</div>'
                list_product_html += '<div class="product_category">' + val.category_name 
                list_product_html += '</div>'
                list_product_html += '<div class="product_price"> Rp. ' + formatRupiah(val.unit_price)
                list_product_html += '</div>'
                list_product_html += '</div>'
                list_product_html += '<div class="product_detail_wrapper">'
                list_product_html += '<a class="uk-button btn-viewDetail" href="{{ url('/produk-detail') }}/' + val.slug + '">View Detail</a>'
                list_product_html += '</div>'
                list_product_html += '</div>'
                list_product_html += '</li>'
            });
            list_product_html += "</ul>";
            $(".product_list_container").append(list_product_html);
            var defaultGrid = $('.products_grid_container ul > li').get();
        },
        error: function() { 
            console.log(data);
        }
    });
});

$(".coming_soon").click(function(e){
    $(".products_grid_container").html("");
    $(".product_list_container").html("");
    $.ajax({
        type: 'GET',
        url: "{{ route('ajax.soon') }}",
        success: function (data) {
            $("#from").html('1');
            $("#to").html(data.products.length);
            $("#total").html(data.products.length);
            
            var content_html = "<ul>";
            $.each(data.products, function (key, val) {
                content_html += '<li data-sort="' + val.name + '">'
                content_html += '<div class="product_grid_item_wrapper">' 
                content_html += '<div class="product_grid_image_wrapper">'
                content_html += '<a href="{{ url("produk-detail/") }}/' + val.slug+ '">'
                content_html += '<div class="container-prod">'
                content_html += '<img src="{{ asset("files/products") }}/'+ val.slug + '/thumbs/' + val.main_image + '" >'
                content_html += '<div class="overlay-prod">'
                content_html += '<div class="text-prod">View Detail</div>'
                content_html += ' </div>'
                content_html += '</div>'
                content_html += '</a>'
                content_html += '</div>'
                content_html += '<div class="product_grid_description_wrapper">'
                content_html += '<div class="prod_grid_category_name">'
                content_html += '<span class="uk-text-emphasis uk-margin-remove-top">' + val.category_name +'</span>'
                content_html += '</div>'
                content_html += '<div class="prod_grid_name">'
                content_html += '<p class="product-name uk-margin-remove-top uk-margin-remove-bottom">'+ val.name +' </p>'
                content_html += '</div>'
                content_html += '<div class="prod_grid_price">'
                content_html += '<p class="text-color-one uk-margin-remove-top">Rp.'+ formatRupiah(val.unit_price) + '</p>'
                content_html += '</div>'
                content_html += '</div>'
                content_html += '</div>'
                content_html += '</li>'
            });
            content_html += "</ul>";
            $(".products_grid_container").append(content_html); //// For Append

            var list_product_html = "<ul>";
            $.each(data.products, function (key, val) {
                list_product_html += '<li>'
                list_product_html += '<div class="product_item_wrapper">'
                list_product_html += '<div class="product_image_wrapper">'
                list_product_html += '<a href="{{ url("produk-detail/") }}/' + val.slug+ '">'
                list_product_html += '<img src="{{ asset("files/products") }}/'+ val.slug + '/thumbs/' + val.main_image + '" class="">'
                list_product_html += '</a>'
                list_product_html += '</div>'
                list_product_html += '<div class="product_description_wrapper">'
                list_product_html += '<div class="product_name">' + val.name
                list_product_html += '</div>'
                list_product_html += '<div class="product_category">' + val.category_name 
                list_product_html += '</div>'
                list_product_html += '<div class="product_price"> Rp. ' + formatRupiah(val.unit_price)
                list_product_html += '</div>'
                list_product_html += '</div>'
                list_product_html += '<div class="product_detail_wrapper">'
                list_product_html += '<a class="uk-button btn-viewDetail" href="{{ url('/produk-detail') }}/' + val.slug + '">View Detail</a>'
                list_product_html += '</div>'
                list_product_html += '</div>'
                list_product_html += '</li>'
            });
            list_product_html += "</ul>";
            $(".product_list_container").append(list_product_html);
            var defaultGrid = $('.products_grid_container ul > li').get();
        },
        error: function() { 
            console.log(data);
        }
    });
});

$(".color-palette").click(function(){
    $(".products_grid_container").html("");
    $(".product_list_container").html("");
    var color_arr = $(this).data("id").split('#')
    $.ajax({
        type: 'GET',
        url: "{{ url('ajax/color') }}/"+ color_arr[1],
        success: function (data) {
            $("#from").html('1');
            $("#to").html(data.products.length);
            $("#total").html(data.products.length);
            
            var content_html = "<ul>";
            $.each(data.products, function (key, val) {
                content_html += '<li data-sort="' + val.name + '">'
                content_html += '<div class="product_grid_item_wrapper">' 
                content_html += '<div class="product_grid_image_wrapper">'
                content_html += '<a href="{{ url("produk-detail/") }}/' + val.slug+ '">'
                content_html += '<div class="container-prod">'
                content_html += '<img src="{{ asset("files/products") }}/'+ val.slug + '/thumbs/' + val.main_image + '" >'
                content_html += '<div class="overlay-prod">'
                content_html += '<div class="text-prod">View Detail</div>'
                content_html += ' </div>'
                content_html += '</div>'
                content_html += '</a>'
                content_html += '</div>'
                content_html += '<div class="product_grid_description_wrapper">'
                content_html += '<div class="prod_grid_category_name">'
                content_html += '<span class="uk-text-emphasis uk-margin-remove-top">' + val.category_name +'</span>'
                content_html += '</div>'
                content_html += '<div class="prod_grid_name">'
                content_html += '<p class="product-name uk-margin-remove-top uk-margin-remove-bottom">'+ val.name +' </p>'
                content_html += '</div>'
                content_html += '<div class="prod_grid_price">'
                content_html += '<p class="text-color-one uk-margin-remove-top">Rp.'+ formatRupiah(val.unit_price) + '</p>'
                content_html += '</div>'
                content_html += '</div>'
                content_html += '</div>'
                content_html += '</li>'
            });
            content_html += "</ul>";
            $(".products_grid_container").append(content_html); //// For Append

            var list_product_html = "<ul>";
            $.each(data.products, function (key, val) {
                list_product_html += '<li>'
                list_product_html += '<div class="product_item_wrapper">'
                list_product_html += '<div class="product_image_wrapper">'
                list_product_html += '<a href="{{ url("produk-detail/") }}/' + val.slug+ '">'
                list_product_html += '<img src="{{ asset("files/products") }}/'+ val.slug + '/thumbs/' + val.main_image + '" class="">'
                list_product_html += '</a>'
                list_product_html += '</div>'
                list_product_html += '<div class="product_description_wrapper">'
                list_product_html += '<div class="product_name">' + val.name
                list_product_html += '</div>'
                list_product_html += '<div class="product_category">' + val.category_name 
                list_product_html += '</div>'
                list_product_html += '<div class="product_price"> Rp. ' + formatRupiah(val.unit_price)
                list_product_html += '</div>'
                list_product_html += '</div>'
                list_product_html += '<div class="product_detail_wrapper">'
                list_product_html += '<a class="uk-button btn-viewDetail" href="{{ url('/produk-detail') }}/' + val.slug + '">View Detail</a>'
                list_product_html += '</div>'
                list_product_html += '</div>'
                list_product_html += '</li>'
            });
            list_product_html += "</ul>";
            $(".product_list_container").append(list_product_html);
            var defaultGrid = $('.products_grid_container ul > li').get();
        },
        error: function() { 
            console.log(data);
        }
    });
    return false;
});

$("#sort").change(function () {
    var val = this.value;
    
    var itemsList = $('.product_list_container ul > li').get();
    var itemsGrid = $('.products_grid_container ul > li').get();
    // A to Z
    if(val == 'a_z'){
        itemsList.sort(function(a,b){
            var keyA = $(a).attr('data-sort');
            var keyB = $(b).attr('data-sort');

            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        });
        var ul = $('.product_list_container > ul');
        $.each(itemsList, function(i, li){
            ul.append(li); /* This removes li from the old spot and moves it */
        });

        // grid
        itemsGrid.sort(function(c,d){
            var keyC = $(c).attr('data-sort');
            var keyD = $(d).attr('data-sort');

            if (keyC < keyD) return -1;
            if (keyC > keyD) return 1;
            return 0;
        });
        var ulGrid = $('.products_grid_container > ul');
        $.each(itemsGrid, function(j, li){
            ulGrid.append(li); /* This removes li from the old spot and moves it */
        });
        
    }else if(val == 'z_a'){
        itemsList.sort(function(a,b){
            var keyA = $(a).attr('data-sort');
            var keyB = $(b).attr('data-sort');

            if (keyA > keyB) return -1;
            if (keyA < keyB) return 1;
            return 0;
        });
        var ul = $('.product_list_container > ul');
        $.each(itemsList, function(i, li){
            ul.append(li); /* This removes li from the old spot and moves it */
        });

        // grid
        itemsGrid.sort(function(c,d){
            var keyC = $(c).attr('data-sort');
            var keyD = $(d).attr('data-sort');

            if (keyC > keyD) return -1;
            if (keyC < keyD) return 1;
            return 0;
        });
        var ulGrid = $('.products_grid_container > ul');
        $.each(itemsGrid, function(j, li){
            ulGrid.append(li); /* This removes li from the old spot and moves it */
        });
    }
    else{
        var ul = $('.product_list_container > ul');
        $.each(defaultList, function(i, li){
            ul.append(li); /* This removes li from the old spot and moves it */
        });

        var ulGrid = $('.products_grid_container > ul');
        $.each(defaultGrid, function(i, li){
            ulGrid.append(li); /* This removes li from the old spot and moves it */
        });
    }

});


function formatRupiah(angka){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? ',' : '';
            rupiah += separator + ribuan.join(',');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return ' ' +rupiah;
    }



</script>
@endpush