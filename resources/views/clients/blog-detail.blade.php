@extends('layouts.layout')
@section('content')
@include('components.highlight-page')

<section class="uk-section uk-article">
			<div class="uk-container uk-container-small">
				<h1 class="uk-text-bold uk-h1 uk-margin-remove-adjacent uk-margin-remove-top">{{ $blogs->blog_name }}</h1>
				<p class="uk-article-meta">Written by {{ $blogs->admin_name }} on {{ \Carbon\Carbon::parse($blogs->blog_created)->format('d M Y')}}. Posted in <a href="#">Blog</a> </p><br>				
			</div>
			
			<!-- text -->
			<div class="uk-container uk-container-small">
				{!! $blogs->blog_desc !!}
			</div>
			<!-- text -->
		</section>
        
        
@include('components.other-blog')
@endsection