@extends('layouts.layout')
@section('content')
@include('components.highlight-page')

<div class="uk-container uk-padding-large">
    <p class="uk-text-emphasis uk-text-bold">Info Kontak</p>
    <div class="uk-grid-small uk-child-width-expand@s uk-text-center" uk-grid>
        <div>
            <div class="uk-card uk-background-muted uk-card-body">
                <span uk-icon="icon: location"></span><br><br><br>
                <span class="uk-text-large uk-text-emphasis uk-text-bold">Alamat Lengkap</span><br>
                <p class="uk-text-meta uk-text-emphasis">{{ isset($items->address) ? $items->address : '' }}</p>
            </div>
        </div>
        <div>
            <div class="uk-card">
                <div class="uk-flex uk-flex-column uk-width-1-1">
                    <div class="uk-card uk-card-body uk-background-muted">
                        <span uk-icon="icon: phone"></span>{{ isset($items->phone) ? $items->phone : '' }}
                    </div>
                    <div class="uk-card uk-card-body uk-margin-top  uk-background-muted">
                        <span uk-icon="icon: mail"></span> {{ isset($items->email) ? $items->email : '' }}
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="uk-card uk-background-muted uk-card-body">
                <span uk-icon="icon: clock"></span><br><br><br>
                <span class="uk-text-large uk-text-emphasis uk-text-bold">Buka Toko</span><br>
                <p class="uk-text-meta uk-text-emphasis">{{ isset($items->open_store) ? $items->open_store : '' }}</p>
            </div>
        </div>
    </div>
    @if (Session::get('success') || Session::get('error'))
    @else
    <p class="uk-text-emphasis uk-text-bold">Hubungi Kami Melalui :</p>
    <div>
    <form action="{{ url('message/save') }}" method="POST">
        @csrf
        <div class="uk-margin">
            <div class="uk-grid-small uk-child-width-expand@s uk-text-center" uk-grid>
                <div>
                    <div class="uk-card uk-text-left">
                        <input class="uk-input uk-background-muted uk-text-small" type="text" placeholder="Nama Lengkap / Instansi" name="name">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div>
                    <div class="uk-card uk-text-left">
                        <input class="uk-input uk-background-muted uk-text-small" type="text" placeholder="Alamat Email" name="email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-margin uk-text-left">
            <input class="uk-input uk-background-muted uk-text-small" type="text" placeholder="Judul Pesan" name="subject">
            @error('subject')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="uk-margin uk-text-left">
            <textarea class="uk-textarea uk-background-muted uk-text-small" rows="5" placeholder="Tuils Pesan" name="description"></textarea>
            @error('description')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="uk-margin">
            <button class="uk-button uk-text-capitalize button-send-message" type="submit">Kirim Pesan</button>
        </div>

    </form>
    </div>
    @endif
    <br>
    @if ($message = Session::get('success'))
        <div class="uk-alert-success" uk-alert>
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="mapouter uk-visible@m"><div class="gmap_canvas"><iframe width="100%" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=Senyum%20ummi&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div></div>
    <div class="mapouter uk-hidden@m"><div class="gmap_canvas"><iframe width="100%" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=Senyum%20ummi&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div></div>
</div>
@endsection

@push('scripts')

@endpush