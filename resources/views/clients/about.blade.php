@extends('layouts.layout')
@section('content')
@include('components.highlight-page')

<div class="uk-text-center" uk-grid>
    
    <div class="uk-width-1-3@m uk-visible@m">
        <div class="uk-card">
            <img class="lazy" src="{{ asset('images/left-image-about.png') }}" alt="">
        </div>
    </div>
    <div class="uk-width-1-3@m uk-hidden@m">
        <div class="uk-card">
            <img class="lazy" src="{{ asset('images/bg-about-us.jpg') }}" alt="">
        </div>
    </div>
    <div class="uk-grid-small uk-width-expand@m uk-visible@m">
        <div class="uk-card uk-card-body uk-text-justify">
            <br><br>
            <h1>Tentang Kaheci</h1>
            <p>Bermula pada tahun 2014 melihat ada yang butuh terkait kaos kaki motif henna yang susah ditemui di pasaran, kita coba untuk membuat kaos kaki henna tersebut dan alhamdulillah dapat respon posifit di pasaran sehingga sampai sekarang sudah terdapat sekiar 35 kaos kaki motif henna dan insya Allah terus berkembang. Memudahkan bagi ukhti muslimah melangsungkan pernikahan tanpa repot-repot mengecat kaki nya dengan henna (pacar), cukup menggunakan kaos kaki saja.</p>
            <p><b>Nama Kaheci</b> sendiri diambil dari singkatan dari Kaos Kaki Henna Cantik, karena itu jadilah Kaheci menjadi pelopor kaos kaki motif Henna pertama di Indonesia.</p>
            <p>Akhir tahun 2017 kita mulai membuat sarung tangan yang unik dengan berbagai model yang sebelumnya hanya dikenal sarung tangan klasik saja, seperti sarung tangan renda dan bunga yabng menjadi produk unggulan kaheci, dan bisa buat touchscreen, pegang HP ga perlu lepas sarung tangan.</p>
            <p>Semakin banyaknya ukhti muslimah yang hijrah semakin sadar bahwa mereka butuh menutup aurat lebih sempurna, Kita mulai sediakan produk handsock, manset untuk membantu menutup aurat wanita sebab kadang-kadang baju hijab disekitar lengan tangan masih kurang menutup aurat, khususnya pada kondisi-kondisi tertentu seperti naik motor, Tersedia belasan motif di Kaheci dan insya Allah akan terus bertambah. Sekarang produk kaheci juga mulai merambah ke sarung tangan haji premium, jilbab /cadar special. Oh iya bagi ukhti muslimah semua jika kamu sudah punya jilbab/cadar dari kaheci, kamu termasuk yang beruntung, karena kita Cuma bikin dalam jumlah yang SANGAT-SANGAT terbatas, beneran deh.</p>
            <p>Bagi ukhti muslimah, sahabat kaheci semuanya, jika punya ide pengin bikin model sesuai bisa kok disampaikan ke kita, Insya Allah kita bantu untuk untuk meluncurkan produk yang inginkan.</p>
        </div>
    </div>

    <div class="uk-grid-small uk-width-expand@m uk-hidden@m">
        <div class="uk-card uk-card-body uk-text-justify">
            <h4 class="uk-text-center">Tentang Kaheci</h4>
            <p>Bermula pada tahun 2014 melihat ada yang butuh terkait kaos kaki motif henna yang susah ditemui di pasaran, kita coba untuk membuat kaos kaki henna tersebut dan alhamdulillah dapat respon posifit di pasaran sehingga sampai sekarang sudah terdapat sekiar 35 kaos kaki motif henna dan insya Allah terus berkembang. Memudahkan bagi ukhti muslimah melangsungkan pernikahan tanpa repot-repot mengecat kaki nya dengan henna (pacar), cukup menggunakan kaos kaki saja.</p>
            <p><b>Nama Kaheci</b> sendiri diambil dari singkatan dari Kaos Kaki Henna Cantik, karena itu jadilah Kaheci menjadi pelopor kaos kaki motif Henna pertama di Indonesia.</p>
            <p>Akhir tahun 2017 kita mulai membuat sarung tangan yang unik dengan berbagai model yang sebelumnya hanya dikenal sarung tangan klasik saja, seperti sarung tangan renda dan bunga yabng menjadi produk unggulan kaheci, dan bisa buat touchscreen, pegang HP ga perlu lepas sarung tangan.</p>
            <p>Semakin banyaknya ukhti muslimah yang hijrah semakin sadar bahwa mereka butuh menutup aurat lebih sempurna, Kita mulai sediakan produk handsock, manset untuk membantu menutup aurat wanita sebab kadang-kadang baju hijab disekitar lengan tangan masih kurang menutup aurat, khususnya pada kondisi-kondisi tertentu seperti naik motor, Tersedia belasan motif di Kaheci dan insya Allah akan terus bertambah. Sekarang produk kaheci juga mulai merambah ke sarung tangan haji premium, jilbab /cadar special. Oh iya bagi ukhti muslimah semua jika kamu sudah punya jilbab/cadar dari kaheci, kamu termasuk yang beruntung, karena kita Cuma bikin dalam jumlah yang SANGAT-SANGAT terbatas, beneran deh.</p>
            <p>Bagi ukhti muslimah, sahabat kaheci semuanya, jika punya ide pengin bikin model sesuai bisa kok disampaikan ke kita, Insya Allah kita bantu untuk untuk meluncurkan produk yang inginkan.</p>
        </div>
    </div>
</div>

<div class="uk-section uk-section-default uk-text-center uk-padding-remove-vertical ">
    <div class="uk-container">

        <h2 class="uk-visible@m">Video Ulasan Kaheci</h2>
        <h4 class="uk-hidden@m">Video Ulasan Kaheci</h4>

        <div class="uk-grid-match uk-child-width-1-1@m" uk-grid>
            <div>
            <object width="400" height="450"
                data="https://www.youtube.com/embed/-8C7Cg4iyQI">
                </object>
            </div>
        </div>

    </div>
</div>

<div class="uk-section uk-section-default uk-text-center uk-padding-remove">
    <div class="uk-background-cover uk-panel uk-flex uk-flex-center uk-flex-middle uk-visible@m" style="background-image: url({{ asset('images/bg-banner-keunggulan.jpg') }});height:1084px;">
    <div class="uk-grid-small uk-child-width-1-1 uk-padding-large uk-text-center" uk-grid>
            <div>
                <div class="uk-card uk-card-body">
                    <p class="uk-h4">Keunggulan Kaheci</p>
                </div>
            </div>
            <div>
                <div class="uk-card">
                    <div class="uk-child-width-1-2 uk-text-right" uk-grid>
                        <div>
                            <div class="uk-card item-keunggulan">
                                <h1>Hand Made</h1>
                                <p><b>hand made &amp; jahitan super rapi</b> <i>(khusus produk selain kaos kaki)</i></p>
                            </div>
                        </div>
                        <div style="max-width:200px;">
                            <div class="uk-card">
                                <img src="{{ asset('images/tentang-kami/hand-made.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-card">
                    <div class="uk-child-width-1-2 uk-text-right" uk-grid>
                        <div>
                            <div class="uk-card item-keunggulan">
                                <h1>Teknik Menjahit Khusus</h1>
                                <p>Menjahit handsock dengan kain elastis yang bagus membutuhkan teknis khusus agar jahitan tidak rusak saat menyesuaikan dengan bentuk tangan. Teknik khusus rahasia dari Kaheci</p>
                            </div>
                        </div>
                        <div style="max-width:200px;">
                            <div class="uk-card">
                                <img src="{{ asset('images/tentang-kami/menjahit-khusus.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-card">
                    <div class="uk-child-width-1-2 uk-text-right" uk-grid>
                        <div>
                            <div class="uk-card item-keunggulan">
                                <h1>Bahan Kualitas Super</h1>
                            </div>
                        </div>
                        <div style="max-width:200px;">
                            <div class="uk-card">
                                <img src="{{ asset('images/tentang-kami/kualitas-super.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-card">
                    <div class="uk-child-width-1-2 uk-text-right" uk-grid>
                        <div>
                            <div class="uk-card item-keunggulan">
                                <h1>Original Design Yang Terus Berkembang</h1>
                                <p>Dinamis dengan model baru menyesuaikan kebutuhan dan tren yang ada</p>
                            </div>
                        </div>
                        <div style="max-width:200px;">
                            <div class="uk-card">
                                <img src="{{ asset('images/tentang-kami/original-design.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-background-cover uk-panel uk-flex uk-flex-center uk-flex-middle uk-hidden@m uk-padding" style="background-image: url({{ asset('images/bg-keunggulan-mobile.jpg') }});">
        <div class="uk-grid-small uk-child-width-expand@s uk-text-center" uk-grid>
            <div>
                <div class="uk-card uk-card-body">
                    <p class="uk-h4">Keunggulan Kaheci</p>
                </div>
            </div>
            <div>
                <div class="uk-card">
                    <div class="uk-child-width-1-2 uk-text-left" uk-grid>
                        <div style="max-width:130px;">
                            <div class="uk-card">
                                <img src="{{ asset('images/tentang-kami/hand-made.png')}}" alt="">
                            </div>
                        </div>
                        <div style="width:200px;">
                            <div class="uk-card item-keunggulan">
                                <h1>Hand Made</h1>
                                <p><b>hand made &amp; jahitan super rapi</b> <i>(khusus produk selain kaos kaki)</i></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-card">
                    <div class="uk-child-width-1-2 uk-text-left" uk-grid>
                        <div style="max-width:130px;">
                            <div class="uk-card">
                                <img src="{{ asset('images/tentang-kami/menjahit-khusus.png')}}" alt="">
                            </div>
                        </div>
                        <div style="width:200px;">
                            <div class="uk-card item-keunggulan">
                                <h1>Teknik Menjahit Khusus</h1>
                                <p>Menjahit handsock dengan kain elastis yang bagus membutuhkan teknis khusus agar jahitan tidak rusak saat menyesuaikan dengan bentuk tangan. Teknik khusus rahasia dari Kaheci</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-card">
                    <div class="uk-child-width-1-2 uk-text-left" uk-grid>
                        <div style="max-width:130px;">
                            <div class="uk-card">
                                <img src="{{ asset('images/tentang-kami/kualitas-super.png')}}" alt="">
                            </div>
                        </div>
                        <div style="width:200px;">
                            <div class="uk-card item-keunggulan">
                                <h1>Bahan Kualitas Super</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-card">
                    <div class="uk-child-width-1-2 uk-text-left" uk-grid>
                        <div style="max-width:130px;">
                            <div class="uk-card">
                                <img src="{{ asset('images/tentang-kami/original-design.png')}}" alt="">
                            </div>
                        </div>
                        <div style="width:200px;">
                            <div class="uk-card item-keunggulan">
                                <h1>Original Design Yang Terus Berkembang</h1>
                                <p>Dinamis dengan model baru menyesuaikan kebutuhan dan tren yang ada</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
</div>

@endsection