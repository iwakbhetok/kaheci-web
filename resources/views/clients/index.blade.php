@extends('layouts.layout')
@section('content')
@include('components.slider')

@if(isset($category[0]))
<div class="uk-section-default uk-section uk-padding-remove-vertical">
    <div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin-large-top uk-margin-remove-top uk-visible@m" uk-grid>
        @foreach($category as $item)
        <a href="{{ url('category/'.$item->slug)}}">
        <div>
            <div class="uk-light uk-inline">
                <p class="uk-position-center-left text-category-grid">{{ get_first_word($item->name) }} <span>{{ get_last_word($item->name) }}</span></p>
                <img src="{{ asset('files/category/'.$item->slug.'/'.$item->image) }}" alt="{{ $item->name }}" style="max-height: 220px; max-width: 337px">
            </div>
        </div>
        </a>
        @endforeach
    </div>

    <div class="uk-position-relative uk-visible-toggle uk-light uk-hidden@m" tabindex="-1" uk-slider="sets: true">
        <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@m">
            @foreach($category as $item)
            <a href="{{ url('category/'.$item->slug)}}">
            <li>
                <p class="uk-position-center-left text-category-grid">{{ get_first_word($item->name) }} <span>{{ get_last_word($item->name) }}</span></p>
                <img src="{{ asset('files/category/'.$item->slug.'/'.$item->image) }}" alt="{{ $item->name }}" style="max-height: 130px; max-width: 200px">
            </li>
            </a>
            @endforeach
        </ul>
        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

    </div>
 
</div>
@endif
<!-- Produk Unggulan Desktop -->
@if(isset($premiums[0]))
<div class="uk-section uk-section-medium uk-section-default uk-visible@m">
    <div class="uk-container">
        <h3 class="uk-text-center text-title-section">Produk Unggulan</h3>
        <div class="uk-grid-medium uk-child-width-expand@s uk-text-center uk-animation-fade" uk-grid>
            @foreach($premiums as $premium)
            <div>
                <div class="uk-card">
                    <div class="container">
                        @if(!empty($premium->products_discount))
                        <div class="product-highlight-yellow uk-position-left uk-flex uk-flex-middle">
                            <span class="uk-position-center">{{ $premium->products_discount }}%</span>
                        </div>
                        @else
                        <div class="product-highlight-new uk-position-left uk-flex uk-flex-middle">
                            <span class="uk-position-center">NEW</span>
                        </div>
                        @endif
                        <a href="{{ url('/produk-detail/'.$premium->products_url) }}">
                            <img src="{{ asset('files/products/'.$premium->products_url.'/thumbs/'.$premium->products_thumb) }}" alt="{{ $premium->products_thumb }}">
                            <div class="overlay">
                                <div class="text">View Detail</div>
                            </div>
                        </a>
                    </div>
                    <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                        <span class="uk-text-emphasis uk-margin-remove-top">{{ $premium->cat_name }}</span>
                        <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">{{ $premium->products_name }} </p>
                        <p class="text-color-one uk-margin-remove-top">Rp. {{ $premium->products_discount == 0 ? number_format($premium->products_price,2) : number_format($premium->products_price - ($premium->products_price * $premium->products_discount / 100) ,2) }}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif

<!-- End Mobile Produk Unggulan -->
@if(isset($premiums[0]))
<div class="uk-padding-remove-bottom uk-section uk-section-medium uk-section-default uk-hidden@m">
    <div class="uk-container">
        <h3 class="uk-text-center">Produk Unggulan</h3>
        <div uk-slider>

            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m">
                    @foreach($premiums as $premium)
                    <li>
                        <div class="uk-card uk-padding">
                            <div class="container">
                                @if(!empty($premium->products_discount))
                                <div class="product-highlight-new-mobile uk-position-left uk-flex uk-flex-middle" style="background-color: #f8a046 !important">
                                    <span class="uk-position-center">{{$premium->products_discount}}%</span>
                                </div>
                                @else
                                <div class="product-highlight-new-mobile uk-position-left uk-flex uk-flex-middle">
                                    <span class="uk-position-center">NEW</span>
                                </div>
                                @endif
                                <a href="{{ url('/produk-detail/'.$premium->products_url) }}">
                                    <img src="{{ asset('files/products/'.$premium->products_url.'/thumbs/'.$premium->products_thumb) }}" alt="{{ $premium->products_thumb }}">
                                </a>
                            </div>
                            <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                                <a href="{{ url('/produk-detail/'.$premium->products_url) }}" class="label-product-mobile">
                                <span class="uk-margin-remove-top">{{ $premium->cat_name }}</span>
                                <p class="product-name-mobile uk-margin-remove-top uk-margin-remove-bottom">{{ $premium->products_name }} </p>
                                <p class="text-color-one-mobile uk-margin-remove-top">Rp. {{ $premium->products_discount == 0 ? number_format($premium->products_price,2) : number_format($premium->products_price - ($premium->products_price * $premium->products_discount / 100) ,2) }}</p>
                                </a>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>

                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

            </div>

            <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

        </div>
    </div>
</div>
@endif

<!--Coming soon product desktop section -->
@if(isset($comingsoon[0]))
<div class="uk-section uk-section-small uk-section-default uk-visible@m">
    <div class="uk-cover-container" uk-scrollspy="cls:uk-animation-fade; repeat: true">
        <div class="uk-background-image@m uk-background-cover uk-background-muted uk-height-large" style="background-image: url({{ asset('images/banner/desktop/'.$comingsoon[0]->image) }});background-repeat: no-repeat;background-size:100%;">
            <div class="uk-grid-large uk-child-width-expand@s uk-text-center uk-padding" uk-grid>
                <div class="uk-padding-large">
                    <div class="uk-card uk-padding-large">
                        <a class="uk-button button-coming-soon" href="
                        {{ $comingsoon[0]->url }}">{{ $comingsoon[0]->text_url }} <span uk-icon="arrow-right"></span></a>
                    </div>
                </div>
                <div>
                    <div class="uk-card" style="background-color:rgba(96, 77, 56, 0.6);border: 2px;border-radius: 5px;">
                        <p class="uk-h4 uk-visible@m uk-light uk-text-uppercase countdown-text" style="color: #f5efef; padding-left: 25px; padding-right: 25px; padding-top: 25px">{{ $comingsoon[0]->name }}</p>
                        <br>
                        <span class="uk-text-emphasis" style="text-align: left; ">
                            <p style="color: #fff;padding-left: 25px; padding-right: 25px; padding-top: 0;">{{str_limit(strip_tags(str_replace(',', ' ', $comingsoon[0]->description)),150,'...')}}</p>
                        </span><br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End coming soon product desktop section -->

<!--Coming soon product mobile section -->
<div class="uk-section uk-section-small uk-section-default uk-hidden@m uk-padding-remove-vertical">
    <div class="uk-cover-container" uk-scrollspy="cls:uk-animation-fade; repeat: true">
        <div class="uk-background-cover uk-height-medium" style="background-image: url({{ asset('images/banner/mobile/'.$comingsoon[0]->image_mobile) }});">
            <div class="uk-grid-large uk-child-width-expand@s uk-text-center uk-padding" uk-grid>
                <div class="">

                    <div class="uk-card" style="margin-top:5%;">
                        <p class="uk-margin-remove uk-text-strtoupper countdown-text-mobile uk-text-uppercase" style="background-color:rgba(196, 153, 109, .6); color: #fff; border-radius:5px; font-size: 1em; padding: 15px 10px 15px 10px">{{ $comingsoon[0]->name }}</p>
                        <br><br>
                        <a class="uk-button button-coming-soon" href="{{ $comingsoon[0]->url }}">{{ $comingsoon[0]->text_url }} <span uk-icon="arrow-right"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<!--End coming soon product mobile section -->

<!-- Produk Unggulan Desktop -->
<div class="uk-section uk-section-small uk-section-default uk-visible@m">
    <div class="uk-container">
        
        <ul class="uk-flex-center" uk-tab>
            <li><a href="#">Dijual</a></li>
            <li><a href="#">Terbaru</a></li>
        </ul>

        <ul class="uk-switcher uk-margin">
            @if(isset($sale[0]))
            <li>
                <div class="uk-grid-medium uk-child-width-expand@s uk-text-center uk-animation-slide-left-medium" uk-grid>
                    @foreach($sale as $itemsale)
                    <div>
                        <div class="uk-card">
                            <div class="container">
                                @if(!empty($itemsale->products_discount))
                                <div class="product-highlight-sale uk-position-left uk-flex uk-flex-middle">
                                    <span class="uk-position-center">{{ $itemsale->products_discount }}%</span>
                                </div>
                                @else
                                <div class="product-highlight-sale uk-position-left uk-flex uk-flex-middle" style="background-color: #c4996c !important;">
                                    <span class="uk-position-center">{{ strtoupper($itemsale->products_type) }}</span>
                                </div>
                                @endif
                                <a href="{{ url('/produk-detail/'.$itemsale->products_url) }}">
                                    <img src="{{ asset('files/products/'.$itemsale->products_url.'/thumbs/'.$itemsale->products_thumb)}}" alt="{{ $itemsale->products_thumb }}" style="max-height: 380px">
                                    <div class="overlay">
                                        <div class="text">View Detail</div>
                                    </div>
                                </a>
                            </div>
                            <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                                <span class="uk-text-emphasis uk-margin-remove-top">{{ $itemsale->cat_name }}</span>
                                <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">{{ $itemsale->products_name }} </p>
                                <p class="text-color-one uk-margin-remove-top">Rp. {{ $itemsale->products_discount == 0 ? number_format($itemsale->products_price, 2) : number_format($itemsale->products_price - ($itemsale->products_price * $itemsale->products_discount / 100),2) }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </li>
            @endif
            @if(isset($new[0]))
            <li>
                <div class="uk-grid-medium uk-child-width-expand@s uk-text-center uk-animation-slide-left-medium" uk-grid>
                    @foreach($new as $itemnew)
                    <div>
                        <div class="uk-card">
                            <div class="container">
                                <div class="product-highlight-sale uk-position-left uk-flex uk-flex-middle">
                                    <span class="uk-position-center">{{ strtoupper($itemnew->products_type) }}</span>
                                </div>
                                <a href="{{ url('/produk-detail/'.$itemnew->products_url) }}">
                                    <img src="{{ asset('files/products/'.$itemnew->products_url.'/thumbs/'.$itemnew->products_thumb)}}" alt="{{ $itemnew->products_thumb }}">
                                    <div class="overlay">
                                        <div class="text">View Detail</div>
                                    </div>
                                </a>
                            </div>
                            <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                                <span class="uk-text-emphasis uk-margin-remove-top">{{ $itemnew->cat_name }}</span>
                                <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">{{ $itemnew->products_name }} </p>
                                <p class="text-color-one uk-margin-remove-top">Rp. {{ $itemnew->products_discount == 0 ? number_format($itemnew->products_price, 2) : number_format($itemnew->products_price - ($itemnew->products_price * $itemnew->products_discount / 100),2) }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </li>
            @endif
        </ul>

    </div>
</div>
<!-- End Produk Unggulan -->
<!-- Produk Unggulan Mobile -->
<div class="uk-section uk-section-small uk-section-default uk-hidden@m">
    <div class="uk-container">
        
        <ul class="uk-flex-center" uk-tab>
            <li><a href="#">Dijual</a></li>
            <li><a href="#">Terbaru</a></li>
        </ul>

        <ul class="uk-switcher uk-margin">
            @if(isset($sale[0]))
            <li>
                <div class="uk-grid-medium uk-child-width-1-2 uk-text-center uk-animation-slide-left-medium" uk-grid>
                    @foreach($sale as $item)
                    <div>
                        <div class="uk-card">
                            <div class="container">
                                @if(($item->products_discount != 0) || ($item->products_discount == null))
                                <div class="product-highlight-sale-mobile uk-position-left uk-flex uk-flex-middle" style="background-color: #f8a045">
                                    <span class="uk-position-center">{{ $item->products_discount }}%</span>
                                </div>
                                @else
                                <div class="product-highlight-sale-mobile uk-position-left uk-flex uk-flex-middle" style="background-color: #c4996c">
                                    <span class="uk-position-center">{{ strtoupper($item->products_type) }}</span>
                                </div>
                                @endif
                                <a href="{{ url('/produk-detail/'.$item->products_url) }}">
                                    <img src="{{ asset('files/products/'.$item->products_url.'/thumbs/'.$item->products_thumb)}}" alt="{{ $item->products_thumb }}">
                                </a>
                            </div>
                            <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                                <a href="#" class="label-product-mobile">
                                <span class="uk-text-emphasis uk-margin-remove-top">{{ $item->cat_name }}</span>
                                <p class="product-name-mobile uk-margin-remove-top uk-margin-remove-bottom">{{ $item->products_name }} </p>
                                <p class="text-color-one-mobile uk-margin-remove-top">Rp. {{ $item->products_discount == 0 ? $item->products_price : $item->products_price - ($item->products_price * $item->products_discount / 100) }}</p>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </li>
            @endif
            @if(isset($new))
            <li>
                <div class="uk-grid-medium uk-child-width-1-2 uk-text-center uk-animation-slide-left-medium" uk-grid>
                    @foreach($new as $item)
                    <div>
                        <div class="uk-card">
                            <div class="container">
                                @if(($item->products_discount != 0) || ($item->products_discount == null))
                                <div class="product-highlight-new-mobile uk-position-left uk-flex uk-flex-middle" style="background-color: #f8a045">
                                    <span class="uk-position-center">{{$item->products_discount}}%</span>
                                </div>
                                @else
                                <div class="product-highlight-new-mobile uk-position-left uk-flex uk-flex-middle">
                                    <span class="uk-position-center">NEW</span>
                                </div>
                                @endif
                                <a href="{{ url('/produk-detail/'.$item->products_url) }}">
                                    <img src="{{ asset('files/products/'.$item->products_url.'/thumbs/'.$item->products_thumb)}}" alt="{{ $item->products_thumb }}">
                                </a>
                            </div>
                            <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                                <a href="#" class="label-product-mobile">
                                <span class="uk-text-emphasis uk-margin-remove-top">{{ $item->cat_name }}</span>
                                <p class="product-name-mobile uk-margin-remove-top uk-margin-remove-bottom">{{ $item->products_name }} </p>
                                <p class="text-color-one-mobile uk-margin-remove-top">Rp. {{ $item->products_discount == 0 ? $item->products_price : $item->products_price - ($item->products_price * $item->products_discount / 100) }}</p>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </li>
            @endif
        </ul>

    </div>
</div>

<!-- Section for blogs -->
@if(isset($blogs[0]))
<div class="uk-padding-remove-vertical uk-section uk-section-small uk-section-default" uk-scrollspy="cls:uk-animation-fade; repeat: true">
    <div class="uk-container">

        <h3 class="uk-text-center text-title-section">Blog Kaheci</h3>

        <div class="uk-grid-medium uk-child-width-expand@s uk-text-left" uk-grid>
            @foreach($blogs as $blog)
            <div class="uk-card">
                <a href="{{ url('/blog/'.$blog->blog_url) }}">
                    <div class="blog-category uk-position-left uk-flex uk-flex-middle">
                        <span class="uk-position-center">{{ $blog->cat_name }}</span>
                    </div>
                </a>
                <a href="{{ url('/blog/'.$blog->blog_url) }}">
                <img src="{{ asset('images/blogs/'.$blog->blog_url.'/'.$blog->blog_thumb) }}" alt="">
                </a>
                <p class="uk-comment-meta uk-margin-remove-bottom"><a class="uk-link-reset" href="#">By {{ $blog->admin_name }}</a></p>
                <a href="{{ url('/blog/'.$blog->blog_url) }}">
                    <p class="title-blog">{{ $blog->blog_name }}</p>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif

@endsection