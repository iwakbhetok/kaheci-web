@extends('layouts.layout')
@section('content')
@include('components.highlight-page')

<!--CONTENT-->
<div class="uk-section uk-section-default">
    <div class="uk-container">
        <div class="uk-grid" data-ukgrid>
            <div class="uk-width-2-3@m">
                <h4 class="uk-heading-line uk-text-bold"><span>Latest Blogs</span></h4>
                @foreach($blogs as $items)
                <article class="uk-section uk-section-small uk-padding-remove-top">
                    <div class="uk-card">
                        <a href="{{ url('/blog/'.$items->blog_url) }}">
                        <div class="blog-category uk-position-left uk-flex uk-flex-middle">
                            <span class="uk-position-center">{{ $items->cat_name }}</span>
                        </div>
                        </a>
                        <a href="{{ url('/blog/'.$items->blog_url) }}" rel="nofollow">
                        <img src="{{ asset('images/blogs/'.$items->blog_url.'/'.$items->blog_thumbs) }}" alt="">
                        </a>
                        <p class="uk-article-meta">Written by {{ $items->admin_name }} on {{ \Carbon\Carbon::parse($items->blog_created)->format('M d, Y')}}. Posted in <a href="#">Blog</a></p>
                        <a href="{{ url('/4-style-hijab') }}" class="link-title-blog">
                            <p class="title-blog">{{ $items->blog_name }}</p>
                        </a>
                        <p>{{str_limit(strip_tags(str_replace('&nbsp;', ' ', $items->blog_desc)),400,'...')}}</p>
                    </div>
                    <a href="{{ url('/blog/'.$items->blog_url) }}" title="Read More" class="uk-button uk-button-default uk-button-small">READ MORE</a>
                    <hr>
                </article>
                @endforeach
                <div class="uk-section uk-section-default uk-text-center uk-flex uk-flex-center">
                    <div class="uk-container">


                        <div class="uk-child-width-1-1@m uk-child-width-1-1" uk-grid>
                            <div>
                            @if ($blogs->lastPage() > 1)
                            <ul class="uk-pagination">
                                <li class="{{ ($blogs->currentPage() == 1) ? 'disabled' : '' }}">
                                    <a href="{{ $blogs->url(1) }}">Previous</a>
                                </li>
                                @for ($i = 1; $i <= $blogs->lastPage(); $i++)
                                    <li class="{{ ($blogs->currentPage() == $i) ? 'uk-active' : '' }}">
                                        <a href="{{ $blogs->url($i) }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                <li class="{{ ($blogs->currentPage() == $blogs->lastPage()) ? 'disabled' : '' }}">
                                    <a href="{{ $blogs->url($blogs->currentPage()+1) }}" >Next</a>
                                </li>
                            </ul>
                            @endif

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="uk-width-1-3@m">
                <h4 class="uk-heading-line uk-text-bold"><span>Archive</span></h4>
                <ul class="uk-list">
                    @foreach($archive as $items)
                    <li>
                        <a href="/blog/all?q={{ \Carbon\Carbon::parse($items->created_at)->format('m')}}">{{ \Carbon\Carbon::parse($items->created_at)->isoFormat('MMMM')}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
<!--/CONTENT-->
        
@endsection