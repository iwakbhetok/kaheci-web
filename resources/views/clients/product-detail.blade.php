@extends('layouts.layout')
@section('content')
@include('components.highlight-page')
<div class="uk-section uk-section-medium uk-section-default" id="viewport">
    <div class="uk-container">
        <div class="uk-grid-small uk-child-width-expand@s uk-text-center" uk-grid>
            <div>
                <div class="uk-card">
                    <div uk-grid>
                        <div class="uk-width-auto@m">
                            <img src="{{ asset('files/products/'. $product->slug.'/original/'.$product->main_image .'') }}" alt="">
                        </div>
                        @if($product->image_1)
                        <div class="uk-width-auto@m">
                            <img src="{{ asset('files/products/'. $product->slug.'/other/'.$product->image_1 .'') }}" alt="">
                        </div>
                        @else
                        @endif
                        @if($product->image_2)
                        <div class="uk-width-auto@m">
                            <img src="{{ asset('files/products/'. $product->slug.'/other/'.$product->image_2 .'') }}" alt="">
                        </div>
                        @else
                        @endif
                        @if($product->image_3)
                        <div class="uk-width-auto@m">
                            <img src="{{ asset('files/products/'. $product->slug.'/other/'.$product->image_3 .'') }}" alt="">
                        </div>
                        @else
                        @endif
                        @if($product->image_4)
                        <div class="uk-width-auto@m">
                            <img src="{{ asset('files/products/'. $product->slug.'/other/'.$product->image_4 .'') }}" alt="">
                        </div>
                        @else
                        @endif
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-card uk-text-left">
                    <div class="uk-card uk-card-body uk-sticky" uk-sticky="animation: uk-animation-fade;offset: 75; bottom: #viewport" style="">
                    <p class="uk-margin-remove-bottom"> {{ $product->category_name }} </p>
                    <h3 class="uk-margin-remove-top text-title-section">{{ $product->name }}</h3>
                    <p>Diskon:</p>
                    <p style={{ $product->discount == 0 ? "color:red;" : "color:green;" }}>{{ $product->discount }}%</p>
                    <p>Deskripsi:</p>
                    {!! $product->description !!}
                    <div>
                        <p>Pilihan Size:</p>
                        @foreach(json_decode($product->available_size) as $size )
                            <a class="uk-button uk-button-default uk-text-capitalize btn-order">{{ $size }}</a>
                        @endforeach
                    </div>
                    <div>
                        <p>Pilihan Warna:</p>
                        @if($product->available_colors != 'null')
                            @foreach(json_decode($product->available_colors) as $color )
                            <div class="color-option" style="background-color: {{ $color }};"></div>
                            @endforeach
                        @else
                        <p>Saat ini tidak ada pilihan warna</p>
                        @endif
                        
                    </div>
                    <h3 class="uk-margin-remove-top text-title-section">Rp. {{ number_format($product->unit_price, 0) }} </h3>
                    <a class="uk-button uk-button-default btn-order" target="_blank" href="http://line.me/ti/p/{{ isset(GlobalVar::show_cs('line')[0]->value) ? GlobalVar::show_cs('line')[0]->value : "" }}" style="background-color: #25d366; color: #ffffff;">Line</a>
                    &nbsp;or&nbsp;
                    <a class="uk-button uk-button-default btn-order" target="_blank" href="https://api.whatsapp.com/send?phone={{ isset(GlobalVar::show_cs('wa')[0]->value) ? GlobalVar::show_cs('wa')[0]->value : "" }}&text=Saya%20tertarik%20dengan%20produk%20{{ $product->name }}" style="background-color: #25d366; color: #ffffff;">Whatsapp</a>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('components.related-product')
@endsection