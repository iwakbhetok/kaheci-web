<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kaheci Home</title>
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/css/uikit.min.css" />
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto Slab' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300&display=swap" rel="stylesheet">

    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/js/uikit-icons.min.js"></script>
    <style>
    body, html{
        font-size: 100%;
        font-family: 'Roboto', sans-serif;
        }
    .text-highlight-slide{
        font-family: 'Roboto Slab', sans-serif;
        font-weight: bolder;
    }
    .text-color-one{
        color: #c4996c;
        font-size: 1.5em;
        font-weight: bold;
    }
    .button-cta-category{
        background-color: #fff;
        border-radius: 25px;
        border-color:#dcdcdc;
    }
    .button-cta-category:hover{
        color:#a89285;
    }
    .arrow-category{
        background-color: #c4996c;
        color:#fff;
        border-radius: 15px;
        height: 20px;
        width: 20px;
    }
    .text-category-grid{
        color: #000;
        margin-left:10px;
        text-align: left;
    }
    .text-category-grid > span{
        display: block;
        text-align: left;
        font-size: 1.5em;
    }
    .product-highlight-new{
        width: 50px;
        height: 50px;
        background-color: #c4996c;
        border-radius: 25px;
        margin-left:10px;
        margin-top:10px;
    }
    .product-highlight-new > span{
        color:#fff;
    }
    .product-highlight-sale{
        width: 50px;
        height: 50px;
        background-color: #f8a045;
        border-radius: 25px;
        margin-left:10px;
        margin-top:10px;
    }
    .product-highlight-sale > span{
        color:#fff;
    }
    .product-name{
        color: #000;
        font-size: 1em;
        font-weight: bold;
    }
    .overlay {
        position: absolute;
        top:59.5%;
        bottom: 0;
        height: 10%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color: #ffe3c6;
    }
    .container:hover .overlay {
       opacity: 1;
    }
    .text {
        color: #c4996c;
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        text-align: center;
    }
    .countdown-text{
        color:#c4996c;
        font-size: 2.5rem;
        font-weight:bold;
        text-align: left;
    }
    .coming-soon{
        display: inline-block;
        vertical-align: middle;
    }
    .button-coming-soon{
        background-color: #c4996c;
        color: #fff;
        font-weight: bold;
        border-radius:25px;
    }
    .button-coming-soon:hover{
        color: #eaeaea;
    }
    .date-text{
        font-family: 'Roboto', sans-serif;
        font-size: 4.5rem;
    }
    .title-blog{
        color: #000;
        margin-top:0;
    }
    .title-blog:hover{
        text-decoration: none;
    }
    .text-title-section{
        font-size:2rem;
    }
    .blog-category{
        background-color: #c4996c;
        color:#fff;
        width: 120px;
        height: 30px;
        margin-left:40px;
        margin-top:10px;
        font-size: 12px;
    }
    </style>
</head>
<body>
    <div>
        @include('components.header')
        @include('components.slider')

        <div class="uk-section-default uk-section uk-padding-remove-vertical">
            <div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin-large-top uk-margin-remove-top" uk-grid>
                <div>
                    <div class="uk-light uk-inline">
                        <p class="uk-position-center-left text-category-grid">Handsock &amp; <span>Manset</span></p>
                        <img src="{{ asset('images/handsock-manset-category.jpg') }}" alt="">
                    </div>
                </div>
                <div>
                    <div class="uk-light uk-inline">
                        <p class="uk-position-center-left text-category-grid">Sarung <span>Tangan</span></p>
                        <img src="{{ asset('images/sarung-tangan-category.jpg') }}" alt="">
                    </div>
                </div>
                <div>
                    <div class="uk-light uk-inline">
                        <p class="uk-position-center-left text-category-grid">Kaos <span>Kaki</span></p>
                        <img src="{{ asset('images/kaos-kaki-category.jpg') }}" alt="">
                    </div>
                </div>
                <div>
                    <div class="uk-light uk-inline">
                        <p class="uk-position-center-left text-category-grid"><span>Jilbab</span></p>
                        <img src="{{ asset('images/jilbab-category.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-section uk-section-medium uk-section-default">
            <div class="uk-container">

                <h3 class="uk-text-center text-title-section">Produk Unggulan</h3>

                <div class="uk-grid-medium uk-child-width-expand@s uk-text-center uk-animation-fade" uk-grid>
                    <div>
                        <div class="uk-card">
                            <div class="container">
                                <div class="product-highlight-new uk-position-left uk-flex uk-flex-middle">
                                    <span class="uk-position-center">NEW</span>
                                </div>
                                <a href="#">
                                    <img src="{{ asset('images/produk-unggulan/handsock-manset-1.jpg') }}" alt="">
                                    <div class="overlay">
                                        <div class="text">View Detail</div>
                                    </div>
                                </a>
                            </div>
                            <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                                <span class="uk-text-emphasis uk-margin-remove-top">Handsock &amp; Manset</span>
                                <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">Handsock Aisyah Jersey Maroon </p>
                                <p class="text-color-one uk-margin-remove-top">Rp. 85.000</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <div class="container">
                                <div class="product-highlight-sale uk-position-left uk-flex uk-flex-middle">
                                    <span class="uk-position-center">SALE</span>
                                </div>
                                <a href="#">
                                    <img src="{{ asset('images/produk-unggulan/sarung-tangan.jpg') }}" alt="">
                                    <div class="overlay">
                                        <div class="text">View Detail</div>
                                    </div>
                                </a>
                            </div>
                            <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                                <span class="uk-text-emphasis uk-margin-remove-top">Handsock &amp; Manset</span>
                                <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">Handsock Aisyah Jersey Maroon </p>
                                <p class="text-color-one uk-margin-remove-top">Rp. 85.000</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <div class="container">
                                <div class="product-highlight-new uk-position-left uk-flex uk-flex-middle">
                                    <span class="uk-position-center">NEW</span>
                                </div>
                                <a href="#">
                                    <img src="{{ asset('images/produk-unggulan/kaos-kaki.jpg') }}" alt="">
                                    <div class="overlay">
                                        <div class="text">View Detail</div>
                                    </div>
                                </a>
                            </div>
                            <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                                <span class="uk-text-emphasis uk-margin-remove-top">Handsock &amp; Manset</span>
                                <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">Handsock Aisyah Jersey Maroon </p>
                                <p class="text-color-one uk-margin-remove-top">Rp. 85.000</p>
                            </div>
                        </div>
                    </div>
                    <div>   
                        <div class="uk-card">
                            <div class="container">
                                <div class="product-highlight-sale uk-position-left uk-flex uk-flex-middle">
                                    <span class="uk-position-center">SALE</span>
                                </div>
                                <a href="#">
                                    <img src="{{ asset('images/produk-unggulan/handsock-manset-2.jpg') }}" alt="">
                                    <div class="overlay">
                                        <div class="text">View Detail</div>
                                    </div>
                                </a>
                            </div>
                            <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                                <span class="uk-text-emphasis uk-margin-remove-top">Handsock &amp; Manset</span>
                                <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">Handsock Aisyah Jersey Maroon </p>
                                <p class="text-color-one uk-margin-remove-top">Rp. 85.000</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="uk-section uk-section-small uk-section-default">
            <div class="uk-cover-container" uk-scrollspy="cls:uk-animation-fade; repeat: true">
                <div class="uk-background-image@m uk-background-cover uk-background-muted uk-height-large" style="background-image: url({{ asset('images/bg-countdown.jpg') }});">
                    <div class="uk-grid-large uk-child-width-expand@s uk-text-center uk-padding" uk-grid>
                        <div class="uk-padding-large">
                            <div class="uk-card uk-padding-large">
                                <a class="uk-button button-coming-soon" href="#">Segera Hadir <span uk-icon="arrow-right"></span></a>
                            </div>
                        </div>
                        <div>
                            <div class="uk-card">
                                <p class="uk-h4 uk-margin-remove uk-visible@m uk-light uk-text-uppercase countdown-text">produk baru <br> hansock humaira <br> abu</p>
                                <div class="uk-grid-small uk-child-width-expand@s uk-text-center" uk-grid>
                                    <div>
                                        <div class="uk-card uk-card-default date-text">14</div>
                                    </div>
                                    <div>
                                        <div class="uk-card uk-card-default date-text">3</div>
                                    </div>
                                    <div>
                                        <div class="uk-card uk-card-default date-text">55</div>
                                    </div>
                                    <div>
                                        <div class="uk-card uk-card-default date-text">07</div>
                                    </div>
                                </div>
                                <div class="uk-grid-small uk-child-width-expand@s uk-text-center" uk-grid>
                                    <div>
                                        <div class="uk-card uk-card-default">Days</div>
                                    </div>
                                    <div>
                                        <div class="uk-card uk-card-default">Hours</div>
                                    </div>
                                    <div>
                                        <div class="uk-card uk-card-default">Mins</div>
                                    </div>
                                    <div>
                                        <div class="uk-card uk-card-default">Secs</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-section uk-section-small uk-section-default">
            <div class="uk-container">
                
                <ul class="uk-flex-center" uk-tab>
                    <li><a href="#">Unggulan</a></li>
                    <li><a href="#">Dijual</a></li>
                    <li><a href="#">Terbaru</a></li>
                </ul>

                <ul class="uk-switcher uk-margin">
                    <li>
                        <div class="uk-grid-medium uk-child-width-expand@s uk-text-center uk-animation-slide-left-medium" uk-grid>
                            <div>
                                <div class="uk-card">
                                    <div class="container">
                                        <div class="product-highlight-sale uk-position-left uk-flex uk-flex-middle">
                                            <span class="uk-position-center">SALE</span>
                                        </div>
                                        <a href="#">
                                            <img src="{{ asset('images/product-filter/1.jpg')}}" alt="">
                                            <div class="overlay">
                                                <div class="text">View Detail</div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                                        <span class="uk-text-emphasis uk-margin-remove-top">Handsock &amp; Manset</span>
                                        <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">Handsock Aisyah Jersey Maroon </p>
                                        <p class="text-color-one uk-margin-remove-top">Rp. 85.000</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="uk-card">
                                    <div class="container">
                                        <div class="product-highlight-sale uk-position-left uk-flex uk-flex-middle">
                                            <span class="uk-position-center">SALE</span>
                                        </div>
                                        <a href="#">
                                            <img src="{{ asset('images/product-filter/2.jpg')}}" alt="">
                                            <div class="overlay">
                                                <div class="text">View Detail</div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                                        <span class="uk-text-emphasis uk-margin-remove-top">Handsock &amp; Manset</span>
                                        <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">Handsock Aisyah Jersey Maroon </p>
                                        <p class="text-color-one uk-margin-remove-top">Rp. 85.000</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="uk-card">
                                    <div class="container">
                                        <div class="product-highlight-sale uk-position-left uk-flex uk-flex-middle">
                                            <span class="uk-position-center">SALE</span>
                                        </div>
                                        <a href="#">
                                            <img src="{{ asset('images/product-filter/3.jpg')}}" alt="">
                                            <div class="overlay">
                                                <div class="text">View Detail</div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                                        <span class="uk-text-emphasis uk-margin-remove-top">Handsock &amp; Manset</span>
                                        <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">Handsock Aisyah Jersey Maroon </p>
                                        <p class="text-color-one uk-margin-remove-top">Rp. 85.000</p>
                                    </div>
                                </div>
                            </div>
                            <div>   
                                <div class="uk-card">
                                    <div class="container">
                                        <div class="product-highlight-sale uk-position-left uk-flex uk-flex-middle">
                                            <span class="uk-position-center">SALE</span>
                                        </div>
                                        <a href="#">
                                            <img src="{{ asset('images/product-filter/4.jpg')}}" alt="">
                                            <div class="overlay">
                                                <div class="text">View Detail</div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="uk-text-left uk-padding-small uk-padding-remove-top">
                                        <span class="uk-text-emphasis uk-margin-remove-top">Handsock &amp; Manset</span>
                                        <p class="product-name uk-margin-remove-top uk-margin-remove-bottom">Handsock Aisyah Jersey Maroon </p>
                                        <p class="text-color-one uk-margin-remove-top">Rp. 85.000</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                    <div class="uk-grid-medium uk-child-width-expand@s uk-text-center uk-animation-slide-left-medium" uk-grid>
                            <div>
                                <div class="uk-card">
                                    <img src="{{ asset('images/product-filter/1.jpg')}}" alt="">
                                </div>
                            </div>
                            <div>
                                <div class="uk-card">
                                    <img src="{{ asset('images/product-filter/3.jpg')}}" alt="">
                                </div>
                            </div>
                            <div>
                                <div class="uk-card">
                                    <img src="{{ asset('images/product-filter/4.jpg')}}" alt="">
                                </div>
                            </div>
                            <div>
                                <div class="uk-card">
                                    <img src="{{ asset('images/product-filter/2.jpg')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                    <div class="uk-grid-medium uk-child-width-expand@s uk-text-center uk-animation-slide-left-medium" uk-grid>
                            <div>
                                <div class="uk-card">
                                    <img src="{{ asset('images/product-filter/4.jpg')}}" alt="">
                                </div>
                            </div>
                            <div>
                                <div class="uk-card">
                                    <img src="{{ asset('images/product-filter/3.jpg')}}" alt="">
                                </div>
                            </div>
                            <div>
                                <div class="uk-card">
                                    <img src="{{ asset('images/product-filter/2.jpg')}}" alt="">
                                </div>
                            </div>
                            <div>
                                <div class="uk-card">
                                    <img src="{{ asset('images/product-filter/1.jpg')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

            </div>
        </div>

        <div class="uk-section uk-section-small uk-section-default" uk-scrollspy="cls:uk-animation-fade; repeat: true">
            <div class="uk-container">

                <h3 class="uk-text-center text-title-section">Blog Kaheci</h3>

                <div class="uk-grid-medium uk-child-width-expand@s uk-text-left" uk-grid>
                    <div class="uk-card">
                        <a href="#">
                        <div class="blog-category uk-position-left uk-flex uk-flex-middle">
                            <span class="uk-position-center">Story</span>
                        </div>
                        </a>
                        <a href="#">
                        <img src="{{ asset('images/blogs/1.jpg') }}" alt="">
                        </a>
                        <p class="uk-comment-meta uk-margin-remove-bottom"><a class="uk-link-reset" href="#">By Admin Content</a></p>
                        <a href="#">
                            <p class="title-blog">4 Style hijab untuk anda yang suka bepergian keluar rumah.</p>
                        </a>
                    </div>
                    <div class="uk-card">
                        <a href="#">
                        <div class="blog-category uk-position-left uk-flex uk-flex-middle">
                            <span class="uk-position-center">Tips &amp; trik</span>
                        </div>
                        </a>
                        <a href="#">
                        <img src="{{ asset('images/blogs/2.jpg') }}" alt="">
                        </a>
                        <p class="uk-comment-meta uk-margin-remove-bottom"><a class="uk-link-reset" href="#">By Admin Content</a></p>
                        <a href="#">
                            <p class="title-blog">Tips nyaman menggunakan hansock dalam keseharian anda.</p>
                        </a>
                    </div>
                    <div class="uk-card">
                        <a href="#">
                        <div class="blog-category uk-position-left uk-flex uk-flex-middle">
                            <span class="uk-position-center">Seputar Marketing</span>
                        </div>
                        </a>
                        <a href="#">
                        <img src="{{ asset('images/blogs/3.jpg') }}" alt="">
                        </a>
                        <p class="uk-comment-meta uk-margin-remove-bottom"><a class="uk-link-reset" href="#">By Admin Content</a></p>
                        <a href="#">
                            <p class="title-blog">Apa bahan yang digunakan untuk pembuatan hansock.</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @include('components.footer')
        @include('components.modal-search')
    </div>
    
</body>
</html>