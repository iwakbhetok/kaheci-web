<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','{{ config('myconfig.GTM_ID') }}' );</script>
    <!-- End Google Tag Manager -->
    <!-- Google Analytics -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', '{{ config('myconfig.GA_ID') }}', 'auto');
    ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '{{ config('myconfig.PIXEL_ID') }}');
    fbq('track', 'PageView');
    </script>
    <noscript>
    <img height="1" width="1" style="display:none" 
        src="https://www.facebook.com/tr?id={{ config('myconfig.PIXEL_ID') }}&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ isset($active['meta-title']) ? $active['meta-title'] : 'Kaheci | '.rand() }}</title>
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/css/uikit.min.css" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto Slab' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300&display=swap" rel="stylesheet">
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>

    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.2.0/js/uikit-icons.min.js"></script>
    <style>
    body, html{
        font-size: 100%;
        font-family: 'Roboto', sans-serif;
    }
    .uk-pagination .disabled {
      pointer-events: none;
      cursor: default;
    }
    .text-highlight-slide{
        font-family: 'Roboto Slab', sans-serif;
        font-weight: bolder;
    }
    .text-highlight-slide-mobile{
        font-family: 'Roboto', sans-serif;
        font-weight: 700;
        font-size: 1.8em;
    }
    .description-slider{
        font-size: 0.8em;
        /* color: #979797; */
    }
    .text-color-one{
        color: #c4996c;
        font-size: 1.5em;
        font-weight: bold;
    }
    .text-color-one-mobile{
        color: #c4996c;
        font-size: 0.8em;
        font-weight: bold;
    }
    .button-cta-category{
        background-color: #fff;
        border-radius: 25px;
        border-color:solid 10px #d2b291;
        margin: 0;
    }
    .button-cta-category:hover{
        color:#a89285;
    }
    .arrow-category{
        background-color: #c4996c;
        color:#fff;
        border-radius: 15px;
        height: 20px;
        width: 20px;
    }
    .text-category-grid{
        color: #000;
        margin-left:10px;
        text-align: left;
    }
    .text-category-grid > span{
        display: block;
        text-align: left;
        font-size: 1.5em;
    }
    .product-highlight-yellow {
        background-color: #f8a046 !important;
        width: 50px;
        height: 50px;
        border-radius: 25px;
        margin-left:10px;
        margin-top:10px;
    }
    .product-highlight-new{
        width: 50px;
        height: 50px;
        background-color: #c4996c;
        border-radius: 25px;
        margin-left:10px;
        margin-top:10px;
    }
    .product-highlight-new > span{
        color:#fff;
    }

    .product-highlight-yellow > span{
        color:#fff;
    }

    .product-highlight-new-mobile{
        width: 40px;
        height: 40px;
        background-color: #c4996c;
        border-radius: 20px;
        margin-left:10px;
        margin-top:10px;
    }
    .product-highlight-new-mobile > span{
        color:#fff;
        font-size: 0.6em;
    }
    .product-highlight-sale{
        width: 50px;
        height: 50px;
        background-color: #f8a045;
        border-radius: 25px;
        margin-left:10px;
        margin-top:10px;
    }
    .product-highlight-sale > span{
        color:#fff;
    }
    .product-highlight-sale-mobile{
        width: 40px;
        height: 40px;
        background-color: #f8a045;
        border-radius: 20px;
        margin-left:10px;
        margin-top:10px;
    }
    .product-highlight-sale-mobile > span{
        color:#fff;
        font-size: 0.6em;
    }
    .product-name{
        color: #000;
        font-size: 1em;
        font-weight: bold;
    }
    .product-name-mobile{
        color: #000;
        font-size: 0.8em;
        font-weight: bold;
    }
    .label-product-mobile > span{
        color:#333;
        font-size: 0.4em;
    }
    .overlay {
        position: relative;
        top:59.5%;
        bottom: 0;
        height: 10%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color: #ffe3c6;
    }
    .container-product:hover .overlay-product-detail {
       height: 20%;
       opacity: 1;
    }
    .overlay-product-detail{
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: #ffe3c6;
        overflow: hidden;
        width: 100%;
        height: 0;
        transition: .5s ease;
    }
    .text {
        color: #c4996c;
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        text-align: center;
    }

    .container-prod {
        display: flex;
        position:relative;
        max-width: 230px;
        max-height: 230px;
        border: solid 1px #eaeaea;
    }
    .overlay_wrapper{
        position: absolute;
        width: 100%;
        height: 50px;
        bottom: 0;
    }
    .overlay-prod {
        position: relative;
        width: 100%;
        height: 100%;
        background-color: #ffe3c6;
        overflow: hidden;
        transition: .5s ease;
    }
    .text-prod {
        color: #c4996c;
        font-size: 1rem;
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        text-align: center;
    }

    .countdown-text{
        color:#c4996c;
        font-size: 2.5rem;
        font-weight:bold;
        text-align: left;
    }
    .countdown-text-mobile{
        color:#c4996c;
        font-size: 1.5rem;
        font-weight:bold;
        text-align: center;
    }
    .coming-soon{
        display: inline-block;
        vertical-align: middle;
    }
    .button-coming-soon{
        background-color: #c4996c;
        color: #fff;
        font-weight: bold;
        border-radius:25px;
    }
    .button-coming-soon:hover{
        color: #eaeaea;
    }
    .date-text{
        font-family: 'Roboto', sans-serif;
        font-size: 4.5rem;
    }
    .date-text-mobile{
        font-family: 'Roboto', sans-serif;
        font-size: 1.5rem;
    }
    .time-text-mobile{
        font-family: 'Roboto', sans-serif;
        font-size: 0.6rem;
    }
    .title-blog{
        color: #333;
        margin-top:0;
    }
    .title-blog:hover{
        text-decoration: none;
    }
    .link-title-blog:hover{
        text-decoration: none;
    }
    .text-title-section{
        font-size:2rem;
    }
    .blog-category{
        background-color: #c4996c;
        color:#fff;
        width: 120px;
        height: 30px;
        margin-left:40px;
        margin-top:10px;
        font-size: 12px;
    }
    .button-send-message{
        background-color: #c4996c;
        color:#fff;
    }
    #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
    }
    .circle-totop{
        height: 50px;
        width: 50px;
        background-color: #c4996c;
        border-radius: 25px;
        text-align: center;
        vertical-align: middle;
        line-height: 50px; 
    }
    .circle-totop a{
        color:#fff;
    }
    .btn-order{
        border-radius: 25px;
    }
    .color-option{
        width: 40px;
        height: 40px;
        border-radius: 20px;
        display: inline-block;
    }
    .item-keunggulan h1{
        padding-top:20px;
        margin-bottom:0px;
        color:#c4996c;
        font-size: 0.9em;
        font-weight: bold;
    }
    .item-keunggulan p{
        margin-top:5px;
        font-size:0.8em;
    }
    .label-show-result-mobile{
        border: solid 1px #e5e5e5;
    }

    /* combobox product */
    .dropdown {
        width: 100%;
        display: inline-block;
        border:1px solid #e5e5e5;
        position: relative;
        font-size: 14px;
        height: 100%;
        text-align: left
    }
    .dropdown .select {
        cursor: pointer;
        display: block;
        padding: 10px
    }
    .dropdown .select > i {
        font-size: 13px;
        color: #888;
        cursor: pointer;
        /* transition: all .3s ease-in-out; */
        float: right;
        line-height: 20px
    }
    .dropdown:active {
        background-color: #f8f8f8
    }
    .dropdown.active:hover,
    .dropdown.active {
        background-color: #f8f8f8
    }
    .dropdown.active .select > i {
        transform: rotate(-90deg)
    }
    .dropdown .dropdown-menu {
        position: absolute;
        background-color: #fff;
        width: 100%;
        left: 0;
        margin-top: 1px;
        box-shadow: 0 1px 2px rgb(204, 204, 204);
        border-radius: 0 1px 5px 5px;
        overflow: hidden;
        display: none;
        max-height: 144px;
        overflow-y: auto;
        z-index: 9
    }
    .dropdown .dropdown-menu li {
        padding: 10px;
        /* transition: all .2s ease-in-out; */
        cursor: pointer
    } 
    .dropdown .dropdown-menu {
        padding: 0;
        list-style: none
    }
    .dropdown .dropdown-menu li:hover {
        background-color: #f2f2f2
    }
    .dropdown .dropdown-menu li:active {
        background-color: #e2e2e2
    }
    .pagination>*{
        flex: none;
        padding-left: 20px;
        position: relative;
    }
    .pagination{
        display: flex;
        flex-wrap: wrap;
        margin-left: -20px;
        padding: 0;
        list-style: none;
    }
    .note-video-clip {
        width: 100% !important;
    }
    .product_list_container ul{
        margin: 0;
        padding:0;
    }
    .product_list_container li{
        list-style-type: none;
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .product_item_wrapper{
        max-width: 900px;
        margin: 0 auto;
        display: flex;
        border: solid 1px #eaeaea;
    }
    .product_image_wrapper{
        flex: 1;
        text-align: left;
        max-width: 140px;
    }
    .product_image_wrapper img{
        height: 140px;
        width: 140px;
    }
    .product_description_wrapper{
        flex:2;
        display: flex;
        flex-direction: column;
        text-align: left;
        padding:10px;
    }
    .product_detail_wrapper{
        flex:1;
        margin: auto;
    }
    .product_name{
        flex:1;
    }
    .product_category{
        flex:1;
    }
    .product_price{
        flex:1;
    }
    .btn-viewDetail{
        background-color: #ffe3c6;
        color:#c4996c;
    }
    #gridContainer{
        padding-top: 15px;
    }
    .products_grid_container{
        display: flex;
        max-width: 100%;
        justify-content: space-between;
    }
    .products_grid_container ul{
        margin:0;
        padding: 0;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
    }
    .products_grid_container li{
        list-style-type: none;
        /* display: inline-block; */
        width: calc(100% / 3);
        /* margin: 5px; */
    }
    .product_grid_image_wrapper{
        /* height: 246px;
        width: 246px; */
    }
    .whatsapp_footer{
        position:fixed;
        width:60px;
        height:60px;
        bottom:40px;
        right:40px;
        background-color:#25d366;
        color:#FFF;
        border-radius:50px;
        text-align:center;
        font-size:30px;
        box-shadow: 2px 2px 3px #999;
        z-index:100;
    }
    .whatsapp_footer_icon{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
    }
    .color_wrapper_container{
        display: flex;
        max-width: 300px;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
    }
    .color_wrapper{
        margin: 5px;
        max-height: 32px;
    }
    @media only screen and (max-width: 768px) {
        /* For mobile phones: */
        .container-prod {
            display: flex;
            position:relative;
            max-width: 100%;
            max-height: 300px;
        }
        .product_grid_image_wrapper{
            width: 100%;
        }
        .products_grid_container li{
            width: 100%;
        }
        .whatsapp_footer{
            position:fixed;
            width:60px;
            height:60px;
            bottom:25px;
            right:25px;
            background-color:#25d366;
            color:#FFF;
            border-radius:50px;
            text-align:center;
            font-size:30px;
            box-shadow: 2px 2px 3px #999;
            z-index:100;
        }
  
    }

    </style>
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ config('myconfig.GTM_ID') }}"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div>
        @if ($message = Session::get('success'))
        <div class="uk-alert-success" uk-alert>
            <a class="uk-alert-close" uk-close></a>
            <p>{{ $message }}</p>
        </div>
        @endif

        @if ($message = Session::get('error'))
        <div class="uk-alert-danger" uk-alert>
            <a class="uk-alert-close" uk-close></a>
            <p>{{ $message }}</p>
        </div>
        @endif

        @if ($message = Session::get('warning'))
        <div class="uk-alert-warning" uk-alert>
            <a class="uk-alert-close" uk-close></a>
            <p>{{ $message }}</p>
        </div>
        @endif

        @if ($message = Session::get('info'))
        <div class="uk-alert-primary" uk-alert>
            <a class="uk-alert-close" uk-close></a>
            <p>{{ $message }}</p>
        </div>
        @endif

        @if ($errors->any())
        <div class="uk-alert-danger" uk-alert>
            <a class="uk-alert-close" uk-close></a>
            <p>Please check the form below for errors</p>
        </div>
        @endif
        @include('components.header')

        @yield('content')

        @include('components.footer')
        @include('components.modal-search')
    </div>
    <script defer src="{{ asset('js/script.js')}}"></script>
    @stack('scripts')
</body>
</html>