<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Dashboard | {{ config('app.name', 'Kaheci - Web Application') }}</title>
    <meta name="description" content="Responsive, Bootstrap, BS4">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="{{ asset('admin/assets/css/site.min.css') }}">

    <!-- include libraries(jQuery, bootstrap) -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    {{-- <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script> --}}
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />

    <!-- include summernote css/js -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
    <style type="text/css">
        .popover {
            z-index: 0 !important;
        }
        .panel-heading .note-icon-caret {
            display: none;
        }
        .panel-heading {
            background: #c1bcbc;
        }
        .note-editable b{
            font-weight:800;
        }
        .note-editing-area {
            background: #ffffff;
        }

    </style>
</head>
<style>
.upload-btn-wrapper, .upload-btn-wrapper-main, .upload-btn-wrapper-image-1, .upload-btn-wrapper-image-2, .upload-btn-wrapper-image-3, .upload-btn-wrapper-image-4 {
  position: relative;
  overflow: hidden;
  display: inline-block;
}

.upload-btn-wrapper input[type=file], .upload-btn-wrapper-main input[type=file], .upload-btn-wrapper-image-1 input[type=file], .upload-btn-wrapper-image-2 input[type=file], .upload-btn-wrapper-image-3 input[type=file], .upload-btn-wrapper-image-4 input[type=file]{
  font-size: 100px;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}
.note-toolbar, .note-editable{
    border:1px solid;
    border-color: rgba(135,150,165,.15);
}
</style>

@include('admin.components.sidebar')
    <body class="layout-row">

    <div id="main" class="layout-column flex">
        
        @include('admin.components.header')

        <div id="content" class="flex">
            <div>

                @yield('content')
                <!--div class="page-hero page-container" id="page-hero">
                    <div class="padding d-flex">
                        <div class="page-title">
                            <h2 class="text-md text-highlight">Blank</h2><small class="text-muted">Start application in a blank page</small></div>
                        <div class="flex"></div>
                    </div>
                </div>
                <div class="page-content page-container" id="page-content">
                    <div></div>
                </div-->
            </div>
        </div>
        
        @include('admin.components.footer')

    </div>
    <script src="{{ asset('admin/assets/js/site.js') }}"></script>
    <script src="{{ asset('admin/assets/js/script.js') }}"></script>
    <script src="{{ asset('admin/assets/js/plugins/jscolor.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
    
    @yield('plugin_js')
</body>

</html>