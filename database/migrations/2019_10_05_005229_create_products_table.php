<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sku_number')->default('null');
            $table->string('name');
            $table->string('description');
            $table->string('main_image')->default('null');
            $table->string('image_1')->default('null');
            $table->string('image_2')->default('null');
            $table->string('image_3')->default('null');
            $table->string('image_4')->default('null');
            $table->string('thumbnail')->default('null');
            $table->string('category_id');
            $table->string('quantity_per_unit')->default('0');
            $table->string('unit_price')->default('0');
            $table->string('available_size')->default('null');
            $table->string('available_colors')->default('null');
            $table->string('size')->default('null');
            $table->string('color')->default('null');
            $table->string('discount')->default('0');
            $table->string('unit_weight')->default('0');
            $table->string('unit_in_stock')->default('0');
            $table->enum('status_product_available',['0','1'])->default('0');
            $table->enum('product_type', ['new','sale', 'available', 'premium','coming soon'])->default('new');
            $table->enum('status', ['new', 'read', 'remove', 'spam'])->default('new');
            $table->char('no_telp_cs',15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
