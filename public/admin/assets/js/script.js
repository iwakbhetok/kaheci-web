$( window ).on("load",function() {
    
    $("#btn-trash").on('click', function(e){
        var messageIDs = $(".id_message:checkbox:checked").map(function(){
            return $(this).val();
        }).get(); 
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        if (messageIDs && messageIDs.length) {
            var r = confirm("Apakah anda yakin akan menghapus pesan ini?");
            if(r == true){
                $.ajax({
                    url: "/admin/deleteMsg",
                    method: 'post',
                    data: {
                        messageIDs: messageIDs,
                    },
                    success: function(result){
                        if(result.msg === 'success'){
                            window.location.reload();
                        }
                        console.log(result.msg);
                    }});
            }else{
                return false;
            }
            
         } else {
            alert("Anda belum memilih pesan yang akan dihapus.");
         }

    })


});