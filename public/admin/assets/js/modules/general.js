(function ($) {
    "use strict";

    $("#all_product_sidebar").click(function(){
        window.location.href = '/admin/products';
    })

    $("#add_product_sidebar").click(function(){
        window.location.href = '/admin/product/add';
    })

    $("#category_product_sidebar").click(function(){
        window.location.href = '/admin/product/category';
    })

    $("#all_blog_sidebar").click(function(){
        window.location.href = '/admin/blogs';
    })

    $("#add_blog_sidebar").click(function(){
        window.location.href = '/admin/blog/add';
    })

    $("#all_category_blog_sidebar").click(function(){
        window.location.href = '/admin/blog/category';
    })

    $("#all_tag_blog_sidebar").click(function(){
        window.location.href = '/admin/blog/tag';
    })

    $("#all_page_sidebar").click(function(){
        window.location.href = '/admin/pages';
    })

    $("#add_page_sidebar").click(function(){
        window.location.href = '/admin/pages/add';
    })

    $("#all_contact_sidebar").click(function(){
        window.location.href = '/admin/contact_master';
    })

    $("#add_contact_sidebar").click(function(){
        window.location.href = '/admin/contact_master/add';
    })

    $("#all_testimonial_sidebar").click(function(){
        window.location.href = '/admin/testimonial';
    })

    $("#add_testimonial_sidebar").click(function(){
        window.location.href = '/admin/testimonial/add';
    })

    $("#all_message_sidebar").click(function(){
        window.location.href = '/admin/messages';
    })

    $("#all_user_sidebar").click(function(){
        window.location.href = '/admin/admins';
    })

    $("#all_general_sidebar").click(function(){
        window.location.href = '/admin/general';
    })

    $("#all_menu_sidebar").click(function(){
        window.location.href = '/admin/menu';
    })

    $('#image_header_banner').on("change", function(){ readFileDesktop(this); });

            function readFileDesktop(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                    reader.onload = function (e) {

                        $('.desktop .banner_header .preview').attr("style", "display:inline-block;").html('<img width="100%" src="' + e.target.result + '" />');
                        $('.desktop .banner_header .upload-btn-wrapper').attr("style", "display:none;");      
                        $('.desktop .banner_header .my-4').attr("style", "display:none;");
                        $(".desktop .banner_header .upload-btn-wrapper").attr('display','none');
                        $('.desktop .banner_header .btnCloseDesktop').attr("style", "display:inline-block;");
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.btnCloseDesktop').on("click", function(e){
                e.preventDefault();
                $('.desktop .banner_header .preview').attr("style", "display:none;");
                $('.desktop .banner_header .btnCloseDesktop').attr("style", "display:none;");
                $('.desktop .banner_header .image p').attr("style", "display:inline-block;");
                $('.desktop .banner_header .upload-btn-wrapper').attr("style", "display:inline-block;");
            });            


            $('#image_header_banner_mobile').on("change", function(){ readFileMobile(this); });

            function readFileMobile(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                    reader.onload = function (e) {

                        $('.mobile .banner_header .preview').attr("style", "display:inline-block;").html('<img width="100%" src="' + e.target.result + '" />');
                        $('.mobile .banner_header .upload-btn-wrapper').attr("style", "display:none;");      
                        $('.mobile .banner_header .my-4').attr("style", "display:none;");
                        $(".mobile .banner_header .upload-btn-wrapper").attr('display','none');
                        $('.mobile .banner_header .btnCloseMobile').attr("style", "display:inline-block;");
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.mobile .banner_header .btnCloseMobile').on("click", function(e){
                e.preventDefault();
                $('.mobile .banner_header .preview').attr("style", "display:none;");
                $('.mobile .banner_header .btnCloseMobile').attr("style", "display:none;");
                $('.mobile .banner_header .image p').attr("style", "display:inline-block;");
                $('.mobile .banner_header .upload-btn-wrapper').attr("style", "display:inline-block;");
            });            


            //batas image prod banner script
            $('#image_prod_banner_desktop').on("change", function(){ readFileProdBannerDesktop(this); });

            function readFileProdBannerDesktop(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                    reader.onload = function (e) {

                        $('.desktop .prod_banner .preview').attr("style", "display:inline-block;").html('<img width="100%" src="' + e.target.result + '" />');
                        $('.desktop .prod_banner .upload-btn-wrapper').attr("style", "display:none;");      
                        $('.desktop .prod_banner .my-4').attr("style", "display:none;");
                        $(".desktop .prod_banner .upload-btn-wrapper").attr('display','none');
                        $('.desktop .prod_banner .btnCloseProdBannDesktop').attr("style", "display:inline-block;");
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.desktop .prod_banner .btnCloseProdBannDesktop').on("click", function(e){
                e.preventDefault();
                $('.desktop .prod_banner .preview').attr("style", "display:none;");
                $('.desktop .prod_banner .btnCloseProdBannDesktop').attr("style", "display:none;");
                $('.desktop .prod_banner .image p').attr("style", "display:inline-block;");
                $('.desktop .prod_banner .upload-btn-wrapper').attr("style", "display:inline-block;");
            });            


            $('#image_prod_banner_mobile').on("change", function(){ readFileProdBannerMobile(this); });

            function readFileProdBannerMobile(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                    reader.onload = function (e) {

                        $('.mobile .prod_banner .preview').attr("style", "display:inline-block;").html('<img width="100%" src="' + e.target.result + '" />');
                        $('.mobile .prod_banner .upload-btn-wrapper').attr("style", "display:none;");      
                        $('.mobile .prod_banner .my-4').attr("style", "display:none;");
                        $(".mobile .prod_banner .upload-btn-wrapper").attr('display','none');
                        $('.mobile .prod_banner .btnCloseProdBannMobile').attr("style", "display:inline-block;");
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.mobile .prod_banner .btnCloseProdBannMobile').on("click", function(e){
                e.preventDefault();
                $('.mobile .prod_banner .preview').attr("style", "display:none;");
                $('.mobile .prod_banner .btnCloseProdBannMobile').attr("style", "display:none;");
                $('.mobile .prod_banner .image p').attr("style", "display:inline-block;");
                $('.mobile .prod_banner .upload-btn-wrapper').attr("style", "display:inline-block;");
            });    

        // Batas logo header script
            $('#image_logo_header').on("change", function(){ readFileLogoHeader(this); });

            function readFileLogoHeader(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                    reader.onload = function (e) {

                        $('.header .preview').attr("style", "display:inline-block;").html('<img width="100%" src="' + e.target.result + '" />');
                        $('.header .upload-btn-wrapper').attr("style", "display:none;");      
                        $('.header .my-4').attr("style", "display:none;");
                        $(".header .upload-btn-wrapper").attr('display','none');
                        $('.header .btnLogoHeaderClose').attr("style", "display:inline-block;");
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.header .btnLogoHeaderClose').on("click", function(e){
                e.preventDefault();
                $('.header .preview').attr("style", "display:none;");
                $('.header .btnLogoHeaderClose').attr("style", "display:none;");
                $('.header .image p').attr("style", "display:inline-block;");
                $('.header .upload-btn-wrapper').attr("style", "display:inline-block;");
            });            

        // Batas logo footer script
            $('#image_logo_footer').on("change", function(){ readFileLogoFooter(this); });

            function readFileLogoFooter(input) {
                if (input.files && input.files[0]) {
                var reader = new FileReader();
                    reader.onload = function (e) {

                        $('.footer .preview').attr("style", "display:inline-block;").html('<img width="100%" src="' + e.target.result + '" />');
                        $('.footer .upload-btn-wrapper').attr("style", "display:none;");      
                        $('.footer .my-4').attr("style", "display:none;");
                        $(".footer .upload-btn-wrapper").attr('display','none');
                        $('.footer .btnLogoFooterClose').attr("style", "display:inline-block;");
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.footer .btnLogoFooterClose').on("click", function(e){
                e.preventDefault();
                $('.footer .preview').attr("style", "display:none;");
                $('.footer .btnLogoFooterClose').attr("style", "display:none;");
                $('.footer .image p').attr("style", "display:inline-block;");
                $('.footer .upload-btn-wrapper').attr("style", "display:inline-block;");
            });

})(jQuery);