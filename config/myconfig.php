<?php

return [
    'GTM_ID' => env('GTAG_MANAGER_ID', ''),
    'GA_ID' => env('GA_ID', ''),
    'PIXEL_ID' => env('PIXEL_ID', ''),
];