<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 
Route::get('/', 'HomeController@index')->name('index');
Route::get('/produk', 'ProductController@index')->name('product');
Route::get('/pages/{slug}', 'PagesController@show')->name('detail.pages');
Route::get('/produk-detail/{slug}', 'ProductController@show')->name('detail.product');
// Route::get('/tentang-kami', 'HomeController@about')->name('about');
// Route::get('/program-reseller', 'HomeController@reseller')->name('reseller');
Route::get('/testimonial', 'HomeController@testimonial')->name('testimonial');
Route::get('/kontak-kami', 'HomeController@contact')->name('contact');
Route::post('/message/save', 'MessageController@store')->name('message.store');
Route::get('/category/{categoryName}', 'ProductController@index')->name('category');

Route::post('/search', 'ProductSearchController@index')->name('search.product');

Route::get('/blog/all', 'BlogController@index')->name('all.blog');
Route::get('/blog/{blogSlug}', 'BlogController@show')->name('detail.blog');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login')->name('client.login.submit');

Route::prefix('ajax')->name('ajax.')->group(function(){
    Route::get('premium', 'AjaxController@getPremiumProduct')->name('premium');
    Route::get('sale', 'AjaxController@getSaleProduct')->name('sale');
    Route::get('new', 'AjaxController@getNewProduct')->name('new');
    Route::get('available', 'AjaxController@getAvailProduct')->name('available');
    Route::get('soon', 'AjaxController@getSoonProduct')->name('soon');

    Route::get('color/{color?}', 'AjaxController@getColorProduct')->name('color');
});

        //preview blog
    Route::get('/preview/{blogSlug}', 'BlogController@preview')->name('preview.blog')->middleware('admin');
    Route::get('/page-preview/{pagesSlug}', 'PagesController@preview')->name('pagepreview.blog')->middleware('admin');

Route::prefix('admin')->name('admin.')->namespace('Admin')->group(function(){
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login')->name('login.submit');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('register','Auth\LoginController@showRegisterPage');
    Route::post('register', 'Auth\LoginController@register')->name('register.submit');

    Route::group(['middleware' => ['admin']], function () {

        Route::get('kontak-kami', 'ContactUsController@index')->name('contact_us');
        Route::post('kontak-kami/save', 'ContactUsController@save')->name('contact_us.save');

        // profile
        Route::get('profile','HomeController@showProfile');


        // dashboard
        Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');


        // product
        Route::get('products', 'ProductController@index')->name('products');
        Route::get('product/add', 'ProductController@create')->name('add.product');
        Route::post('product/save', 'ProductController@store')->name('create.product');
        Route::get('product/edit/{id}', 'ProductController@edit')->name('edit.product');
        Route::post('product/update', 'ProductController@update')->name('update.product');
        Route::get('product/remove/{id}', 'ProductController@destroy')->name('remove.product');
        Route::post('product/removeall', 'ProductController@destroyAll')->name('removeall.product');

        // contact master
        Route::get('contact_master', 'ContactController@index')->name('contact');
        Route::get('contact_master/add', 'ContactController@create')->name('add.contact');
        Route::post('contact_master/save', 'ContactController@store')->name('create.contact');
        Route::get('contact_master/edit/{id}', 'ContactController@edit')->name('edit.contact');
        Route::post('contact_master/update', 'ContactController@update')->name('update.contact');
        Route::get('contact_master/remove/{id}', 'ContactController@destroy')->name('remove.contact');
        Route::post('contact_master/removeall', 'ContactController@destroyAll')->name('removeall.contact');

        //category product
        Route::get('product/category', 'ProductCategoryController@index')->name('category.product');
        Route::get('category/add', 'ProductCategoryController@create')->name('create.category');
        Route::get('category/edit/{id}', 'ProductCategoryController@edit')->name('edit.category');
        Route::post('category/save', 'ProductCategoryController@store')->name('store.category');
        Route::post('category/update', 'ProductCategoryController@update')->name('update.category');
        Route::get('category/remove/{id}', 'ProductCategoryController@destroy')->name('remove.category');
        Route::post('category/removeall', 'ProductCategoryController@destroyAll')->name('removeall.category');


        //blogs
        Route::get('blogs', 'BlogController@index')->name('blogs');
        Route::get('blog/add', 'BlogController@create')->name('create.blog');
        Route::post('blog/save', 'BlogController@store')->name('store.blog');
        Route::get('blog/edit/{id}', 'BlogController@edit')->name('edit.blog');
        Route::post('blog/update', 'BlogController@update')->name('update.blog');
        Route::get('blog/remove/{id}', 'BlogController@destroy')->name('remove.blog');
        Route::post('blog/removeall', 'BlogController@destroyAll')->name('removeall.blog');


        //category blog
        Route::get('blog/category', 'BlogCategoryController@index')->name('category.blog');
        Route::get('blog/category/add', 'BlogCategoryController@create')->name('add.category.blog');
        Route::post('blog/category/save', 'BlogCategoryController@store')->name('create.category.blog');
        Route::get('blog/category/edit/{id}', 'BlogCategoryController@edit')->name('remove.category.blog');
        Route::post('blog/category/update', 'BlogCategoryController@update')->name('update.category.blog');
        Route::get('blog/category/remove/{id}', 'BlogCategoryController@destroy')->name('remove.category.blog');
        Route::post('blog/category/removeall', 'BlogCategoryController@destroyAll')->name('removeall.category.blog');


        //tags blog
        Route::get('blog/tag', 'BlogTagController@index')->name('tag.blog');
        Route::get('blog/tag/add', 'BlogTagController@create')->name('add.tag.blog');
        Route::post('blog/tag/save', 'BlogTagController@store')->name('create.tag.blog');
        Route::get('blog/tag/edit/{id}', 'BlogTagController@edit')->name('edit.tag.blog');
        Route::post('blog/tag/update', 'BlogTagController@update')->name('update.tag.blog');
        Route::get('blog/tag/remove/{id}', 'BlogTagController@destroy')->name('remove.tag.blog');
        Route::post('blog/tag/removeall', 'BlogTagController@destroyAll')->name('removeall.tag.blog');


        //menu setting
        Route::get('menu', 'MenuController@index')->name('menu');
        Route::get('menu/add', 'MenuController@create')->name('add.menu');
        Route::post('menu/save', 'MenuController@store')->name('create.menu');
        Route::get('menu/edit/{id}', 'MenuController@edit')->name('edit.menu');
        Route::post('menu/update', 'MenuController@update')->name('update.menu');
        Route::get('menu/remove/{id}', 'MenuController@destroy')->name('remove.menu');
        Route::post('menu/removeall', 'MenuController@destroyAll')->name('removeall.menu');

        //pages
        Route::get('pages', 'PagesController@index')->name('pages');
        Route::get('pages/add', 'PagesController@create')->name('add.pages');
        Route::post('pages/save', 'PagesController@store')->name('create.pages');
        Route::get('pages/edit/{id}', 'PagesController@edit')->name('edit.pages');
        Route::post('pages/update', 'PagesController@update')->name('update.pages');
        Route::get('pages/remove/{id}', 'PagesController@destroy')->name('remove.pages');
        Route::post('pages/removeall', 'PagesController@destroyAll')->name('removeall.pages');

        //testimonial
        Route::get('testimonial', 'TestimonialController@index')->name('show.testimonial');
        Route::get('testimonial/add', 'TestimonialController@create')->name('add.testimonial');
        Route::post('testimonial/save', 'TestimonialController@store')->name('create.testimonial');
        Route::get('testimonial/edit/{id}', 'TestimonialController@edit')->name('update.TestimonialController');
        Route::post('testimonial/update', 'TestimonialController@update')->name('update.TestimonialController');    
        Route::get('testimonial/remove/{id}', 'TestimonialController@destroy')->name('remove.TestimonialController');
        Route::post('testimonial/removeall', 'TestimonialController@destroyAll')->name('removeall.TestimonialControllerg');

        //general
        Route::get('general', 'GeneralController@index')->name('logo');
        Route::post('general/save_logo_header', 'GeneralController@save_logo_header')->name('save_header.logo');
        Route::post('general/save_logo_footer', 'GeneralController@save_logo_footer')->name('save_footer.logo');
        Route::post('general/save_banner', 'GeneralController@save_banner')->name('SaveBanner');
        Route::post('general/save_prod_banner', 'GeneralController@save_prod_banner')->name('SaveProdBanner');

        // users
        Route::get('admins', 'AdminsController@index')->name('admins');
        Route::get('create', 'AdminsController@create')->name('create');
        Route::post('save', 'AdminsController@store')->name('save');
        Route::get('edit/{id}', 'AdminsController@edit')->name('edit');
        Route::post('update', 'AdminsController@update')->name('update');
        Route::get('remove/{id}', 'AdminsController@destroy')->name('remove');

        //messages
        Route::get('messages', 'HomeController@showMessages')->name('show.messages');
        Route::get('messages/{type}', 'HomeController@showMessages')->name('show.messages.type');
        Route::get('messages/detail/{id}', 'HomeController@detailMessages')->name('detail.messages');
        Route::post('deleteMsg', 'HomeController@deleteMsg')->name('delete.message');

        Route::get('settings', 'HomeController@showSettings')->name('show.settings');

        

        Route::post('ajax_file_upload_handler', 'ProductCategoryController@uploadImageCategory')->name('upload.image.category');
    
    });
});

Route::get('/home', 'HomeController@index')->name('home');
