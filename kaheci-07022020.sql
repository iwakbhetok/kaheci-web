-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 07, 2020 at 02:59 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `senyumu_kaheci_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` char(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `no_telp`, `created_at`, `updated_at`) VALUES
(1, 'Test', 'admin@demo.com', '$2y$10$Mnw/XLOlKibXLrMy7PVROeyNypHmUR5CRhAKtzkGCtLjxF1WFne96', '087829946140', '2019-10-12 01:32:59', '2019-10-12 01:32:59'),
(2, 'john joe', 'admin@admin.com', '$2y$10$4cDx69vaXVgjo8js4jjb1.h8nWlxM8ODnL2GK153G50en7gkMOwuK', '087829946140', '2019-10-13 20:09:21', '2019-10-13 20:09:21'),
(3, 'contoh', 'admin@admin2.com', '$2y$10$87d4C8fsMqkfi4XKFJav7epom2rtVZarK5gTuxGpnAby9.qB0FTgO', '087829946140', '2020-01-18 10:23:55', '2020-01-18 10:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(191) DEFAULT NULL,
  `image_mobile` varchar(191) DEFAULT NULL,
  `url` varchar(191) NOT NULL,
  `text_url` varchar(191) NOT NULL,
  `type` enum('top','coming_soon') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `name`, `description`, `image`, `image_mobile`, `url`, `text_url`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Koleksi Handsock Terbaik Kualitas Premium', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.', 'header-banner.jpg', 'header-banner.jpg', 'http://localhost:8000/', 'Lihat Kategori', 'top', '2020-02-02 06:48:52', '2020-02-06 16:27:53');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '[]',
  `published` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `admin_id`, `blog_category_id`, `title`, `slug`, `image`, `description`, `tags`, `published`, `created_at`, `updated_at`) VALUES
(18, 1, 7, 'Tips Kecantikan Ala Kaheci', 'tips-kecantikan-ala-kaheci', '1580489920-IERMfK58keMsskNiYSQlyAUzQ8RHCcFW.jpg', '<p style=\"text-align: justify;\"><font color=\"#000000\" face=\"Open Sans, Arial, sans-serif\"><b>Where can I get some?</b></font><p style=\"text-align: justify;\"><font color=\"#000000\" face=\"Open Sans, Arial, sans-serif\"><a href=\"http://www.google.com\" target=\"_blank\">There</a> are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</font></p><p style=\"text-align: center;\"><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/Uhr1z8JTz1Q\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><span style=\'font-size: 0.875rem; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\'><br></span></p><h2 style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Why do we use it?</h2><p style=\'margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif;\'>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p><p style=\"text-align: left;\"><br></p><p style=\"text-align: center;\"><img data-filename=\"3.jpg\" style=\"width: 489px;\" src=\"/images/blogs/15806284730.png\"><span style=\'color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\'><br></span></p><p></p><div style=\"text-align: left;\"><br></div><span style=\"text-align: justify;\"><div style=\"text-align: left;\"><h2 style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Where can I get some?</h2><p style=\'margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif;\'>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p></div></span><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p></p>\n', '[\"7\"]', '1', '2019-12-05 09:58:40', '2020-02-06 09:30:55'),
(19, 2, 8, 'Tips Diet Ala Millenials', 'tips-diet-ala-millenials', '1580675765-j2O5jJXFxIVKhnCd6sAVnNJZn8dZkLqw.jpeg', '<p><strong style=\'margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\'>Lorem Ipsum</strong><span style=\'color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\'>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><p></p><div style=\"text-align: center;\"><img data-filename=\"2.jpg\" style=\"color: rgb(0, 0, 0); font-size: 0.875rem; width: 489px;\" src=\"/images/blogs/15806285120.png\"></div><h2 style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\"><br></h2><h2 style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Where can I get some?</h2><p style=\'margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif;\'>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p><p></p></p>\n', '[\"8\",\"7\"]', '1', '2020-01-31 09:58:40', '2020-02-02 13:36:05'),
(20, 1, 7, 'Kecantikan Naturan Ala Kaheci', 'kecantikan-naturan-ala-kaheci', '1580491098-9yxE0hDJ9aRZER6WRNFzDlfd4IKfDIUE.jpg', '<p style=\"text-align: left;\"><strong style=\'margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\'>Lorem Ipsum</strong><span style=\'color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\'>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><p></p><div style=\"text-align: center;\"><img data-filename=\"3ea342d338e6a26954d11a6a8b5fdf0f.jpg\" src=\"/images/blogs/15806287610.png\" style=\"color: rgb(0, 0, 0); font-size: 0.875rem; width: 640.656px;\"></div><div style=\"text-align: center;\"><br></div><h2 style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Where can I get some?</h2><div style=\"text-align: center;\"><p style=\'margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif;\'>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p></div><p></p><p></p><p></p></p>\n', '[\"7\"]', '1', '2020-01-31 10:18:18', '2020-02-02 01:15:50');

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

DROP TABLE IF EXISTS `blog_categories`;
CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `slug`, `description`, `status`, `created_at`, `updated_at`) VALUES
(7, 'Kecantikan', 'kecantikan', 'Ini adalah kecantikan', '1', '2020-01-31 05:16:25', '2020-01-31 05:16:25'),
(8, 'Tips', 'tips', 'tips', '1', '2020-01-31 10:14:23', '2020-01-31 10:14:23');

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

DROP TABLE IF EXISTS `blog_tags`;
CREATE TABLE IF NOT EXISTS `blog_tags` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_tags`
--

INSERT INTO `blog_tags` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(7, 'Kecantikan', 'kecantikan', '1', '2020-01-31 09:58:18', '2020-01-31 09:58:18'),
(8, 'Diet', 'diet', '1', '2020-01-31 10:14:35', '2020-01-31 10:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `logos`
--

DROP TABLE IF EXISTS `logos`;
CREATE TABLE IF NOT EXISTS `logos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` enum('favicon','header','footer') NOT NULL,
  `image` varchar(191) NOT NULL,
  `description` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logos`
--

INSERT INTO `logos` (`id`, `location`, `image`, `description`, `created_at`, `updated_at`) VALUES
(2, 'footer', 'logo-footer.jpg', 'dsadsa', '2020-02-06 18:55:52', '2020-02-06 18:57:10');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('new','read','remove','spam','sent','draft') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `subject`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Jhon Doe', 'jhon@email.com', 'Berikut ini judul pesan', 'Berikut ini keterangan pesan', 'new', '2019-10-14 00:38:48', '2019-10-14 00:38:48'),
(2, 'Namaku', 'alamat@email.com', 'judul pesan saya adalah', 'isi pesan saya adalah', 'new', '2019-11-17 07:18:28', '2019-11-17 07:18:28'),
(3, 'Handsock', 'sularso.moch@gmail.com', 'Tanya', 'Hai', 'new', '2020-01-28 12:44:14', '2020-01-28 12:44:14'),
(4, 'Handsock', 'sularso.moch@gmail.com', 'Tanya', 'tes', 'new', '2020-01-28 12:45:44', '2020-01-28 12:45:44');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2019_10_04_221311_create_messages_table', 1),
(7, '2019_10_05_005146_create_product_categories_table', 1),
(8, '2019_10_05_005229_create_products_table', 1),
(9, '2019_10_05_005405_create_testimonials_table', 1),
(11, '2019_10_06_151104_create_blog_categories_table', 1),
(12, '2019_10_06_151748_create_blog_tags_table', 1),
(13, '2019_10_12_080142_create_admins_table', 1),
(14, '2019_11_05_024253_create_about_table', 2),
(15, '2019_10_06_151006_create_blogs_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `admin_id`, `name`, `slug`, `image`, `description`, `published`, `created_at`, `updated_at`) VALUES
(14, 2, 'Tentang Kami', 'tentang-kami', '1580617165-RF7gOslOZ5NOoP40j2R9lVz085jMKnD2.png', '<p>Tentang Kami<br></p>\n', '1', '2020-02-01 21:19:25', '2020-02-01 21:19:25'),
(15, 2, 'Cara Pemesanan', 'cara-pemesanan', '1580617182-Oy5bdHJ4Ckji9WwGDDvB1FncBYPtGQTh.png', '<p>Cara Pemesanan&nbsp;&nbsp;&nbsp;&nbsp;<br></p>\n', '1', '2020-02-01 21:19:42', '2020-02-01 21:19:42'),
(17, 2, 'Cara Pembayaran', 'cara-pembayaran', '1580617229-vNOiCIRSLCDVobiyJ1UA5XqTgRc9PyR7.png', '<p>Cara Pembayaran<br></p>\n', '1', '2020-02-01 21:20:29', '2020-02-01 21:20:29'),
(18, 2, 'Karir', 'karir', '1580618124-jmvpEmVb5VEhJ7UM6nEH8qbP1Da9DJls.png', '<p>Karir<br></p>\n', '1', '2020-02-01 21:35:24', '2020-02-01 21:35:24');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sku_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity_per_unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `unit_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `available_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `available_colors` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `unit_weight` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `unit_in_stock` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status_product_available` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `product_type` enum('new','sale','available','premium','coming soon') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `status` enum('new','read','remove','spam') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `no_telp_cs` char(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku_number`, `name`, `slug`, `description`, `main_image`, `image_1`, `image_2`, `image_3`, `image_4`, `thumbnail`, `category_id`, `quantity_per_unit`, `unit_price`, `available_size`, `available_colors`, `size`, `color`, `discount`, `unit_weight`, `unit_in_stock`, `status_product_available`, `product_type`, `status`, `no_telp_cs`, `created_at`, `updated_at`) VALUES
(25, 'fdsaffdksjaf', 'Milla Brukat Navy', 'jazeerah-red-velvet-1580671438', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.', '1580672751-jazeerah-red-velvet-1580671438.jpg', '1-jazeerah-red-velvet-1580671438.jpg', '2-jazeerah-red-velvet-1580671438.png', '3-jazeerah-red-velvet-1580671438.jpg', '4-jazeerah-red-velvet-1580671438.jpg', 'thumb-jazeerah-red-velvet-1580671438.jpg', '40', '0', '1100000', '[\"all\"]', '[\"#e66465\"]', NULL, NULL, '0', '0', '0', '1', 'coming soon', 'new', '087829946140', '2020-02-02 12:23:59', '2020-02-02 12:48:10'),
(26, 'kdnsadksa', 'Jazeera Turkey Red', 'jazeera-turkey-red-1580673274', 'Jazeera Turkey Red', '1580674171-jazeera-turkey-red-1580673274.jpg', NULL, NULL, NULL, NULL, 'thumb-jazeera-turkey-red-1580673274.jpg', '43', '0', '200000', '[\"all\"]', 'null', NULL, NULL, '0', '0', '0', '1', 'new', 'new', '087829946140', '2020-02-02 12:54:34', '2020-02-02 14:44:46'),
(27, 'kjhkljh', 'Reenata Green', 'reenata-green-1580674022', 'fhwlkfjhjflkh', '1580674228-reenata-green-1580674022.jpg', NULL, NULL, NULL, NULL, 'thumb-reenata-green-1580674022.jpg', '40', '0', '300000', '[\"l\"]', '[\"#e66465\"]', NULL, NULL, '0', '0', '0', '1', 'new', 'new', '087829946140', '2020-02-02 13:07:02', '2020-02-02 13:10:28'),
(28, 'kjfhdfk', 'Beibeh Hitam', 'beibeh-hitam-1580674126', 'lkjdkhdfkljhf', '1580674126-beibeh-hitam-1580674126.jpg', NULL, NULL, NULL, NULL, 'thumb-beibeh-hitam-1580674126-1580674127.jpg', '40', '0', '80000', '[\"all\"]', '[\"#e66465\"]', NULL, NULL, '0', '0', '0', '1', 'new', 'new', '087829946140', '2020-02-02 13:08:47', '2020-02-02 13:08:47'),
(29, 'kkkk', 'Hansock Amoera', 'hansock-amoera-1580675637', 'Hansock Amoera', '1580675668-hansock-amoera-1580675637.jpg', NULL, NULL, NULL, NULL, 'thumb-hansock-amoera-1580675637.jpg', '40', '0', '90000', '[\"xs\",\"l\",\"all\"]', 'null', NULL, NULL, '0', '0', '0', '1', 'sale', 'new', '087829946140', '2020-02-02 13:33:57', '2020-02-02 13:34:28'),
(30, 'kjnkj', 'H&M', 'kjnjkn-1580678896', 'kklmlmklm', '1580679586-kjnjkn-1580678896.jpg', '1-kjnjkn-1580678896.jpg', NULL, NULL, NULL, 'thumb-kjnjkn-1580678896.jpg', '40', '0', '9090', '[\"xs\"]', '[\"#e66465\"]', NULL, NULL, '80', '0', '0', '1', 'sale', 'new', '087829946140', '2020-02-02 14:28:16', '2020-02-02 14:39:47'),
(31, 'second123', 'Three Second', 'three-second-1580679028', 'Three Second', '1580679028-three-second-1580679028.jpg', '1-three-second-1580679028-1580679028.jpg', NULL, NULL, NULL, 'thumb-three-second-1580679028-1580679028.jpg', '42', '0', '800000', '[\"l\",\"xl\"]', 'null', NULL, NULL, '8', '0', '0', '1', 'sale', 'new', '087829946140', '2020-02-02 14:30:28', '2020-02-02 14:44:36'),
(32, 'kdjsalkdja', 'Vanilla Hijab Merah Maroon', 'vanilla-hijab-merah-maroon-1580686620', 'Vanilla Hijab Merah Maroon', '1580686620-vanilla-hijab-merah-maroon-1580686620.jpg', NULL, NULL, NULL, NULL, 'thumb-vanilla-hijab-merah-maroon-1580686620-1580686620.jpg', '42', '0', '400000', '[\"m\"]', '[\"#e66465\"]', NULL, NULL, '10', '0', '0', '1', 'sale', 'new', '087829946140', '2020-02-02 16:37:00', '2020-02-06 13:34:31'),
(33, 'elzatta989', 'Elzatta Blue Navy', 'elzatta-blue-navy-1580686675', 'Elzatta Blue Navy', '1580686675-elzatta-blue-navy-1580686675.jpg', NULL, NULL, NULL, NULL, 'thumb-elzatta-blue-navy-1580686675-1580686675.jpg', '42', '0', '500000', '[\"m\",\"l\"]', '[\"#e66465\"]', NULL, NULL, '10', '0', '0', '1', 'premium', 'new', '087829946140', '2020-02-02 16:37:55', '2020-02-02 16:37:55'),
(34, '98asas', 'Senada Dress Green A1', 'senada-dress-green-a1-1580686747', 'Senada Dress Green A1', '1580686747-senada-dress-green-a1-1580686747.jpg', NULL, NULL, NULL, NULL, 'thumb-senada-dress-green-a1-1580686747-1580686747.jpg', '42', '0', '800000', '[\"s\"]', '[\"#e66465\"]', NULL, NULL, '0', '0', '0', '1', 'premium', 'new', '087829946140', '2020-02-02 16:39:07', '2020-02-02 16:39:07'),
(35, 'jsld78', 'Baju Gamis Brukat A2', 'baju-gamis-brukat-a2-1580686807', 'Baju Gamis Brukat A2', '1580686807-baju-gamis-brukat-a2-1580686807.jpg', NULL, NULL, NULL, NULL, 'thumb-baju-gamis-brukat-a2-1580686807-1580686807.jpg', '41', '0', '700000', '[\"s\"]', '[\"#e66465\"]', NULL, NULL, '0', '0', '0', '1', 'premium', 'new', '087829946140', '2020-02-02 16:40:07', '2020-02-02 16:40:07');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

DROP TABLE IF EXISTS `product_categories`;
CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive','remove') COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(2) DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `slug`, `description`, `status`, `parent_id`, `image`, `created_at`, `updated_at`) VALUES
(39, 'Hansock & Manset', 'hansock-manset', 'Siimply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to ma', 'active', 40, '1581018413-nEWAcuZ9S2q5wdT1FlZN9m21rvPfjs4g.jpg', '2020-01-27 11:52:32', '2020-02-06 13:17:15'),
(40, 'Sarung Tangan', 'sarung-tangan', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to ma', 'active', NULL, '1581019364-4Wfokkp0zpv2GmKSYnCFWylmK6WKvXmB.jpg', '2020-01-27 11:52:32', '2020-02-06 13:02:51'),
(41, 'Jilbab Syari', 'jilbab-syari', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to ma', 'active', NULL, '1581018755-eN0n7RgGVTV1OPmFwMfTrG09TBflVZWD.jpg', '2020-01-27 11:52:32', '2020-02-06 13:18:51'),
(42, 'Kaos Kaki', 'kaos-kaki', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to ma', 'active', NULL, '1581018453-sxidOu9Mo54gYdhY9tFFmPmvjKjCMiLr.jpg', '2020-01-27 11:52:32', '2020-02-06 12:49:18'),
(43, 'Jilbab', 'jilbab', 'Aksesoris', 'active', NULL, '1581018469-SnTFGBGX01SpUobjKGr3BoT5l33aTxeV.jpg', '2020-02-02 10:32:02', '2020-02-06 12:49:41');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `description`, `slug`, `image`, `status`, `created_at`, `updated_at`) VALUES
(17, 'Kerudungnya Bagus', 'Kerudungnya Bagus', 'kerudungnya-bagus', '1580618423-ptau01ywyFwOp0D5ZDej9IIhcpnxxD53.png', '1', '2020-02-01 21:40:23', '2020-02-01 21:40:23'),
(19, 'Kaos Kakinya Bagus ya', 'Kaos Kakinya Bagus ya', 'kaos-kakinya-bagus-ya', '1580618468-afhgl0WD0H2aIcankb6hp47dVzq1llld.png', '1', '2020-02-01 21:41:08', '2020-02-01 21:41:08'),
(20, 'Kerudungnya Bagus 2', 'Kerudungnya Bagus', 'kerudungnya-bagus', '1580618423-ptau01ywyFwOp0D5ZDej9IIhcpnxxD53.png', '1', '2020-02-01 21:40:23', '2020-02-01 21:40:23'),
(21, 'Kaos Kakinya Bagus ya 2', 'Kaos Kakinya Bagus ya', 'kaos-kakinya-bagus-ya', '1580618468-afhgl0WD0H2aIcankb6hp47dVzq1llld.png', '1', '2020-02-01 21:41:08', '2020-02-01 21:41:08'),
(22, 'Kerudungnya Bagus 3', 'Kerudungnya Bagus', 'kerudungnya-bagus', '1580618423-ptau01ywyFwOp0D5ZDej9IIhcpnxxD53.png', '1', '2020-02-01 21:40:23', '2020-02-01 21:40:23'),
(23, 'Kaos Kakinya Bagus ya 4', 'Kaos Kakinya Bagus ya', 'kaos-kakinya-bagus-ya', '1580618468-afhgl0WD0H2aIcankb6hp47dVzq1llld.png', '1', '2020-02-01 21:41:08', '2020-02-01 21:41:08'),
(24, 'Kerudungnya Bagus 5', 'Kerudungnya Bagus', 'kerudungnya-bagus', '1580618423-ptau01ywyFwOp0D5ZDej9IIhcpnxxD53.png', '1', '2020-02-01 21:40:23', '2020-02-01 21:40:23'),
(25, 'Kaos Kakinya Bagus ya 6', 'Kaos Kakinya Bagus ya', 'kaos-kakinya-bagus-ya', '1580618468-afhgl0WD0H2aIcankb6hp47dVzq1llld.png', '1', '2020-02-01 21:41:08', '2020-02-01 21:41:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
