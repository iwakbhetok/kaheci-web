<?php

namespace App;

use App\Traits\NullableFields;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    use NullableFields;

    protected $fillable = [
        'name','description','image', 'slug','status'
    ];
    
    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = $this->nullIfEmpty($value);
    }
}
