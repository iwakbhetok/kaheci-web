<?php

namespace App\Exceptions;

use Exception;
use Redirect;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Intervention\Image\Exception\NotSupportedException ) {
            return Redirect::back()->with(['error' => 'Ekstensi file gambar harus jpeg, jpg, & png.']);
        }
        if ($exception instanceof \Intervention\Image\Exception\NotReadableException ) {
            return Redirect::back()->with(['error' => 'Mungkin size file gambar terlalu besar.']);
        }
        if ($exception instanceof \Illuminate\Http\Exceptions\PostTooLargeException  ) {
            return Redirect::back()->with(['error' => 'Mungkin size file gambar terlalu besar.']);
        }
        return parent::render($request, $exception);
    }
}
