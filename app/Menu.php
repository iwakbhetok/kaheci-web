<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Menu extends Model
{

    protected $fillable = [
        'name','url','location','status','row'
    ];
}
