<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banners';

    protected $fillable = [
        'name','description', 'image','image_mobile', 'url','text_url','type'
    ];
}
