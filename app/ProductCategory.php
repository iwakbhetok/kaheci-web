<?php

namespace App;

use Nestable\NestableTrait;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use NestableTrait;

    protected $parent = 'parent_id';
    
    protected $fillable = [
        'name','description','status', 'parent_id', 'image', 'slug'
    ];
}
