<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'title','description','image', 'slug', 'admin_id', 'blog_category_id', 'tags','published'
    ];
}
