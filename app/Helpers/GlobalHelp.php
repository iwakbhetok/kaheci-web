<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class GlobalHelp {
    public static function show_logo_header() {
        $result = DB::table('logos')->where('location', 'header')->get();
        $sql    = (empty($result[0]->image)) ? array() : $result[0]->image ;
        return $sql;
    }

    public static function footer() {
        $result = DB::table('logos')->where('location', 'footer')->get();
        $sql    = (empty($result[0])) ? array() : $result[0] ;
        return $sql;
    }

    public static function show_menu($loc,$row) {
        $result = DB::table('menus')->where('location', $loc)->where('status', '1')->where('row', $row)->get();
        return $result;
    }

    public static function show_cs($type) {
        $result = DB::table('contact_support')->where('status', '1')->where('type', $type)->inRandomOrder()->limit(1)->get();
        $sql    = (empty($result)) ? array() : $result ;
        return $sql;
    }
}