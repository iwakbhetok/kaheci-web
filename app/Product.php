<?php

namespace App;
use App\Traits\NullableFields;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	use NullableFields;
	
    protected $guarded = [];
    protected $fillable = [
			'sku_number',
			'name',
			'slug',
			'description',
			'main_image',
			'image_1',
			'image_2',
			'image_3',
			'image_4',
			'thumbnail',
			'category_id',
			'quantity_per_unit',
			'unit_price',
			'available_size',
			'available_colors',
			'size',
			'color',
			'discount',
			'status_product_available',
			'product_type',
			'status',
			'unit_weight'
    ];
}
