<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'about';

    protected $fillable = [
        'description','yt_video_url', 'txt_benefit', 'bg_image'
    ];
}
