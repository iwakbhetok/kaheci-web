<?php 
namespace App\Traits;

trait NullableFields {

    protected function nullIfEmpty($input)
    {
        return (($input == null) || ($input == 0)) ? '0' : "$input";
    }
}