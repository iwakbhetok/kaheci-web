<?php
use App\ProductCategory;
use Illuminate\Support\Facades\DB;

if (! function_exists('show_parent_id')) {
    function show_parent_category($parent_id)
    {
        if($parent_id != null){
            $category = ProductCategory::find($parent_id);
            return $category->name;
        }else{
            return "Utama";
        }
    }
}

if (! function_exists('buildTree')) {
    function buildTree(Array $data, $parent = 0) {
        $tree = array();
        foreach ($data as $d) {
            if ($d['parent_id'] == $parent) {
                $children = buildTree($data, $d['id']);
                // set a trivial key
                if (!empty($children)) {
                    $d['_children'] = $children;
                }
                $tree[] = $d;
            }
        }
        return $tree;
    }
}

if (! function_exists('printTree')) {
    function printTree($tree, $r = 0,$g="", $p = null) {
        foreach ($tree as $i => $t) {
            $dash = ($t['parent_id'] == 0) ? null : str_repeat('-', $r) .' ';
            $c = ($t['id'] == $g || $t['name'] == $g ? "selected" : "");
            printf("\t<option %s value='%d'>%s%s</option>\n", $c, $t['id'], $dash, $t['name']);
            if ($t['parent_id'] == $p) {
                $r = 0;
            }
            if (isset($t['_children'])) {
                printTree($t['_children'], ++$r,$g, $t['parent_id']);
            }
        }
    }
}

if (! function_exists('create_menu_category')) {
    function create_menu_category($category)
    {
        $html = '';
        foreach ($category as $item) {
           if($item->parent_id == 0){
               $childs = ProductCategory::where('parent_id', $item->id)->get();
               if($childs->isEmpty()){
                $html .= '<li><a href="/category/'.$item->slug.'">'.$item->name.'</a></li>';
               }else{
                    $html .= '<li class="uk-parent"><a href="#">'. $item->name.'</a>';
                    $html .= '<ul class="uk-nav-sub">';
                    foreach ($childs as $child) {
                        $html .= '<li><a href="/category/'.$child->slug.'">'.$child->name.'</a></li>';
                    }
                    $html .= '</ul></li>';
               }
           }
           else{
                $html .= '';
           }
        }
        return $html;
    }
}

if (! function_exists('summernote')) {
    function summernote($input,$path_name)
    {
        $detail = $input;

        $dom    = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    

        $images = $dom->getElementsByTagName('img');

        foreach($images as $k => $img){

            $data = $img->getAttribute('src');

            if (preg_match('/data:image/', $data)) {

                list($type, $data) = explode(';', $data);

                list(, $data)      = explode(',', $data);

                $data = base64_decode($data);

                $image_name= "/images/".$path_name."/" . time().$k.'.png';

                $path = public_path() . $image_name;

                file_put_contents($path, $data);

                $img->removeAttribute('src');

                $img->setAttribute('src', $image_name);
            }

        }

        $detail = $dom->saveHTML();
        
        return $detail;
    }
}

if (! function_exists('summernote_no_img')) {
    function summernote_no_img($input)
    {
        $detail = $input;

        $dom    = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
        $detail = $dom->saveHTML();
        
        return $detail;
    }
}

if (! function_exists('create_combobox_category')) {
    function create_combobox_category($categories)
    {
        $html = '<select class="custom-select" name="category">';
        $html .= '<option value="" selected="selected">Pilih kategori produk</option>';
        foreach ($categories as $item) {
           if($item->parent_id != 0){
                $html .= '<option value="'.$item->id.'"> -- '.$item->name .'</option>';
           }
           else{
                $html .= '<option value="'.$item->id.'">'.$item->name.'</option>';
           }
        }
        $html .= '</select>';
        return $html;
    }
}

if (! function_exists('update_combobox_category')) {
    function update_combobox_category($categories,$cat_id)
    {
        $html = '<select class="custom-select" name="category">';
        $html .= '<option value="" >Pilih kategori product</option>';
        foreach ($categories as $item) {
           if($item->parent_id != 0){
                $html .= '<option value="'.$item->id.'"'.($item->id == $cat_id ? "selected" :"").'> -- '.$item->name .'</option>';
           }
           else{
                $html .= '<option value="'.$cat_id.'"'.($item->id == $cat_id ? "selected" :"").'>'.$item->name.'</option>';
           }
        }
        $html .= '</select>';
        return $html;
    }
}

if (! function_exists('get_last_word')) {
    function get_last_word($str)
    {
        $pieces = explode(' ', $str);
        $last_word = array_pop($pieces);

        return $last_word;
    }
}

if (! function_exists('get_first_word')) {
    function get_first_word($str)
    {
        $arr = explode(' ', $str);
        if(count($arr) == 1){
            return '';
        }else{
            $first_word = preg_replace('/\W\w+\s*(\W*)$/', '$1', $str);
            return $first_word;
        }
    }
}

if (! function_exists('get_product_type')) {
    function get_product_type(){
        // count product premium
        $premium = DB::table('products')->join('product_categories', 'products.category_id', '=', 'product_categories.id')->selectRaw('products.*, product_categories.name as category_name')->where('products.status_product_available', '1')
        ->where('products.product_type', 'premium')
        ->where('products.status','!=', 'remove')
        ->count();

        // count product sale
        $sale = DB::table('products')->join('product_categories', 'products.category_id', '=', 'product_categories.id')->selectRaw('products.*, product_categories.name as category_name')->where('products.status_product_available', '1')
                ->where('products.product_type', 'sale')
                ->where('products.status','!=', 'remove')
                ->count();

        // count product new
        $new = DB::table('products')->join('product_categories', 'products.category_id', '=', 'product_categories.id')->selectRaw('products.*, product_categories.name as category_name')->where('products.status_product_available', '1')
                ->where('products.product_type', 'new')
                ->where('products.status','!=', 'remove')
                ->count();

        // count product available
        $available = DB::table('products')->join('product_categories', 'products.category_id', '=', 'product_categories.id')->selectRaw('products.*, product_categories.name as category_name')->where('products.status_product_available', '1')
                ->where('products.product_type', 'available')
                ->where('products.status','!=', 'remove')
                ->count();

        // count product coming soon
        $coming_soon = DB::table('products')->join('product_categories', 'products.category_id', '=', 'product_categories.id')->selectRaw('products.*, product_categories.name as category_name')->where('products.status_product_available', '1')
                ->where('products.product_type', 'coming soon')
                ->where('products.status','!=', 'remove')
                ->count();
        
        return array(
            'premium'     => $premium,
            'sale'        => $sale,
            'new'         => $new,
            'available'   => $available,
            'coming_soon' => $coming_soon
        );
    }
}

if (! function_exists('get_product_color')) {
    function get_product_color(){
        $colors = DB::table('products')->where('products.status','!=', 'remove')->whereNotIn('available_colors', ["null", ""])->get();
        $list_color = array();
        $set = [];
        foreach ($colors as $color) {
            foreach(json_decode($color->available_colors) as $each_value){
                if(!isset($set[$each_value])) $set[$each_value] = true;
            }
        }
        return $set;
    }
}