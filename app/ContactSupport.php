<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ContactSupport extends Model
{

	protected $table = "contact_support";
    protected $fillable = [
        'name','value','type','status'
    ];
}
