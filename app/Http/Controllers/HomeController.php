<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use App\Product;
use App\Blog;
use App\BlogCategory;
use App\Banner;
use App\Testimonial;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        $active = array(
            'index' => 'uk-active',
            'products' => '',
            'blogs' => '',
            'about' => '',
            'testimonial' => '',
            'reseller' => '',
            'contact' => '',
            'meta-title' => 'Official Kaheci Indonesia | Pilihan Muslimah Berhijab'
        );


        $category   = ProductCategory::where(['parent_id' => null, 'status' => 'active'])->limit(4)->get();

        $premium    = Product::where(['products.product_type' => 'premium','products.status' => 'new'])
                    ->leftJoin('product_categories', 'products.category_id', '=', 'product_categories.id')
                    ->select(
                        'product_categories.id as id_cat',
                        'product_categories.name as cat_name',
                        'products.id as id_prod',
                        'products.name as products_name',
                        'products.slug as products_url',
                        'products.discount as products_discount',
                        'products.unit_price as products_price',
                        'products.description as products_desc',
                        'products.thumbnail as products_thumb',
                        'products.product_type as products_type',
                        'products.status as products_status'
                    )->inRandomOrder()->limit(4)->get();

        $blog       = Blog::where(['published' => '1'])
                    ->leftJoin('blog_categories', 'blogs.blog_category_id', '=', 'blog_categories.id')
                    ->leftJoin('admins', 'blogs.admin_id', '=', 'admins.id')
                    ->select(
                        'blog_categories.id as id_cat',
                        'blog_categories.name as cat_name',
                        'blogs.id as id_prod',
                        'blogs.title as blog_name',
                        'blogs.admin_id as admin_id',
                        'blogs.slug as blog_url',
                        'blogs.description as blog_desc',
                        'blogs.image as blog_thumb',
                        'admins.name as admin_name'
                    )->inRandomOrder()->limit(3)->get();
        $bannerTop  = Banner::where(['type' => 'top'])->get();

        $comingsoon = Banner::where(['type' => 'coming_soon'])->get();

        $sale       = Product::where(['products.product_type' => 'sale','products.status' => 'new'])
                    ->leftJoin('product_categories', 'products.category_id', '=', 'product_categories.id')
                    ->select(
                        'product_categories.id as id_cat',
                        'product_categories.name as cat_name',
                        'products.id as id_prod',
                        'products.name as products_name',
                        'products.slug as products_url',
                        'products.discount as products_discount',
                        'products.unit_price as products_price',
                        'products.description as products_desc',
                        'products.thumbnail as products_thumb',
                        'products.product_type as products_type',
                        'products.status as products_status'
                    )->inRandomOrder()->limit(4)->get();

        $new        = Product::where(['products.product_type' => 'new','products.status' => 'new'])
                    ->leftJoin('product_categories', 'products.category_id', '=', 'product_categories.id')
                    ->select(
                        'product_categories.id as id_cat',
                        'product_categories.name as cat_name',
                        'products.id as id_prod',
                        'products.name as products_name',
                        'products.slug as products_url',
                        'products.discount as products_discount',
                        'products.unit_price as products_price',
                        'products.description as products_desc',
                        'products.thumbnail as products_thumb',
                        'products.product_type as products_type',
                        'products.status as products_status'
                    )->inRandomOrder()->limit(4)->get();

        return view('clients.index',
            [
                'active'        => $active, 
                'category'      => isset($category) ? $category : array(),
                'blogs'         => isset($blog) ? $blog : array(),
                'banner'        => isset($bannerTop) ? $bannerTop : array(),
                'comingsoon'    => isset($comingsoon) ? $comingsoon : array(),
                'premiums'      => isset($premium) ? $premium : array(),
                'sale'          => isset($sale) ? $sale : array(),
                'new'           => isset($new) ? $new : array(),
            ]
        );
    }


    public function about()
    {
        $active = array(
            'index' => '',
            'products' => '',
            'about' => 'uk-active',
            'blogs' => '',
            'testimonial' => '',
            'reseller' => '',
            'contact' => ''
        );
        return view('clients.about',['active' => $active, 'pageName' => 'Tentang Kami', 'breadcrumb' => 'Tentang kami']);
    }

    public function reseller()
    {
        $active = array(
            'index' => '',
            'products' => '',
            'about' => '',
            'blogs' => '',
            'testimonial' => '',
            'reseller' => 'uk-active',
            'contact' => ''
        );
        return view('clients.reseller',['active' => $active, 'pageName' => 'Reseller', 'breadcrumb' => 'Reseller']);
    }

    public function testimonial()
    {
        $active = array(
            'index' => '',
            'products' => '',
            'about' => '',
            'blogs' => '',
            'testimonial' => 'uk-active',
            'reseller' => '',
            'contact' => '',
            'meta-title' => 'Testimoni Pelanggan | Kaheci'
        );

        $testimonial = Testimonial::where(['status' => '1'])
                        ->orderBy('created_at','desc')->paginate(8);

        return view('clients.testimonial',
            [
                'active' => $active, 
                'pageName' => 'Testimonial', 
                'breadcrumb' => 'Testimonial',
                'testimoni' => $testimonial
            ]
        );
    }

    public function contact()
    {
        $active = array(
            'index' => '',
            'products' => '',
            'about' => '',
            'blogs' => '',
            'testimonial' => '',
            'reseller' => '',
            'contact' => 'uk-active',
            'meta-title' => 'Kontak Kami | Kaheci'
        );

        $sql = \App\ContactUs::first();

        return view('clients.contact',['active' => $active, 'pageName' => 'Kontak Kami', 'breadcrumb' => 'Kontak kami', 'items' => $sql]);
    }
}
