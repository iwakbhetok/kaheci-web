<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\ProductCategory;

class ProductSearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginate = 25;
        if(!empty($request->keyword)){
            $active = array(
                'index' => '',
                'products' => 'uk-active',
                'about' => '',
                'blogs' => '',
                'testimonial' => '',
                'reseller' => '',
                'contact' => '',
                'meta-title' => 'Semua Produk Kaheci | Kaheci'
            );
            $products = DB::table('products')
                        ->join('product_categories', 'products.category_id', '=', 'product_categories.id')
                        ->selectRaw('products.*, product_categories.name as category_name')
                        ->where('products.status_product_available', '1')
                        ->where('products.status','!=', 'remove')
                        ->where('products.name','like', '%'.$request->keyword.'%')
                        ->paginate($paginate);
            $menuCategory = ProductCategory::where(['status' => 'active'])->orderBy('name')->get();
            $get_product_type = get_product_type();
            $get_product_color = get_product_color();
            return view('clients.products',['active' => $active, 'pageName' => 'Produk', 'breadcrumb' => 'Kategori produk', 'category' => $menuCategory, 'products' => $products, 'paginate' => $paginate, 'get_product_type' => $get_product_type, 'get_product_color' => $get_product_color]);
        }else{
            $active = array(
                'index' => '',
                'products' => 'uk-active',
                'about' => '',
                'blogs' => '',
                'testimonial' => '',
                'reseller' => '',
                'contact' => '',
                'meta-title' => 'Semua Produk Kaheci | Kaheci'
            );
            $products = DB::table('products')
                        ->join('product_categories', 'products.category_id', '=', 'product_categories.id')
                        ->selectRaw('products.*, product_categories.name as category_name')
                        ->where('products.status_product_available', '1')
                        ->paginate($paginate);
            $menuCategory = ProductCategory::where(['status' => 'active'])->orderBy('name')->get();
            $get_product_type = get_product_type();
            $get_product_color = get_product_color();
            return view('clients.products',['active' => $active, 'pageName' => 'Produk', 'breadcrumb' => 'Kategori produk', 'category' => $menuCategory, 'products' => $products, 'paginate' => $paginate, 'get_product_type' => $get_product_type, 'get_product_color' => $get_product_color]);
        }
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
