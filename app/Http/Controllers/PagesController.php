<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $active = array(
            'index' => '',
            'products' => '',
            'about' => '',
            'blogs' => '',
            'testimonial' => '',
            'reseller' => '',
            'contact' => ''
        );

        if($slug == null){
            abort(404);
        }else{
            $pages = Page::where('slug',$slug)->where('published','1')->firstOrFail();
            $active = array_merge($active,['meta-title' => "Halaman ".$pages->name." | Kaheci"]);
        }

        return view('clients.page-detail', 
            [
                'active' => $active, 
                'pageName' => 'Page', 
                'breadcrumb' => 'Detail halaman', 
                'pages'     => $pages
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function preview($pagesSlug)
    { 
        $active = array(
            'index' => '',
            'products' => '',
            'about' => '',
            'blogs' => '',
            'testimonial' => '',
            'reseller' => '',
            'contact' => ''
        );
        if($pagesSlug == null){
            abort(404);
        }else{
            $pages = Page::where('slug',$pagesSlug)->firstOrFail();
            $active = array_merge($active,['meta-title' => "Halaman ".$pages->name." | Kaheci"]);
        }

        return view('clients.page-detail', 
            [
                'active' => $active, 
                'pageName' => 'Page', 
                'breadcrumb' => 'Detail halaman', 
                'pages'     => $pages
            ]
        );
    }

}
