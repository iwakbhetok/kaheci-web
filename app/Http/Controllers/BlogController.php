<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use App\Blog;
use App\BlogCategory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $active = array(
            'index' => '',
            'products' => '',
            'blogs' => 'uk-active',
            'about' => '',
            'testimonial' => '',
            'reseller' => '',
            'contact' => '',
            'meta-title' => 'Blog Kaheci | Info Terkini di Kaheci'
        );

        if($request->q != null) {
            $blog = Blog::when($request->q, function ($query) use ($request) {
                $query->where(['blogs.published' => '1', 'blog_categories.status' => '1'])
                    ->where(DB::raw("(DATE_FORMAT(blogs.created_at,'%m'))"),"=", "{$request->q}")
                    ->leftJoin('blog_categories', 'blogs.blog_category_id', '=', 'blog_categories.id')
                    ->leftJoin('admins', 'blogs.admin_id', '=', 'admins.id')
                    ->select(
                        'blog_categories.id as id_cat',
                        'blog_categories.name as cat_name',
                        'blogs.id as id_prod',
                        'blogs.title as blog_name',
                        'blogs.admin_id as admin_id',
                        'blogs.slug as blog_url',
                        'blogs.description as blog_desc',
                        'blogs.image as blog_thumb',
                        'admins.name as admin_name',
                        'blogs.created_at as blog_created',
                        'blogs.image as blog_thumbs'
                    );
            })->paginate(5);
        } else {
            $blog   = Blog::where(['blogs.published' => '1', 'blog_categories.status' => '1'])
                    ->leftJoin('blog_categories', 'blogs.blog_category_id', '=', 'blog_categories.id')
                    ->leftJoin('admins', 'blogs.admin_id', '=', 'admins.id')
                    ->select(
                        'blog_categories.id as id_cat',
                        'blog_categories.name as cat_name',
                        'blogs.id as id_prod',
                        'blogs.title as blog_name',
                        'blogs.admin_id as admin_id',
                        'blogs.slug as blog_url',
                        'blogs.description as blog_desc',
                        'blogs.image as blog_thumb',
                        'admins.name as admin_name',
                        'blogs.created_at as blog_created',
                        'blogs.image as blog_thumbs'
                    )->paginate(5);
        }

        $archive = DB::table('blogs')
                    ->where('published','1')
                    ->groupBy(DB::raw("date(created_at)"))
                    ->get();

        return view('clients.blogs',
            [
                'active'     => $active, 
                'pageName'   => 'Blog', 
                'breadcrumb' => 'Semua Artikel',
                'blogs'      => $blog,
                'archive'    => $archive
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $blogCategory = BlogCategory::get();
        // return view('admin.blogs.create_blog', ['blogCategory' => $blogCategory]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show($blogSlug)
    { 
        $meta_title = Blog::where('slug',$blogSlug)->get();

        $title      = (isset($meta_title[0]->title) ? $meta_title[0]->title : '').' | Blog Kaheci';
        $active     = array(
            'index'       => '',
            'products'    => '',
            'about'       => '',
            'blogs'       => '',
            'testimonial' => '',
            'reseller'    => '',
            'contact'     => '',
            'meta-title'  => $title
        );

        $blog   = Blog::where(['blogs.published' => '1', 'blog_categories.status' => '1','blogs.slug' => $blogSlug])
                ->leftJoin('blog_categories', 'blogs.blog_category_id', '=', 'blog_categories.id')
                ->leftJoin('admins', 'blogs.admin_id', '=', 'admins.id')
                ->select(
                    'blog_categories.id as id_cat',
                    'blog_categories.name as cat_name',
                    'blogs.id as id_prod',
                    'blogs.title as blog_name',
                    'blogs.admin_id as admin_id',
                    'blogs.slug as blog_url',
                    'blogs.description as blog_desc',
                    'blogs.image as blog_thumb',
                    'admins.name as admin_name',
                    'blogs.created_at as blog_created',
                    'blogs.image as blog_thumbs'
                )->get();

        $related   = Blog::where(['blogs.published' => '1', 'blog_categories.status' => '1'])
                ->where('blogs.slug','!=', "{$blogSlug}")
                ->orWhere('blog_categories.name', 'like', "%{$blogSlug}%")
                ->orWhere('blogs.title', 'like', "%{$blogSlug}%")
                ->leftJoin('blog_categories', 'blogs.blog_category_id', '=', 'blog_categories.id')
                ->leftJoin('admins', 'blogs.admin_id', '=', 'admins.id')
                ->select(
                    'blog_categories.id as id_cat',
                    'blog_categories.name as cat_name',
                    'blogs.id as id_prod',
                    'blogs.title as blog_name',
                    'blogs.admin_id as admin_id',
                    'blogs.slug as blog_url',
                    'blogs.description as blog_desc',
                    'blogs.image as blog_thumb',
                    'admins.name as admin_name',
                    'blogs.created_at as blog_created',
                    'blogs.image as blog_thumbs'
                )->inRandomOrder()->limit(3)->get();

        return view('clients.blog-detail',
            [
                'active'        => $active, 
                'pageName'      => 'Blog', 
                'breadcrumb'    => ucwords((isset($meta_title[0]->title) ? $meta_title[0]->title : '')),
                'blogs'         => isset($blog[0]) ? $blog[0] : $blog[0] = array(''),
                'blogs_related' => $related
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function preview($blogSlug)
    { 
        $meta_title = Blog::where('slug',$blogSlug)->get();

        $title      = (isset($meta_title[0]->title) ? $meta_title[0]->title : '').' | Blog Kaheci';
        $active     = array(
            'index'       => '',
            'products'    => '',
            'about'       => '',
            'blogs'       => '',
            'testimonial' => '',
            'reseller'    => '',
            'contact'     => '',
            'meta-title'  => $title
        );

        $blog   = Blog::where(['blogs.slug' => $blogSlug])
                ->leftJoin('blog_categories', 'blogs.blog_category_id', '=', 'blog_categories.id')
                ->leftJoin('admins', 'blogs.admin_id', '=', 'admins.id')
                ->select(
                    'blog_categories.id as id_cat',
                    'blog_categories.name as cat_name',
                    'blogs.id as id_prod',
                    'blogs.title as blog_name',
                    'blogs.admin_id as admin_id',
                    'blogs.slug as blog_url',
                    'blogs.description as blog_desc',
                    'blogs.image as blog_thumb',
                    'admins.name as admin_name',
                    'blogs.created_at as blog_created',
                    'blogs.image as blog_thumbs'
                )->get();

        $related   = Blog::where(['blogs.published' => '1', 'blog_categories.status' => '1'])
                ->where('blogs.slug','!=', "{$blogSlug}")
                ->orWhere('blog_categories.name', 'like', "%{$blogSlug}%")
                ->orWhere('blogs.title', 'like', "%{$blogSlug}%")
                ->leftJoin('blog_categories', 'blogs.blog_category_id', '=', 'blog_categories.id')
                ->leftJoin('admins', 'blogs.admin_id', '=', 'admins.id')
                ->select(
                    'blog_categories.id as id_cat',
                    'blog_categories.name as cat_name',
                    'blogs.id as id_prod',
                    'blogs.title as blog_name',
                    'blogs.admin_id as admin_id',
                    'blogs.slug as blog_url',
                    'blogs.description as blog_desc',
                    'blogs.image as blog_thumb',
                    'admins.name as admin_name',
                    'blogs.created_at as blog_created',
                    'blogs.image as blog_thumbs'
                )->inRandomOrder()->limit(3)->get();

        return view('clients.blog-detail',
            [
                'active'        => $active, 
                'pageName'      => 'Blog', 
                'breadcrumb'    => ucwords((isset($meta_title[0]->title) ? $meta_title[0]->title : '')),
                'blogs'         => isset($blog[0]) ? $blog[0] : $blog[0] = array(''),
                'blogs_related' => $related
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
    }
}
