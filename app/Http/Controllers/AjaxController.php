<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    public function getPremiumProduct()
    {
        $products = DB::table('products')->join('product_categories', 'products.category_id', '=', 'product_categories.id')->selectRaw('products.*, product_categories.name as category_name')->where('products.status_product_available', '1')
                    ->where('products.product_type', 'premium')
                    ->where('products.status','!=', 'remove')
                    ->get();
        return response()->json(array('success' => true, 'products' => $products));
    }

    public function getSaleProduct()
    {
        $products = DB::table('products')->join('product_categories', 'products.category_id', '=', 'product_categories.id')->selectRaw('products.*, product_categories.name as category_name')->where('products.status_product_available', '1')
                    ->where('products.product_type', 'sale')
                    ->where('products.status','!=', 'remove')
                    ->get();
        return response()->json(array('success' => true, 'products' => $products));
    }

    public function getNewProduct()
    {
        $products = DB::table('products')->join('product_categories', 'products.category_id', '=', 'product_categories.id')->selectRaw('products.*, product_categories.name as category_name')->where('products.status_product_available', '1')
                    ->where('products.product_type', 'new')
                    ->where('products.status','!=', 'remove')
                    ->get();
        return response()->json(array('success' => true, 'products' => $products));
    }

    public function getAvailProduct()
    {
        $products = DB::table('products')->join('product_categories', 'products.category_id', '=', 'product_categories.id')->selectRaw('products.*, product_categories.name as category_name')->where('products.status_product_available', '1')
                    ->where('products.product_type', 'available')
                    ->where('products.status','!=', 'remove')
                    ->get();
        return response()->json(array('success' => true, 'products' => $products));
    }

    public function getSoonProduct()
    {
        $products = DB::table('products')->join('product_categories', 'products.category_id', '=', 'product_categories.id')->selectRaw('products.*, product_categories.name as category_name')->where('products.status_product_available', '1')
                    ->where('products.product_type', 'coming soon')
                    ->where('products.status','!=', 'remove')
                    ->get();
        return response()->json(array('success' => true, 'products' => $products));
    }

    public function getColorProduct($color){
        $result = DB::table('products')->join('product_categories', 'products.category_id', '=', 'product_categories.id')->selectRaw('products.*, product_categories.name as category_name')->where('products.status_product_available', '1')
                ->where('products.available_colors', 'like', '%'.$color.'%')
                ->where('products.status','!=', 'remove')
                ->get();
        return response()->json(array('success' => true, 'products' => $result));
    }
}
