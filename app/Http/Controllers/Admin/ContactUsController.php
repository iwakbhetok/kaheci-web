<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ContactUs;
use Illuminate\Support\Str;

class ContactUsController extends Controller
{
    public function index(Request $request){

        $sql = ContactUs::first();
        return view('admin.contact_us.form_contact_us',['items' => isset($sql) ? $sql :'',]);
    }

    public function save(Request $request){

        $id = $request->id;

        $message = [
            'numeric'   => 'Kolom no telp harus berupa angka.',
            'required'  => 'Format email salah',
        ];

        $rules = [
            'phone'      => 'numeric',
            'email'      => 'email',
        ];

        $validator     = $request->validate($rules, $message); 

        if (!empty($id)) {
            $sql = ContactUs::find($id);
        } else {
            $sql = new ContactUs;            
        }

        $sql->address    = $request->address;
        $sql->phone      = $request->phone;
        $sql->email      = $request->email;
        $sql->open_store = $request->open_store;

        if ($sql->save()) {
                return redirect('/admin/kontak-kami')->with(['success' => 'Info kontak kami berhasil di ubah']);
        } else {
                return redirect('/admin/kontak-kami')->with(['error' => 'Info kontak kami gagal di ubah']);            
        }
    }
}
