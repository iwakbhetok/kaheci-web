<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Rules\Price;
use App\Rules\Sku;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->q != null) {
            $products = Product::when($request->q, function ($query) use ($request) {
                $query->where('name', 'like', "%{$request->q}%") 
                      ->where('status', '!=', 'remove')
                      ->orWhere('sku_number', 'like', "%{$request->q}%");
            })->paginate(10);
        } else {
            $products = Product::where('status', '!=', 'remove')->orderBy('created_at','desc')->paginate(10);
        }

        return view('admin.products.list_product', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = ProductCategory::where('status','active')->get()->toArray();
        $tree = buildTree($category);

        return view('admin.products.create_product', ['category' => $category, 'tree' => $tree]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'main_image'   => ['required','max:2048','dimensions:min_heigh=300,min_width=300','mimes:jpeg,jpg,png'],
            'image_1'      => ['nullable','max:2048','dimensions:min_height=300,min_width=300','mimes:jpeg,jpg,png'],
            'image_2'      => ['nullable','max:2048','dimensions:min_height=300,min_width=300','mimes:jpeg,jpg,png'],
            'image_3'      => ['nullable','max:2048','dimensions:min_height=300,min_width=300','mimes:jpeg,jpg,png'],
            'image_4'      => ['nullable','max:2048','dimensions:min_height=300,min_width=300','mimes:jpeg,jpg,png'],
            'sku'          => ['nullable',new Sku],
            'category'     => 'required',
            'description'  => 'required', 
            'product_type' => 'required',
            'color'        => 'required',
            'size'         => 'required',
            'name'         => 'required|unique:products,name',
            'harga'        => ['required', new price],
            'berat'        => 'digits_between:0,100',
            'discount'     => 'digits_between:0,100'
        ];

        $messages = [
            'uploaded'       => 'Kolom :attribute harus kurang dari 2MB.',
            'unique'         => 'Kolom :attribute sudah digunakan.',
            'max'            => 'Kolom :attribute harus kurang dari :max kb.',
            'min'            => 'Kolom :attribute harus lebih dari :min.',
            'required'       => 'Kolom :attribute tidak boleh kosong.',
            'image'          => 'Kolom :attribute harus berupa file gambar.',
            'dimensions'     => 'Kolom :attribute minimal 300x300 pixel dengan rasio 1:1.',
            'mimes'          => 'Kolom :attribute harus berupa gambar dengan ext:jpeg/jpg/png.',
            'numeric'        => 'Kolom :attribute harus menggunakan angka',
            'digits'         => 'Kolom :attribute harus lebih dari 0.',
            'digits_between' => 'Kolom :attribute harus berupa angka antara 0-100.',
        ];

        // resize main image
        $slug       = str_slug($request->name,'-').'-'.time();
        $main_image = $request->file('main_image');
        $images_1   = $request->file('image_1');
        $images_2   = $request->file('image_2');
        $images_3   = $request->file('image_3');
        $images_4   = $request->file('image_4');
        $mainpath   = public_path().'/files/products/'.$slug.'/original/';
        $thumbpath  = public_path().'/files/products/'.$slug.'/thumbs/';
        $otherpath  = public_path().'/files/products/'.$slug.'/other/';
        $main_img   = Image::make($main_image);
        $filename   = $slug.'.'.$main_image->getClientOriginalExtension();
        $validator  = $request->validate($rules, $messages);
        // check if directory not exist then create it
        if(!File::isDirectory($mainpath)){
            File::makeDirectory($mainpath, 0777, true, true);
        }
        if(!File::isDirectory($thumbpath)){
            File::makeDirectory($thumbpath, 0777, true, true);
        }
        if(!File::isDirectory($otherpath)){
            File::makeDirectory($otherpath, 0777, true, true);
        }

        if (!empty($images_1)) {
            $img_1  = Image::make($images_1);
            $name_1 = '1-'.$slug.'.'.$images_1->getClientOriginalExtension();
            list($width, $height) = getimagesize($images_1);

            if(($width > $height) || ($width == $height)){//landscape
              $img_1->resize(400, null, function ($constraint) {
                  $constraint->aspectRatio();
              });
              
              $img_1->resizeCanvas(400,400,'center', false, 'ffffff');
            } else {
              $img_1->resize(null,400, function ($constraint) {
                  $constraint->aspectRatio();
              });
              $img_1->resizeCanvas(400,400,'center', false, 'ffffff');
            }
            $img_1->save($otherpath.$name_1);
        }
        if (!empty($images_2)) {
            $img_2  = Image::make($images_2);
            $name_2 = '2-'.$slug.'.'.$images_2->getClientOriginalExtension();
            list($width, $height) = getimagesize($images_2);

            if( $width > $height){//landscape
              $img_2->resize(400, null, function ($constraint) {
                  $constraint->aspectRatio();
              });
              
              $img_2->resizeCanvas(400,400,'center', false, 'ffffff');
            } else {
              $img_2->resize(null,400, function ($constraint) {
                  $constraint->aspectRatio();
              });
              $img_2->resizeCanvas(400,400,'center', false, 'ffffff');
            }
            $img_2->save($otherpath.$name_2);
        }
        if (!empty($images_3)) {
            $img_3  = Image::make($images_3);
            $name_3 = '3-'.$slug.'.'.$images_3->getClientOriginalExtension();
            list($width, $height) = getimagesize($images_3);

            if( $width > $height){//landscape
              $img_3->resize(400, null, function ($constraint) {
                  $constraint->aspectRatio();
              });
              
              $img_3->resizeCanvas(400,400,'center', false, 'ffffff');
            } else {
              $img_3->resize(null,400, function ($constraint) {
                  $constraint->aspectRatio();
              });
              $img_3->resizeCanvas(400,400,'center', false, 'ffffff');
            }
            $img_3->save($otherpath.$name_3);
        }
        if (!empty($images_4)) {
            $img_4  = Image::make($images_4);
            $name_4 = '4-'.$slug.'.'.$images_4->getClientOriginalExtension();
            list($width, $height) = getimagesize($images_4);

            if( $width > $height){//landscape
              $img_4->resize(400, null, function ($constraint) {
                  $constraint->aspectRatio();
              });
              
              $img_4->resizeCanvas(400,400,'center', false, 'ffffff');
            } else {
              $img_4->resize(null,400, function ($constraint) {
                  $constraint->aspectRatio();
              });
              $img_4->resizeCanvas(400,400,'center', false, 'ffffff');
            }
            $img_4->save($otherpath.$name_4);
        }

        list($width, $height) = getimagesize($main_image);

        if( $width > $height){//landscape
          $main_img->resize(400, null, function ($constraint) {
              $constraint->aspectRatio();
          });
          
          $main_img->resizeCanvas(400,400,'center', false, 'ffffff');
        } else {
          $main_img->resize(null,400, function ($constraint) {
              $constraint->aspectRatio();
          });
          $main_img->resizeCanvas(400,400,'center', false, 'ffffff');
        }
        $main_img->save($mainpath.$filename);
        $main_img->save($thumbpath.$filename);
        $sku = empty($request->sku) ? '' : strtoupper($request->sku);
        // // start to save to database
        $sql = new Product;
        $sql->name                     = ucwords($request->name);
        $sql->slug                     = $slug;
        $sql->sku_number               = $sku;
        $sql->description              = $request->description;
        $sql->main_image               = $filename;
        $sql->image_1                  = !empty($request->image_1) ? $name_1 : null;
        $sql->image_2                  = !empty($request->image_2) ? $name_2 : null;
        $sql->image_3                  = !empty($request->image_3) ? $name_3 : null;
        $sql->image_4                  = !empty($request->image_4) ? $name_4 : null;
        $sql->thumbnail                = $filename;
        $sql->category_id              = $request->category;
        $sql->unit_price               = $request->harga;
        $sql->discount                 = $request->discount;
        $sql->unit_weight              = $request->berat;
        $sql->available_size           = json_encode($request->size);
        $sql->available_colors         = json_encode($request->color);
        $sql->status_product_available = '1';
        $sql->product_type             = $request->product_type;
        $sql->status                   = 'new';

        if($sql->save()){
            return redirect('/admin/products')->with(['success' => 'Produk baru berhasil ditambahkan.']);
        } else {
            return redirect('/admin/products')->with(['error' => 'Produk baru gagal ditambahkan.']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res = DB::table('products')
                ->select('*',
                    'products.name as name_product', 
                    'product_categories.name as name_category',
                    'products.slug as slug_product', 
                    'product_categories.slug as slug_category', 
                    'products.id as id_product', 
                    'product_categories.id as id_category',
                    'products.available_size as available_size',
                    'products.available_colors as available_colors',
                    'products.main_image as main_image',       
                    'products.description as prod_desc'             
                )
                ->join('product_categories', 'product_categories.id', '=', 'products.category_id')
                ->where('products.id', $id)
                ->where('product_categories.status','!=', 'remove')
                ->get();

        $category = ProductCategory::where('status','active')->get()->toArray();
        $tree = buildTree($category);

        if (!$res->isEmpty()) {
            return view('admin.products.edit_product', 
                [
                    'product' => $res[0], 
                    'sizes' => json_decode($res[0]->available_size), 
                    'colors' => json_decode($res[0]->available_colors),
                    'category' => $tree 
                ]
            );
        } else {
            return abort(404);                        
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id    = $request->input('id');
        $rules = [
            'main_image'   => ['nullable','max:2048','dimensions:min_height=300,min_width=300','mimes:jpeg,jpg,png'],
            'image_1'      => ['nullable','max:2048','dimensions:min_height=300,min_width=300','mimes:jpeg,jpg,png'],
            'image_2'      => ['nullable','max:2048','dimensions:min_height=300,min_width=300','mimes:jpeg,jpg,png'],
            'image_3'      => ['nullable','max:2048','dimensions:min_height=300,min_width=300','mimes:jpeg,jpg,png'],
            'image_4'      => ['nullable','max:2048','dimensions:min_height=300,min_width=300','mimes:jpeg,jpg,png'],
            'sku'          => ['nullable',new Sku],
            'description'  => 'required',
            'category'     => 'required',
            'product_type' => 'required',
            'color'        => 'required',
            'size'         => 'required',
            'name'         => 'required|unique:products,name,'.$id,
            'harga'        => ['required', new price],
            'berat'        => 'digits_between:0,100',
            'discount'     => 'digits_between:0,100'
        ];

        $messages = [
            'uploaded'       => 'Kolom :attribute harus kurang dari :max kb.',
            'unique'         => 'Kolom :attribute sudah digunakan.',
            'max'            => 'Kolom :attribute harus kurang dari :max kb.',
            'min'            => 'Kolom :attribute harus lebih dari :min.',
            'required'       => 'Kolom :attribute tidak boleh kosong.',
            'image'          => 'Kolom :attribute harus berupa file gambar.',
            'dimensions'     => 'Kolom :attribute minimal 300x300 pixel dengan rasio 1:1.',
            'mimes'          => 'Kolom :attribute harus berupa gambar dengan ext:jpeg/jpg/png.',
            'numeric'        => 'Kolom :attribute harus menggunakan angka',
            'digits'         => 'Kolom :attribute harus lebih dari 0.',
            'digits_between' => 'Kolom :attribute harus berupa angka antara 0-100.',
        ];

        // resize main image
        $slug       = $request->slug;
        $main_image = $request->file('main_image');
        $images_1   = $request->file('image_1');
        $images_2   = $request->file('image_2');
        $images_3   = $request->file('image_3');
        $images_4   = $request->file('image_4');
        $mainpath   = public_path().'/files/products/'.$slug.'/original/';
        $thumbpath  = public_path().'/files/products/'.$slug.'/thumbs/';
        $otherpath  = public_path().'/files/products/'.$slug.'/other/';
        $filename   = $request->current_image;
        $validator  = $request->validate($rules, $messages);
        $sql        = Product::find($request->id);

        if (!empty($main_image)) {
            $main_img = Image::make($main_image);
            if (File::delete($mainpath.$filename)) {
                list($width, $height) = getimagesize($main_image);

                if( ($width > $height) || ($width == $height)){//landscape
                  $main_img->resize(400, null, function ($constraint) {
                      $constraint->aspectRatio();
                  });
                  
                  $main_img->resizeCanvas(400,400,'center', false, 'ffffff');
                } else {
                  $main_img->resize(null,400, function ($constraint) {
                      $constraint->aspectRatio();
                  });
                  $main_img->resizeCanvas(400,400,'center', false, 'ffffff');
                }
                $main_img->save($mainpath.$filename);
                $main_img->save($thumbpath.$filename);
            }
        }
        if (!empty($images_1)) {
            $img_1  = Image::make($images_1);
            $name_1 = '1-'.$slug.'.'.$images_1->getClientOriginalExtension();
            list($width, $height) = getimagesize($images_1);

            if( ($width > $height) || ($width == $height)){//landscape
              $img_1->resize(400, null, function ($constraint) {
                  $constraint->aspectRatio();
              });
              
              $img_1->resizeCanvas(400,400,'center', false, 'ffffff');
            } else {
              $img_1->resize(null,400, function ($constraint) {
                  $constraint->aspectRatio();
              });
              $img_1->resizeCanvas(400,400,'center', false, 'ffffff');
            }
            $img_1->save($otherpath.$name_1);
            $sql->image_1 = $name_1;
        }
        if (!empty($images_2)) {
            $img_2  = Image::make($images_2);
            $name_2 = '2-'.$slug.'.'.$images_2->getClientOriginalExtension();
            list($width, $height) = getimagesize($images_2);

            if( ($width > $height) || ($width == $height)){//landscape
              $img_2->resize(400, null, function ($constraint) {
                  $constraint->aspectRatio();
              });
              
              $img_2->resizeCanvas(400,400,'center', false, 'ffffff');
            } else {
              $img_2->resize(null,400, function ($constraint) {
                  $constraint->aspectRatio();
              });
              $img_2->resizeCanvas(400,400,'center', false, 'ffffff');
            }
            $img_2->save($otherpath.$name_2);
            $sql->image_2 = $name_2;
        }
        if (!empty($images_3)) {
            $img_3  = Image::make($images_3);
            $name_3 = '3-'.$slug.'.'.$images_3->getClientOriginalExtension();
            list($width, $height) = getimagesize($images_3);

            if( ($width > $height) || ($width == $height)){//landscape
              $img_3->resize(400, null, function ($constraint) {
                  $constraint->aspectRatio();
              });
              
              $img_3->resizeCanvas(400,400,'center', false, 'ffffff');
            } else {
              $img_3->resize(null,400, function ($constraint) {
                  $constraint->aspectRatio();
              });
              $img_3->resizeCanvas(400,400,'center', false, 'ffffff');
            }
            $img_3->save($otherpath.$name_3);
            $sql->image_3 = $name_3;
        }
        if (!empty($images_4)) {
            $img_4  = Image::make($images_4);
            $name_4 = '4-'.$slug.'.'.$images_4->getClientOriginalExtension();
            list($width, $height) = getimagesize($images_4);

            if( ($width > $height) || ($width == $height)){//landscape
              $img_4->resize(400, null, function ($constraint) {
                  $constraint->aspectRatio();
              });
              
              $img_4->resizeCanvas(400,400,'center', false, 'ffffff');
            } else {
              $img_4->resize(null,400, function ($constraint) {
                  $constraint->aspectRatio();
              });
              $img_4->resizeCanvas(400,400,'center', false, 'ffffff');
            }
            $img_4->save($otherpath.$name_4);
            $sql->image_4 = $name_4;
        }

        $sku = empty($request->sku) ? '' : strtoupper($request->sku);

        // // start to save to database
        $sql->name                     = ucwords($request->name);
        $sql->sku_number               = $sku;
        $sql->description              = $request->description;
        $sql->main_image               = $filename;
        $sql->thumbnail                = $filename;
        $sql->category_id              = $request->category;
        $sql->unit_price               = $request->harga;
        $sql->slug                     = $request->slug;
        $sql->discount                 = $request->discount;
        $sql->unit_weight              = $request->berat;
        $sql->available_size           = json_encode($request->size);
        $sql->available_colors         = json_encode($request->color);
        $sql->status_product_available = '1';
        $sql->product_type             = $request->product_type;
        $sql->status                   = 'new';

        if($sql->update()){
            return redirect('/admin/products')->with(['success' => 'Produk baru berhasil ditambahkan.']);
        } else {
            return redirect('/admin/products')->with(['error' => 'Produk baru gagal ditambahkan.']);                
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if(File::deleteDirectory(public_path().'/files/products/'.$product->slug))
        {
            $product->delete();
            return redirect('/admin/products')->with(['success' => 'Produk Berhasil Dihapus']);
        } else {
            return redirect('/admin/products')->with(['error' => 'Produk gagal dihapus']);
        }
    }

    /**
     * Removeall the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroyAll(Request $request)
    {
        $id = $request->input('id');
        if ($id != null) {
            $product = Product::whereIn('id', $id)->get();

            for ($i=0; $i < count($product); $i++) { 
                if(File::deleteDirectory(public_path().'/files/products/'.$product[$i]->slug))
                {
                    $byId = Product::find($product[$i]->id);
                    $byId->delete();
                }
            }
            return redirect('/admin/products/')->with(['success' => 'Produk berhasil dihapus']);    
        } else {
            return redirect('/admin/products/')->with(['info' => 'Silahkan pilih salah satu produk yang akan dihapus.']);    
        }
    }
}
