<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ProductCategory;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;
use File;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->q != null) {
            $category = ProductCategory::where('status','!=','remove')
                    ->where('name', 'like', "%{$request->q}%")
                    ->paginate(10);
        } else {
            $category = ProductCategory::where('status','!=','remove')->orderBy('created_at','desc')->paginate(10);            
        }

        return view('admin.products.list_category', ['category' => $category]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = ProductCategory::where('status','active')->get()->toArray();
        $tree = buildTree($category);
        return view('admin.products.create_category', ['tree' => $tree]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'  => 'required|unique:product_categories,name',
            'image'  => 'required|mimes:jpeg,jpg,png|max:2048',
            'image'  => 'dimensions:min_width=337,min_height=220',
            'status' => 'required'
        ];

        $message = [
            'uploaded'  => 'Kolom :attribute harus kurang dari :max kb.',
            'unique'    => 'Kolom :attribute sudah digunakan.',
            'max'       => 'Kolom :attribute harus kurang dari :max kb.',
            'required'  => 'Kolom :attribute tidak boleh kosong.',
            'mimes'     => 'Kolom :attribute harus berupa gambar dengan tipe: jpeg/jpg/png.',
            'dimensions'=> 'Dimensi :attribute minimal :min_width px x :min_height px.'
        ];

        $validator      = $request->validate($rules, $message);
        $slug           = str_slug($request->input('title'), '-').'-'.time();
        $img_page       = $request->file('image'); 
        $image_thumb    = Image::make($img_page);
        $filename       = 'thumbnails.'.$img_page->getClientOriginalExtension();
        $thumbnailpath  = public_path().'/files/category/'.$slug.'/';
        $sql            = new ProductCategory; 

        if(!File::isDirectory($thumbnailpath)){
            File::makeDirectory($thumbnailpath, 0777, true, true);
        } 

        list($width, $height) = getimagesize($img_page);
        if( ($width > $height) || ($width == $height) ){
          $image_thumb->resize(337, null, function ($constraint) {
              $constraint->aspectRatio();
          });
          
          $image_thumb->resizeCanvas(337,220,'center', false, 'eeeeee');
        } else {
          $image_thumb->resize(null,220, function ($constraint) {
              $constraint->aspectRatio();
          });
          $image_thumb->resizeCanvas(337,220,'center', false, 'eeeeee');
        }

        $image_thumb->save($thumbnailpath.$filename);

        $sql->name         = ucwords($request->title);
        $sql->slug         = $slug;
        $sql->description  = $request->description;
        $sql->parent_id    = $request->category;
        $sql->image        = $filename;
        $sql->status    = $request->status;

        if ($sql->save()) {
            return redirect('/admin/product/category')->with(['success' => 'Kategori produk berhasil ditambahkan.']);
        } else {
            return redirect('/admin/product/category')->with(['error' => 'Kategori produk gagal ditambahkan.']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = ProductCategory::find($id);
        $categoryAll = ProductCategory::where('status','active')
                        ->where('id','!=',$id)
                        ->get()->toArray();
        $tree = buildTree($categoryAll);
        return view('admin.products.edit_category', ['category' => $category,'tree' => $tree]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id     = $request->id;
        $rules = [
            'title'  => 'required|unique:product_categories,name,'.$id,
            'image'  => 'nullable|mimes:jpeg,jpg,png|max:2048',
            'image'  => 'dimensions:min_width=337,min_height=220',
            'status' => 'required'
        ];

        $message = [
            'uploaded'  => 'Kolom :attribute harus kurang dari :max kb.',
            'unique'    => 'Kolom judul sudah digunakan.',
            'max'       => 'Kolom :attribute harus kurang dari :max kb.',
            'required'  => 'Kolom :attribute tidak boleh kosong.',
            'mimes'     => 'Kolom :attribute harus berupa gambar dengan tipe: jpeg/jpg/png.',
            'dimensions'=> 'Dimensi :attribute minimal :min_width px x :min_height px.'
        ];

        $validator      = $request->validate($rules, $message);
        $slug           = $request->slug;
        $thumbnailpath  = public_path().'/files/category/'.$slug.'/';
        $sql            = ProductCategory::find($id);
        $img_page       = $request->file('image'); 

        if (!empty($img_page)) {
            $image_thumb    = Image::make($img_page);
            $filename       = 'thumbnails.'.$img_page->getClientOriginalExtension();

            list($width, $height) = getimagesize($img_page);

            if(($width > $height) || ($width == $height)) {
              $image_thumb->resize(337, null, function ($constraint) {
                  $constraint->aspectRatio();
              });
              
              $image_thumb->resizeCanvas(337,220,'center', false, 'eeeeee');
            } else {
              $image_thumb->resize(null,220, function ($constraint) {
                  $constraint->aspectRatio();
              });
              $image_thumb->resizeCanvas(337,220,'center', false, 'eeeeee');
            }

            $image_thumb->save($thumbnailpath.$filename);
        }

        $sql->name         = ucwords($request->title);
        $sql->slug         = $slug;
        $sql->description  = $request->description;
        $sql->parent_id    = $request->category;
        $sql->image        = $request->current_image;
        $sql->status       = $request->status;

        if ($sql->update()) {
            return redirect('/admin/product/category')->with(['success' => 'Kategori produk berhasil ditambahkan.']);
        } else {
            return redirect('/admin/product/category')->with(['error' => 'Kategori produk gagal ditambahkan.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkparent  = ProductCategory::where('parent_id',$id)->get();
        $checkproduct = Product::where('category_id',$id)->get();  
        $sql          = ProductCategory::where('id',$id)->get();           

        if ($checkparent->isEmpty() && $checkproduct->isEmpty()) {
            File::deleteDirectory(public_path().'/files/category/'.$sql[0]->slug);
            ProductCategory::where('id', $sql[0]->id)->delete();
            return redirect('/admin/product/category')->with(['success'=> 'Kategori produk berhasil dihapus']);
        } else {
            return redirect('/admin/product/category')->with(['info' => 'Kategori ini hanya bisa di ubah, silahkan pilih kembali kategori yang tidak dipakai !!!']);                    
        }
    }

    /**
     * Removeall the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroyAll(Request $request)
    {
        $id = $request->get('id');
        if ($id == null) {
            return redirect('/admin/product/category')->with(['info' => 'Silahkan pilih salah satu kategori yang akan dihapus.']);
        }

        $sql = DB::table('product_categories')->whereIn('id',$id)->get();
        
        foreach ($sql as $row)
        {
          $checkparent  = ProductCategory::where('parent_id',$row->id)->get();
          $checkproduct = Product::where('category_id',$row->id)->get();  

          if($checkparent->isEmpty() && $checkproduct->isEmpty()){
            File::deleteDirectory(public_path().'/files/category/'.$row->slug);
            ProductCategory::where('id', $row->id)->delete();
          } else {
            continue;
          }
        }
        return redirect('/admin/product/category')->with(['success' =>'Beberapa kategori mungkin tidak bisa dihapus, silahkan pilih kategori yang tidak dipakai.']);
    }
}
