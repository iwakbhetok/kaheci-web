<?php
 
namespace App\Http\Controllers\Admin; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banner;
use App\Logo;
use Image;
use File;
use Illuminate\Support\Str;

class GeneralController extends Controller
{
    public function index(Request $request){

        $top         = Banner::where('type','top')->get();
        $coming_soon = Banner::where('type','coming_soon')->get();
        $footer      = Logo::where('location','footer')->get();
        $header      = Logo::where('location','header')->get();

        return view('admin.general.form_general', 
            [
                'top'         => isset($top[0]) ? $top[0] :'',
                'coming_soon' => isset($coming_soon[0]) ? $coming_soon[0] :'',
                'footer'      => $footer, 
                'header'      => $header
            ]
        );
    }

    public function save_banner(Request $request){

        $id = $request->id;

        $message = [
            'unique'                  => 'Kolom :attribute sudah digunakan.',
            'required'                => 'Kolom :attribute tidak boleh kosong.',
            'text_url.min'            => 'Kolom text link min 4 karakter.',
            'name.max'                => 'Kolom judul max 50 karakter.',
            'text_url.max'            => 'Kolom text link max 20 karakter.',
            'url'                     => 'Kolom :attribute harus berupa url.',
            'image.dimensions'        => 'Dimensi gambar desktop minimal 600x1200px.',
            'image_mobile.dimensions' => 'Dimensi gambar mobile minimal 900x450px.',
            'uploaded'                => 'Size gambar terlalu besar.'
        ];

        $rules = [
            'description'   => 'required',
            'name'          => 'required|max:50|unique:banners,name,'.$id,
            'text_url'      => 'required|min:4|max:20',
            'url'           => 'required|url',
            'image'         => 'nullable|mimes:jpeg,jpg,png|max:2000',
            'image'         => 'dimensions:min_width=1200,min_height=600',
            'image_mobile'  => 'nullable|mimes:jpeg,jpg,png|max:1000',
            'image_mobile'  => 'dimensions:min_width=400,min_height=600',
        ];

        $validator     = $request->validate($rules, $message); 
        $image_desktop = $request->file('image'); 
        $image_mobile  = $request->file('image_mobile'); 
        $desktop       = public_path().'/images/banner/desktop/';   
        $mobile        = public_path().'/images/banner/mobile/';
        $sql           = Banner::find($id);

        if( is_null($sql) ) {
          $sql = new Banner;
        }

        if (!empty($image_desktop)) {
            if(File::exists($desktop.$request->curr_img_desktop)) {
               File::delete($desktop.$request->curr_img_desktop);
            }    
            $thumbDesktop   = Image::make($image_desktop);
            $desktopName    = 'header-banner.'.$image_desktop->getClientOriginalExtension();
            $thumbDesktop->resize(1350, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $thumbDesktop->save($desktop.$desktopName);
            $sql->image = $desktopName;
        } else {
            $sql->image = $request->curr_img_desktop;            
        }

        if (!empty($image_mobile)) {
            if(File::exists($mobile.$request->curr_img_mobile)) {
               File::delete($mobile.$request->curr_img_mobile);
            }    
            $thumbMobile    = Image::make($image_mobile);
            $mobilName      = 'header-banner.'.$image_mobile->getClientOriginalExtension();
            $thumbMobile->resize(450, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $thumbMobile->save($mobile.$mobilName);
            $sql->image_mobile = $mobilName;
        } else {
            $sql->image_mobile = $request->curr_img_mobile;            
        }

        $sql->name        = ucwords($request->name);
        $sql->description = $request->description;
        $sql->url         = $request->url;
        $sql->text_url    = ucwords($request->text_url);
        $sql->type        = $request->type;

        if ($sql->save()) {
                return redirect('/admin/general')->with(['success' => 'Banner header berhasil di ubah']);
        } else {
                return redirect('/admin/general')->with(['error' => 'Banner header gagal di ubah']);            
        }
    }


    public function save_prod_banner(Request $request){

        $id = $request->id;

        $message = [
            'unique'                               => 'Kolom :attribute sudah digunakan.',
            'required'                             => 'Kolom :attribute tidak boleh kosong.',
            'text_url.min'                         => 'Kolom text link min 4 karakter.',
            'name.max'                             => 'Kolom judul max 50 karakter.',
            'text_url.max'                         => 'Kolom text link max 20 karakter.',
            'url'                                  => 'Kolom :attribute harus berupa url.',
            'image_prod_banner_desktop.dimensions' => 'Dimensi gambar desktop minimal 400x800px.',
            'image_prod_banner_desktop.max'        => 'Gambar maksimal :max Kb.',
            'image_prod_banner_mobile.dimensions'  => 'Dimensi gambar mobile minimal 400x600 pixel.',
            'image_prod_banner_mobile.max'         => 'Gambar maksimal :max Kb.',
            'uploaded'                => 'Size gambar terlalu besar.'
        ];

        $rules = [
            'description'               => 'required',
            'name'                      => 'required|max:50|unique:banners,name,'.$id,
            'text_url'                  => 'required|min:4|max:20',
            'url'                       => 'required|url',
            'image_prod_banner_desktop' => 'nullable|mimes:jpeg,jpg,png|max:2000',
            'image_prod_banner_desktop' => 'dimensions:min_width=800,min_height=400',
            'image_prod_banner_mobile'  => 'nullable|mimes:jpeg,jpg,png|max:2000',
            'image_prod_banner_mobile'  => 'dimensions:min_width=600,min_height=400',
        ];

        $validator     = $request->validate($rules, $message); 
        $image_desktop = $request->file('image_prod_banner_desktop'); 
        $image_mobile  = $request->file('image_prod_banner_mobile'); 
        $desktop       = public_path().'/images/banner/desktop/';   
        $mobile        = public_path().'/images/banner/mobile/';
        $sql           = Banner::find($id);


        if( is_null($sql) ) {
          $sql = new Banner;
        }

        if (!empty($image_desktop)) {
            if(File::exists($desktop.$request->curr_img_prod_bann_desktop)) {
               File::delete($desktop.$request->curr_img_prod_bann_mobile);
            }    
            $thumbDesktop   = Image::make($image_desktop);
            $desktopName    = 'prod-header-banner.'.$image_desktop->getClientOriginalExtension();
            $thumbDesktop->resize(1350, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $thumbDesktop->save($desktop.$desktopName);
            $sql->image = $desktopName;
        } else {
            $sql->image = $request->curr_img_prod_bann_desktop;            
        }

        if (!empty($image_mobile)) {
            if(File::exists($mobile.$request->curr_img_mobile)) {
               File::delete($mobile.$request->curr_img_mobile);
            }    
            $thumbMobile    = Image::make($image_mobile);
            $mobilName      = 'prod-header-banner.'.$image_mobile->getClientOriginalExtension();
            $thumbMobile->resize(450, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $thumbMobile->save($mobile.$mobilName);
            $sql->image_mobile = $mobilName;
        } else {
            $sql->image_mobile = $request->curr_img_prod_bann_mobile;            
        }

        $sql->name        = ucwords($request->name);
        $sql->description = $request->description;
        $sql->url         = $request->url;
        $sql->text_url    = ucwords($request->text_url);
        $sql->type        = $request->type;

        if ($sql->save()) {
                return redirect('/admin/general')->with(['success' => 'Banner produk berhasil di ubah']);
        } else {
                return redirect('/admin/general')->with(['error' => 'Banner produk gagal di ubah']);
        }
    }


    public function save_logo_header(Request $request){

        $id = $request->id;

        $message = [
            'unique'                       => 'Kolom :attribute sudah digunakan.',
            'required'                     => 'Kolom :attribute tidak boleh kosong.',
            'image_logo_header.dimensions' => 'Dimensi logo minimal 150x100px.',
            'image_logo_header.max'        => 'Logo maksimal :max Kb.',
            'uploaded'                => 'Size gambar terlalu besar.'
        ];

        $rules = [
            'image_logo_header'  => 'nullable|mimes:jpeg,jpg,png|max:2000',
            'image_logo_header'  => 'dimensions:min_width=150,min_height=100',
        ];

        $validator     = $request->validate($rules, $message); 
        $image_desktop = $request->file('image_logo_header'); 
        $desktop       = public_path().'/images/logo/';   
        $sql           = Logo::find($id);

        if( is_null($sql) ) {
          $sql = new Logo;
        }

        if (!empty($image_desktop)) {
            if(File::exists($desktop.$request->image_logo_header)) {
               File::delete($desktop.$request->image_logo_header);
            }    
            $thumbDesktop   = Image::make($image_desktop);
            $desktopName    = 'logo-header.'.$image_desktop->getClientOriginalExtension();
            $thumbDesktop->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $thumbDesktop->save($desktop.$desktopName);
            $sql->image = $desktopName;
        } else {
            $sql->image = $request->current_image_header;            
        }

        $sql->location    = 'header';

        if ($sql->save()) {
                return redirect('/admin/general')->with(['success' => 'Logo header berhasil di ubah']);
        } else {
                return redirect('/admin/general')->with(['error' => 'Logo header gagal di ubah']);            
        }
    }

    public function save_logo_footer(Request $request){

        $id = $request->id;

        $message = [
            'unique'                       => 'Kolom :attribute sudah digunakan.',
            'required'                     => 'Kolom :attribute tidak boleh kosong.',
            'image_logo_footer.dimensions' => 'Dimensi logo minimal 150x100px.',
            'image_logo_footer.max'        => 'Logo maksimal :max Kb.',
            'uploaded'                => 'Size gambar terlalu besar.'
        ];

        $rules = [
            'description'        => 'required|max:300',
            'image_logo_footer'  => 'nullable|mimes:jpeg,jpg,png|max:2000',
            'image_logo_footer'  => 'dimensions:min_width=150,min_height=100',
        ];

        $validator     = $request->validate($rules, $message); 
        $image_desktop = $request->file('image_logo_footer'); 
        $desktop       = public_path().'/images/logo/';   
        $sql           = Logo::find($id);

        if( is_null($sql) ) {
          $sql = new Logo;
        }

        if (!empty($image_desktop)) {
            if(File::exists($desktop.$request->image_logo_footer)) {
               File::delete($desktop.$request->image_logo_footer);
            }    
            $thumbDesktop   = Image::make($image_desktop);
            $desktopName    = 'logo-footer.'.$image_desktop->getClientOriginalExtension();
            $thumbDesktop->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $thumbDesktop->save($desktop.$desktopName);
            $sql->image = $desktopName;
        } else {
            $sql->image = $request->current_image_footer;            
        }

        $sql->location       = 'footer';
        $sql->description    = $request->description;

        if ($sql->save()) {
                return redirect('/admin/general')->with(['success' => 'Logo footer berhasil di ubah']);
        } else {
                return redirect('/admin/general')->with(['error' => 'Logo footer gagal di ubah']);            
        }
    }
}
