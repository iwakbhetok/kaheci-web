<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->q != null) {
            $Menu = Menu::when($request->q, function ($query) use ($request) {
                $query->orwhere('name', 'like', "%{$request->q}%")
                      ->orWhere('url', 'like', "%{$request->q}%")
                      ->orWhere('location', 'like', "%{$request->q}%");
            })->paginate(10);
        } else {
            $Menu = Menu::orderBy('created_at','desc')->paginate(10);            
        }

        return view('admin.menu.list_menu', ['menu' => $Menu]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.menu.create_menu');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'         => 'required|unique:contact_support,name',
            'status'        => 'required',
            'url'           => 'required|url',
            'location'      => 'required'
        ];

        $message = [
            'unique'        => 'Kolom :attribute sudah digunakan.',
            'required'      => 'Kolom :attribute tidak boleh kosong.',
            'url'           => 'Kolom :attribute harus berupa url.'
        ];

        $validator     = $request->validate($rules, $message); 
        $sql           = new Menu;
        $sql->name     = ucwords($request->title);
        $sql->status   = $request->status;
        $sql->location = $request->location;
        $sql->url      = $request->url;
        $sql->row      = isset($request->row) ? $request->row : null;

        if ($sql->save()) {
            return redirect('/admin/menu')->with(['success' => 'Menu baru berhasil ditambahkan']);
        } else {
            return redirect('/admin/menu')->with(['error' => 'Menu baru gagal ditambahkan']);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $Menu
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res = DB::table('menus')
                ->select('*')
                ->where('id', $id)
                ->get();

        if (!$res->isEmpty()) {
            return view('admin.menu.edit_menu', ['menu' => $res[0]]);
        } else {
            return view('admin.components.404');                        
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $Menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id     = $request->input('id');

        $rules = [
            'title'         => 'required|unique:contact_support,name,'.$id,
            'status'        => 'required',
            'url'           => 'required|url',
            'location'      => 'required'
        ];

        $message = [
            'unique'        => 'Kolom :attribute sudah digunakan.',
            'required'      => 'Kolom :attribute tidak boleh kosong.',
            'url'           => 'Kolom :attribute harus berupa url.'
        ];

        $validator     = $request->validate($rules, $message); 
        $sql           = Menu::find($id);
        $sql->name     = ucwords($request->title);
        $sql->status   = $request->status;
        $sql->location = $request->location;
        $sql->url      = $request->url;
        $sql->row      = isset($request->row) ? $request->row : null;

        if ($sql->update()) {
            return redirect('/admin/menu')->with(['success' => 'Menu baru berhasil di ubah']);
        } else {
            return redirect('/admin/menu')->with(['error' => 'Menu baru gagal di ubah']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $Menu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sql = Menu::destroy('id', $id);

        if ($sql) {
            return redirect('/admin/menu')->with(['success' => 'Menu berhasil di hapus']);
        } else {
            return redirect('/admin/menu')->with(['error' => 'Menu gagal di hapus']);            
        }
    }

    /**
     * Removeall the specified resource from storage.
     *
     * @param  \App\Menu  $Menu
     * @return \Illuminate\Http\Response
     */
    public function destroyAll(Request $request)
    {
        if ($request->id != null) {
            $id     = $request->id;

            $sql = Menu::destroy($id);

            if ($sql) {
                return redirect('/admin/menu')->with(['success' => 'Menu berhasil di hapus']);
            } else {
                return redirect('/admin/menu')->with(['error' => 'Menu gagal di hapus']);            
            }
        } else {
            return redirect('/admin/menu')->with(['info' => 'Silahkan pilih salah satu yang akan dihapus.']);            
        }
    }
}
