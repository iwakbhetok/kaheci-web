<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Message;
use App\About;

class HomeController extends Controller
{
    public function __construct()
    {
        // $this->middleware('admin');
    }
    public function index(){
        return view('admin.login');
    }

    public function login()
    {
        return view('admin.login');
    }

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function showBlogs(){
        return view('admin.blank');
    }

    public function showAddBlog(){
        return view('admin.blank');
    }

    public function showCategoryBlog(){
        $blogs = array();
        return view('admin.blogs.list_category_blog', ['blogs' => $blogs]);
    }

    public function showAboutUs(){
        $about = About::first();
        return view('admin.about', ['about' => $about]);
    }

    public function showTestimonial(){
        $testimonials = array();
        return view('admin.testimonials.list', [ 'testimonials' => $testimonials ]);
    }

    public function addTestimonial(){
        return view('admin.testimonials.list');
    }

    public function showMessages($type = null)
    {
        switch ($type) {
            case 'inbox':
                $allMessage = Message::whereIn('status', ['new', 'read'])->paginate(20);
                $messages = Message::where('status', 'new')->paginate(10);
                $inbox_new = Message::where('status', 'new')->count();
                $trash = Message::where('status', 'remove')->count();
                return view('admin.messages.inbox', ['messages' => $messages, 'inbox' => $inbox_new, 'trash' => $trash, 'allMessage' => $allMessage]);
                break;

            case 'trash':
                $messages = Message::where('status', 'remove')->paginate(10);
                $inbox_new = Message::where('status', 'new')->count();
                $trash = Message::where('status', 'remove')->count();
                return view('admin.messages.trash', ['messages' => $messages, 'trash' => $trash, 'inbox' => $inbox_new]);
                break;
            
            default:
                $allMessage = Message::whereIn('status', ['new', 'read'])->paginate(20);
                $messages = Message::where('status', 'new')->paginate(10);
                $inbox_new = Message::where('status', 'new')->count();
                return view('admin.messages.inbox', ['messages' => $messages, 'inbox' => $inbox_new, 'allMessage' => $allMessage]);
                break;
        }
    }

    public function detailMessages($id){
        $message = Message::find($id);
        $changeStatus = Message::where('id', $id)->update(['status'=>'read']);
        return view('admin.messages.detail',['message' => $message]);
    }

    public function deleteMsg(Request $request){
        $messageIDs = $request->messageIDs;
        $result = Message::whereIn('id', $messageIDs)->update(['status' => 'remove']);
        $response = ($result != 0) ? 'success' : 'failed' ;
        return response()->json(array('msg'=> $response), 200);
    }

    public function showSettings(){
        return view('admin.setting.list_settings');
    }

    public function showProfile(){
        return view('admin.profile');
    }
}
