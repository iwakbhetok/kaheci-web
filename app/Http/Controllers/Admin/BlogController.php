<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Blog;
use App\BlogCategory;
use App\BlogTag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Image;
use File;
 
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->q != null) {
            $blogs = Blog::when($request->q, function ($query) use ($request) {
                $query->where('published', 1) 
                      ->orwhere('title', 'like', "%{$request->q}%")
                      ->orWhere('description', 'like', "%{$request->q}%");
            })->paginate(10);
        } else {
            $blogs = Blog::orderBy('created_at','desc')->paginate(10);            
        }

        return view('admin.blogs.list_blog', ['blogs' => $blogs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blogCategory = BlogCategory::where('status','1')->get();
        $blogTag      = BlogTag::where('status','1')->get();
        return view('admin.blogs.create_blog', ['blogCategory' => $blogCategory,'blogTag' => $blogTag]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'description'   => 'required',
            'title'         => 'required|unique:blogs,title',
            'category'      => 'required',
            'image'         => 'required|mimes:jpeg,jpg,png|max:2000',
            'image'         => 'dimensions:min_width=380,min_height=208',
            'status'        => 'required'
        ];

        $message = [
            'uploaded'            => 'Kolom :attribute harus kurang dari :max kb.',
            'unique'              => 'Kolom :attribute sudah digunakan.',
            'max'                 => 'Kolom :attribute harus kurang dari :max kb.',
            'required'            => 'Kolom :attribute tidak boleh kosong.',
            'image'               => 'Kolom :attribute harus berupa file gambar.',
            'dimensions'          => 'Dimensi :attribute minimal :min_width px x :min_height px.',
            'mimes'               => 'Kolom :attribute harus berupa gambar dengan tipe: jpeg/jpg/png.',
            'description.required'=> 'Kolom konten tidak boleh kosong.'
        ];


        $validator      = $request->validate($rules, $message); 
        $img_tags       = $request->file('image'); 
        $slug           = str_slug($request->input('title'), '-').'-'.time();
        $thumbimage     = Image::make($img_tags);
        $filename       = 'thumbnails.'.$img_tags->getClientOriginalExtension();
        $thumbpath      = public_path().'/images/blogs/'.$slug.'/';
        $path_otherimg  = '/blogs/'.$slug.'/';
        $sql            = new Blog;
        
        // check if directory not exist then create it
        if(!File::isDirectory($thumbpath)){
            File::makeDirectory($thumbpath, 0777, true, true);
        }

        list($width, $height) = getimagesize($img_tags);
        if( ($width > $height) || ($width == $height) ){
          $thumbimage->resize(380, null, function ($constraint) {
              $constraint->aspectRatio();
          });                  
          $thumbimage->resizeCanvas(380,208,'center', false, 'eeeeee');
        } else {
          $thumbimage->resize(null,208, function ($constraint) {
              $constraint->aspectRatio();
          });
          $thumbimage->resizeCanvas(380,208,'center', false, 'eeeeee');
        }
        
        $thumbimage->save($thumbpath.$filename);
        $detail                = summernote($request->description,$path_otherimg);
        $sql->title            = ucwords($request->title);
        $sql->slug             = $slug;
        $sql->description      = $detail;
        $sql->blog_category_id = $request->category;
        $sql->image            = $filename;
        $sql->admin_id         = $request->admin_id;
        $sql->tags             = json_encode($request->tag);
        $sql->published        = $request->status;

        if ($sql->save()) {
            return redirect('/admin/blogs')->with(['success' => 'Kategori blog berhasil Ditambahkan']);
        } else {
            return redirect('/admin/blogs')->with(['success' => 'Kategori blog gagal Ditambahkan']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res = DB::table('blogs')
                ->select(
                    'blogs.tags as tags',
                    'blogs.slug as slug',
                    'blogs.image as thumbnail',
                    'blogs.id as id',
                    'blogs.published as published',
                    'blogs.title as name_blog',
                    'blogs.description as description', 
                    'blogs.id as id_blog', 
                    'blogs.blog_category_id as id_category')
                ->join('blog_categories', 'blog_categories.id', '=', 'blogs.blog_category_id')
                ->where('blogs.id', $id)
                ->where('blog_categories.status','!=', 'remove')
                ->get();

        $BlogCategory   = BlogCategory::where('status', '1')->orderBy('name')->get();
        $BlogTag        = BlogTag::where('status', '1')->orderBy('name')->get();

        if (!$res->isEmpty()) {
            $tag = json_decode($res[0]->tags) == null ? [] :json_decode($res[0]->tags);
            return view('admin.blogs.edit_blog', ['blog' => $res[0],'BlogTag' => $BlogTag,'BlogCategory' => $BlogCategory, 'tagArr' => $tag ]);
        } else {
            return view('admin.components.404');                        
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id     = $request->id;
        $rules = [
            'description'   => 'required',
            'title'         => 'required|unique:blogs,title,'.$id,
            'category'      => 'required',
            'image'         => 'nullable|mimes:jpeg,jpg,png|max:2000',
            'image'         => 'dimensions:min_width=380,min_height=208',
            'status'        => 'required'
        ];

        $message = [
            'uploaded'            => 'Kolom :attribute harus kurang dari :max kb.',
            'unique'              => 'Kolom judul sudah digunakan.',
            'max'                 => 'Kolom :attribute harus kurang dari :max kb.',
            'required'            => 'Kolom :attribute tidak boleh kosong.',
            'image'               => 'Kolom :attribute harus berupa file gambar.',
            'dimensions'          => 'Dimensi :attribute minimal :min_width px x :min_height px.',
            'mimes'               => 'Kolom :attribute harus berupa gambar dengan tipe: jpeg/jpg/png.',
            'description.required'=> 'Kolom konten tidak boleh kosong.'
        ];


        $validator      = $request->validate($rules, $message); 
        $slug           = $request->slug;
        $thumbpath      = public_path().'/images/blogs/'.$slug.'/';
        $path_otherimg  = '/blogs/'.$slug.'/';
        $sql            = Blog::find($id);
        $img_tags       = $request->file('image'); 
        
        if (!empty($img_tags)) {
            $thumbimage     = Image::make($img_tags);
            $filename       = 'thumbnails.'.$img_tags->getClientOriginalExtension();
            if(File::exists($thumbpath.$request->current_image)) {
                File::delete($thumbpath.$request->current_image);
                list($width, $height) = getimagesize($img_tags);
                if( ($width > $height) || ($width == $height) ){
                  $thumbimage->resize(380, null, function ($constraint) {
                      $constraint->aspectRatio();
                  });                  
                  $thumbimage->resizeCanvas(380,208,'center', false, 'eeeeee');
                } else {
                  $thumbimage->resize(null,208, function ($constraint) {
                      $constraint->aspectRatio();
                  });
                  $thumbimage->resizeCanvas(380,208,'center', false, 'eeeeee');
                }

                $thumbimage->save($thumbpath.$filename);
                $sql->image = $filename;
            }    
        }

        $detail                = summernote($request->description,$path_otherimg);
        $sql->title            = ucwords($request->title);
        $sql->slug             = $slug;
        $sql->description      = $detail;
        $sql->blog_category_id = $request->category;
        $sql->admin_id         = $request->admin_id;
        $sql->tags             = json_encode($request->tag);
        $sql->published        = $request->status;

        if ($sql->update()) {
            return redirect('/admin/blogs')->with(['success' => 'Kategori blog berhasil Ditambahkan']);
        } else {
            return redirect('/admin/blogs')->with(['success' => 'Kategori blog gagal Ditambahkan']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::find($id);
        if(File::deleteDirectory(public_path().'/images/blogs/'.$blog->slug))
        {
            $blog->delete();
            return redirect('/admin/blogs')->with(['success' => 'Artikel berhasil dihapus']);    
        } else {
            return redirect('/admin/blogs')->with(['error' => 'Artikel gagal dihapus']);
        }
    }

    /**
     * Removeall the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroyAll(Request $request)
    {
        $id = $request->id;
        if ($id != null) {
            $blog = Blog::whereIn('id', $id)->get();

            for ($i=0; $i < count($blog); $i++) { 
                if(File::deleteDirectory(public_path().'/images/blogs/'.$blog[$i]->slug))
                {
                    $byId = Blog::find($blog[$i]->id);
                    $byId->delete();
                }
            }
            return redirect('/admin/blogs/')->with(['success' => 'Blog berhasil dihapus']);    
        } else {
            return redirect('/admin/blogs/')->with(['info' => 'Silahkan pilih salah satu artikel yang akan dihapus.']);    
        }
    }
}
