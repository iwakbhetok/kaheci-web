<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Image;
use File;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->q != null) {
            $testimonial = Testimonial::when($request->q, function ($query) use ($request) {
                $query->where('status', 1) 
                      ->orwhere('name', 'like', "%{$request->q}%")
                      ->orWhere('description', 'like', "%{$request->q}%");
            })->paginate(10);
        } else {
            $testimonial = Testimonial::orderBy('created_at','desc')->paginate(10);            
        }

        return view('admin.testimonials.list_testimonial', ['testimonial' => $testimonial]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonials.create_testimonial');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'      => 'required|unique:testimonials,name',
            'image'      => 'required|mimes:jpeg,jpg,png|max:2000',
            'image'      => 'dimensions:min_width=300,min_height=300',
            'status'     => 'required'
        ];

        $message = [
            'unique'    => 'Field :attribute sudah digunakan.',
            'max'       => 'Ukuran file gambar harus kurang dari :max kb.',
            'required'  => 'Field :attribute tidak boleh kosong.',
            'image'     => 'Field :attribute harus berupa gambar.',
            'dimensions'=> 'Dimensi file gambar minimal 300x533 px.',
            'mimes'     => 'Gambar harus berupa file dengan tipe: jpeg, jpg, png.'
        ];

        $validator        = $request->validate($rules, $message); 
        $img              = $request->file('image'); 
        $slug             = str_slug($request->title, '-').'-'.time();
        $thumbimage       = Image::make($img);
        $filename         = 'thumbnails.'.$img->getClientOriginalExtension();
        $thumbpath        = public_path().'/images/testimoni/'.$slug.'/';
        $sql              = new Testimonial;
        // check if directory not exist then create it
        if(!File::isDirectory($thumbpath)){
            File::makeDirectory($thumbpath, 0777, true, true);
        }

        list($width, $height) = getimagesize($img);

        if( $width > $height){//landscape
          $thumbimage->resize(300, null, function ($constraint) {
              $constraint->aspectRatio();
          });
          
          $thumbimage->resizeCanvas(300,533,'center', false, 'eeeeee');
        } else {
          $thumbimage->resize(null,533, function ($constraint) {
              $constraint->aspectRatio();
          });
          $thumbimage->resizeCanvas(300,533,'center', false, 'cccccc');
        }

        $thumbimage->save($thumbpath.$filename);
        $sql->name        = $request->title;
        $sql->slug        = $slug;
        $sql->description = $request->description;
        $sql->image       = $filename;
        $sql->status      = $request->status;

        if ($sql->save()) {
            return redirect('/admin/testimonial')->with(['success' => 'Testimoni berhasil ditambahkan']);
        } else {
            return redirect('/admin/testimonial')->with(['success' => 'Testimoni gagal Ditambahkan']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res = DB::table('testimonials')
                ->select('*')
                ->where('testimonials.id', $id)
                ->get();

        if (!$res->isEmpty()) {
            return view('admin.testimonials.edit_testimonial', ['testimonial' => $res[0]]);
        } else {
            return view('admin.components.404');                        
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pages  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id    = $request->id;
        $rules = [
            'title'      => 'required|unique:testimonials,name,'.$id,
            'image'      => 'nullable|mimes:jpeg,jpg,png|max:2000',
            'image'      => 'dimensions:min_width=300,min_height=300',
            'status'     => 'required'
        ];

        $message = [
            'unique'    => 'Field :attribute sudah digunakan.',
            'max'       => 'Ukuran file gambar harus kurang dari :max kb.',
            'required'  => 'Field :attribute tidak boleh kosong.',
            'image'     => 'Field :attribute harus berupa gambar.',
            'dimensions'=> 'File gambar lebar minimal :min_width px & tinggi minimal :min_height px',
            'mimes'     => 'Gambar harus berupa file dengan tipe: jpeg, jpg, png.'
        ];

        $validator        = $request->validate($rules, $message); 
        $img              = $request->file('image'); 
        $slug             = $request->slug;
        $thumbpath        = public_path().'/images/testimoni/'.$slug.'/';
        $sql              = Testimonial::find($id);

        if (!empty($img)) {
            $thumbimage   = Image::make($img);
            $filename     = 'thumbnails.'.$img->getClientOriginalExtension();
            if(File::exists($thumbpath.$request->current_image)) {
                File::delete($thumbpath.$request->current_image);
                list($width, $height) = getimagesize($img);

                if( $width > $height){//landscape
                  $thumbimage->resize(300, null, function ($constraint) {
                      $constraint->aspectRatio();
                  });
                  
                  $thumbimage->resizeCanvas(300,533,'center', false, 'eeeeee');
                } else {
                  $thumbimage->resize(null,533, function ($constraint) {
                      $constraint->aspectRatio();
                  });
                  $thumbimage->resizeCanvas(300,533,'center', false, 'cccccc');
                }
                $thumbimage->save($thumbpath.$filename);
                $sql->image = $filename;
            }    
        }

        $sql->name        = $request->title;
        $sql->slug        = $slug;
        $sql->description = $request->description;
        $sql->status      = $request->status;

        if ($sql->update()) {
            return redirect('/admin/testimonial')->with(['success' => 'Testimoni berhasil di update']);
        } else {
            return redirect('/admin/testimonial')->with(['success' => 'Testimoni gagal di update']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sql = Testimonial::find($id);

        if(File::deleteDirectory(public_path().'/images/testimoni/'.$sql->slug.'/'))
        {
            $sql->delete();
            return redirect('/admin/testimonial')->with(['success' => 'Testimoni berhasil dihapus.']);    
        } else {
            return redirect('/admin/testimonial')->with(['error' => 'Testimoni gagal dihapus']);
        }
    }

    /**
     * Removeall the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroyAll(Request $request)
    {
        $id = $request->id;
        if ($id != null) {
            $sql = Testimonial::whereIn('id', $id)->get();

            for ($i=0; $i < count($sql); $i++) { 
                if(File::deleteDirectory(public_path().'/images/testimoni/'.$sql[$i]->slug.'/'))
                {
                    $byId = Testimonial::find($sql[$i]->id);
                    $byId->delete();
                }
            }
            return redirect('/admin/testimonial/')->with(['success' => 'Testimoni berhasil dihapus']);    
        } else {
            return redirect('/admin/testimonial/')->with(['info' => 'Silahkan pilih salah satu testimoni yang akan dihapus.']);    
        }
    }
}
