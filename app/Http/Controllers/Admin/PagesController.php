<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Image;
use File;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->q != null) {
            $pages = Page::when($request->q, function ($query) use ($request) {
                $query->where('published', 1) 
                      ->orwhere('name', 'like', "%{$request->q}%")
                      ->orWhere('description', 'like', "%{$request->q}%");
            })->paginate(10);
        } else {
            $pages = Page::orderBy('created_at','desc')->paginate(10);            
        }

        return view('admin.pages.list_page', ['pages' => $pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create_page');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'description'      => 'required',
            'title'            => 'required|unique:pages,name',
            'image'            => 'required|mimes:jpeg,jpg,png|max:2000|dimensions:min_width=300',
            'status'           => 'required'
        ];

        $message = [
            'uploaded'         => 'Kolom :attribute harus kurang dari :max kb.',
            'unique'           => 'Kolom :attribute sudah digunakan.',
            'max'              => 'Kolom :attribute harus kurang dari :max kb.',
            'required'         => 'Kolom :attribute tidak boleh kosong.',
            'dimensions'       => 'Dimensi gambar minimal 300px x 300px.',
            'mimes'            => 'Kolom :attribute harus berupa gambar dengan tipe: jpeg/jpg/png.'
        ];

        $validator      = $request->validate($rules, $message); 
        $img_page       = $request->file('image'); 
        $slug           = str_slug($request->title, '-').'-'.time();
        $thumbimage     = Image::make($img_page);
        $filename       = 'thumbnails.'.$img_page->getClientOriginalExtension();
        $thumbpath      = public_path().'/images/pages/'.$slug.'/';
        $path_otherimg  = '/pages/'.$slug.'/';
        $sql            = new Page;
        
        // check if directory not exist then create it
        if(!File::isDirectory($thumbpath)){
            File::makeDirectory($thumbpath, 0777, true, true);
        }

        list($width, $height) = getimagesize($img_page);

        if(($width > $height) || ($width == $height)) {
          $thumbimage->resize(300, null, function ($constraint) {
              $constraint->aspectRatio();
          });
          $thumbimage->resizeCanvas(300,300,'center', false, 'eeeeee');
        } else {
          $thumbimage->resize(null,300, function ($constraint) {
              $constraint->aspectRatio();
          });
          $thumbimage->resizeCanvas(300,300,'center', false, 'eeeeee');
        }

        $thumbimage->save($thumbpath.$filename);

        $detail           = summernote($request->description,$path_otherimg);
        $sql->name        = ucwords($request->title);
        $sql->slug        = $slug;
        $sql->description = $detail;
        $sql->image       = $filename;
        $sql->admin_id    = $request->admin_id;
        $sql->published   = $request->status;

        if ($sql->save()) {
            return redirect('/admin/pages')->with(['success' => 'Halaman berhasil ditambahkan']);
        } else {
            return redirect('/admin/pages')->with(['error' => 'Testimoni gagal ditambahkan']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $pages
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res = DB::table('pages')
                ->select('*')
                ->where('pages.id', $id)
                ->get();

        if (!$res->isEmpty()) {
            return view('admin.pages.edit_page', ['pages' => $res[0]]);
        } else {
            return view('admin.components.404');                        
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id    = $request->id;
        $rules = [
            'description' => 'required',
            'title'       => 'required|unique:pages,name,'.$id,
            'thumbs'      => 'nullable|mimes:jpeg,jpg,png|max:2000|dimensions:min_width=300',
            'status'      => 'required'
        ];

        $message = [
            'unique'     => 'Kolom :attribute sudah digunakan.',
            'max'        => 'Kolom :attribute harus kurang dari :max kb.',
            'required'   => 'Kolom :attribute tidak boleh kosong.',
            'image'      => 'Kolom :attribute harus berupa file gambar.',
            'dimensions' => 'Dimensi gambar minimal 300px x 300px.',
            'mimes'      => 'Kolom :attribute harus berupa gambar dengan tipe: jpeg/jpg/png.'
        ];

        $validator      = $request->validate($rules, $message); 
        $img_page       = $request->file('thumbs'); 
        $slug           = $request->slug;
        $thumbpath      = public_path().'/images/pages/'.$slug.'/';
        $path_otherimg  = '/pages/'.$slug.'/';
        $sql            = Page::find($id);

        if (!empty($img_page)) {
            $thumbimage     = Image::make($img_page);
            $filename       = 'thumbnails.'.$img_page->getClientOriginalExtension();
            if(File::exists($thumbpath.$request->current_image)) {
                File::delete($thumbpath.$request->current_image);

                list($width, $height) = getimagesize($img_page);

                if(($width > $height) || ($width == $height)) {
                  $thumbimage->resize(300, null, function ($constraint) {
                      $constraint->aspectRatio();
                  });
                  $thumbimage->resizeCanvas(300,300,'center', false, 'eeeeee');
                } else {
                  $thumbimage->resize(null,300, function ($constraint) {
                      $constraint->aspectRatio();
                  });
                  $thumbimage->resizeCanvas(300,300,'center', false, 'eeeeee');
                }

                $thumbimage->save($thumbpath.$filename);
                $sql->image = $filename;
            }    
        }

        $detail           = summernote($request->description,$path_otherimg);
        $sql->name        = $request->title;
        $sql->slug        = $slug;
        $sql->description = $detail;
        $sql->admin_id    = $request->admin_id;
        $sql->published   = $request->status;

        if ($sql->update()) {
            return redirect('/admin/pages')->with(['success' => 'Halaman berhasil ditambahkan']);
        } else {
            return redirect('/admin/pages')->with(['error' => 'Halaman gagal ditambahkan']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $pages
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::find($id);
        if(File::deleteDirectory(public_path('/images/pages/'.$page->slug)))
        {
            $page->delete();
            return redirect('/admin/pages')->with(['success' => 'Halaman berhasil dihapus']);    
        } else {
            return redirect('/admin/pages')->with(['error' => 'Halaman gagal dihapus']);
        }
    }

    /**
     * Removeall the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroyAll(Request $request)
    {
        $id = $request->id;
        if ($id != null) {
            $page = Page::whereIn('id', $id)->get();

            for ($i=0; $i < count($page); $i++) { 
                if(File::deleteDirectory(public_path('/images/pages/'.$page[$i]->slug)))
                {
                    $byId = Page::find($page[$i]->id);
                    $byId->delete();
                }
            }
            return redirect('/admin/pages/')->with(['success' => 'Halaman berhasil dihapus']);    
        } else {
            return redirect('/admin/pages/')->with(['info' => 'Silahkan pilih salah satu artikel yang akan dihapus.']);    
        }
    }
}
