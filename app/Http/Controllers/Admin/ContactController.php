<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ContactSupport;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->q != null) {
            $ContactSupport = ContactSupport::when($request->q, function ($query) use ($request) {
                $query->orwhere('name', 'like', "%{$request->q}%")
                      ->orWhere('value', 'like', "%{$request->q}%")
                      ->orWhere('type', 'like', "%{$request->q}%");
            })->paginate(10);
        } else {
            $ContactSupport = ContactSupport::orderBy('created_at','desc')->paginate(10);
        }

        return view('admin.contact.list_contact_support', ['ContactSupport' => $ContactSupport]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.contact.create_contact_support');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'         => 'required',
            'status'        => 'required',
            'type'          => 'required'
        ];

        $message = [
            'required'      => 'Kolom :attribute tidak boleh kosong.'
        ];

        if ($request->type == 'wa') {
            $rules = array_merge(
                $rules,['value'=> 'required|regex:/^((?!\+)(62)[0-9]{9,13})/']);
            $message = array_merge(
                $message,['regex'=> 'No WA harus berupa angka dan di awali angka 62.']);
        } else {
            $rules = array_merge(
                $rules,['value'=> 'required|min:4']);
            $message = array_merge(
                $message,['min'=> 'Id line minimal :min karakter.']);
        }

        $validator   = $request->validate($rules, $message); 
        $sql         = new ContactSupport;
        $sql->name   = ucwords($request->title);
        $sql->value  = $request->value;
        $sql->type   = $request->type;
        $sql->status = $request->status;

        if ($sql->save()) {
            return redirect('/admin/contact_master')->with(['success' => 'Customer support baru berhasil ditambahkan']);
        } else {
            return redirect('/admin/contact_master')->with(['error' => 'Gagal menambahkan!!!']);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContactSupport  $ContactSupport
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res = DB::table('contact_support')
                ->select('*')
                ->where('id', $id)
                ->get();

        if (!$res->isEmpty()) {
            return view('admin.contact.edit_contact_support', ['ContactSupport' => $res[0]]);
        } else {
            return view('admin.components.404');                        
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactSupport  $ContactSupport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id     = $request->input('id');

        $rules = [
            'title'         => 'required',
            'status'        => 'required',
            'type'          => 'required'
        ];

        $message = [
            'required'      => 'Kolom :attribute tidak boleh kosong.'
        ];

        if ($request->type == 'wa') {
            $rules = array_merge(
                $rules,['value'=> 'required|regex:/^((?!\+)(62)[0-9]{9,13})/']);
            $message = array_merge(
                $message,['regex'=> 'No WA harus berupa angka dan di awali angka 62.']);
        } else {
            $rules = array_merge(
                $rules,['value'=> 'required|min:4']);
            $message = array_merge(
                $message,['min'=> 'Id line minimal :min karakter.']);
        }

        $validator   = $request->validate($rules, $message); 
        $sql         = ContactSupport::find($id);
        $sql->name   = ucwords($request->title);
        $sql->value  = $request->value;
        $sql->type   = $request->type;
        $sql->status = $request->status;

        if ($sql->update()) {
            return redirect('/admin/contact_master')->with(['success' => 'Customer support baru berhasil ditambahkan']);
        } else {
            return redirect('/admin/contact_master')->with(['error' => 'Gagal menambahkan!!!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactSupport  $ContactSupport
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sql = ContactSupport::find($id);

        if ($sql->delete()) {
            return redirect('/admin/contact_master')->with(['success' => 'Customer support berhasil di hapus']);
        } else {
            return redirect('/admin/contact_master')->with(['error' => 'Customer support gagal di hapus']);            
        }
    }

    /**
     * Removeall the specified resource from storage.
     *
     * @param  \App\ContactSupport  $ContactSupport
     * @return \Illuminate\Http\Response
     */
    public function destroyAll(Request $request)
    {
        if ($request->id != null) {
            $id     = $request->id;

            $sql = ContactSupport::whereIn('id',$id);

            if ($sql->delete()) {
                return redirect('/admin/contact_master')->with(['success' => 'Customer support berhasil di hapus']);
            } else {
                return redirect('/admin/contact_master')->with(['error' => 'Customer support gagal di hapus']);            
            }
        } else {
            return redirect('/admin/contact_master')->with(['info' => 'Silahkan pilih salah satu yang akan dihapus.']);            
        }
    }
}
