<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\BlogCategory;
use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;


class BlogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->q != null) {
            $BlogCategory = BlogCategory::when($request->q, function ($query) use ($request) {
                $query->where('status', 1) 
                      ->orwhere('name', 'like', "%{$request->q}%")
                      ->orWhere('description', 'like', "%{$request->q}%");
            })->paginate(10);
        } else {
            $BlogCategory = BlogCategory::orderBy('created_at','desc')->paginate(10);            
        }

        return view('admin.blogs.list_category_blog', ['blogCategory' => $BlogCategory]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.blogs.create_category_blog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'         => 'required|unique:blog_categories,name',
            'status'        => 'required'
        ];

        $message = [
            'unique'        => 'Kolom judul sudah digunakan.',
            'required'      => 'Kolom :attribute tidak boleh kosong.',
        ];

        $validator        = $request->validate($rules, $message); 
        $sql              = new BlogCategory;
        $sql->name        = ucwords($request->title);
        $sql->slug        = str_slug($request->title, '-').time();
        $sql->description = $request->description;
        $sql->status      = $request->status;

        if ($sql->save()) {
            return redirect('/admin/blog/category')->with(['success'=>'Halaman Baru Berhasil Ditambahkan.']);
        } else {
            return redirect('/admin/blog/category')->with(['error' => 'Halaman baru gagal ditambahkan']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res = DB::table('blog_categories')
                ->select('*')
                ->where('blog_categories.id', $id)
                ->get();

        if (!$res->isEmpty()) {
            return view('admin.blogs.edit_categories_blog', ['items' => $res[0]]);
        } else {
            return view('admin.components.404');                        
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id     = $request->input('id');

        $rules = [
            'title'       => 'required|unique:blog_categories,name,'.$id,
            'status'      => 'required'
        ];

        $message = [
            'unique'        => 'Kolom judul sudah digunakan.',
            'required'      => 'Kolom :attribute tidak boleh kosong.',
        ];

        $validator        = $request->validate($rules, $message);
        $sql              = BlogCategory::find($id);
        $sql->name        = ucwords($request->title);
        $sql->slug        = $request->slug;
        $sql->description = $request->description;
        $sql->status      = $request->status;

        
        if ($sql->update()) {
            return redirect('/admin/blog/category')->with(['success' => 'Kategori blog berhasil di update']);
        } else {
            return redirect('/admin/blog/category')->with(['error' => 'Kategori blog gagal di update']);            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkparent = BlogCategory::where('id',$id)->get();
        $check       = Blog::where('blog_category_id',$id)->get();    

        if ($checkparent->isEmpty() && $check->isEmpty()) {
            BlogCategory::where('id', $id)->delete();
            return redirect('/admin/blog/category')->with(['success' => 'Kategori blog berhasil di hapus']);
        } else {
            return redirect('/admin/blog/category')->with(['error' => 'Kategori ini hanya bisa di ubah, silahkan pilih kembali kategori yang tidak dipakai !!!']);            
        }
    }

    /**
     * Removeall the specified resource from storage.
     *
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function destroyAll(Request $request)
    {
        $id = $request->id;
        if ($id != null) {
            for ($i=0; $i < count($id); $i++) { 
                $check       = Blog::where('blog_category_id',$id[$i])->get();
                if (empty($check[0])) {
                    BlogCategory::where('id', $id[$i])->delete();
                } else {
                    return redirect('/admin/blog/category')->with(['info'=>'Beberapa kategori hanya bisa diubah, silahkan pilih kembali kategori yang tidak dipakai !!!']);
                }
            }
        } else {
            return redirect('/admin/blog/category')->with(['info' => 'Silahkan pilih salah satu kategori yang akan dihapus.']);
        }
        return redirect('/admin/product/category')->with(['success' => 'Beberapa kategori produk berhasil dihapus !!!']);    
    }
}
