<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\BlogTag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class BlogTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->q != null) {
            $BlogTag = BlogTag::when($request->q, function ($query) use ($request) {
                $query->where('status', 1) 
                      ->orwhere('name', 'like', "%{$request->q}%")
                      ->orWhere('description', 'like', "%{$request->q}%");
            })->paginate(10);
        } else {
            $BlogTag = BlogTag::orderBy('created_at','desc')->paginate(10);            
        }

        return view('admin.blogs.list_tag_blog', ['TagBlogs' => $BlogTag]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.blogs.create_tag_blog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'         => 'required|unique:blog_tags,name',
            'status'        => 'required'
        ];

        $message = [
            'unique'        => 'Kolom :attribute sudah digunakan.',
            'required'      => 'Kolom :attribute tidak boleh kosong.',
        ];

        $validator      = $request->validate($rules, $message); 
        $sql            = new BlogTag;
        $sql->name      = ucwords($request->title);
        $sql->slug      = str_slug($request->title, '-');
        $sql->status    = $request->status;            

        if ($sql->save()) {
            return redirect('/admin/blog/tag')->with(['success' => 'Tag baru berhasil ditambahkan']);
        } else {
            return redirect('/admin/blog/tag')->with(['success' => 'Tag baru gagal ditambahkan']);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlogTag  $blogTag
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res = DB::table('blog_tags')
                ->select('*')
                ->where('blog_tags.id', $id)
                ->get();

        if (!$res->isEmpty()) {
            return view('admin.blogs.edit_tag_blog', ['TagBlogs' => $res[0]]);
        } else {
            return view('admin.components.404');                        
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BlogTag  $blogTag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id     = $request->id;

        $rules = [
            'title'       => 'required|unique:blog_tags,name,'.$id,
            'status'      => 'required'
        ];

        $message = [
            'unique'        => 'Kolom :attribute sudah digunakan.',
            'required'      => 'Kolom :attribute tidak boleh kosong.',
        ];

        $validator      = $request->validate($rules, $message); 
        $sql            = BlogTag::find($id);
        $sql->name      = ucwords($request->title);
        $sql->slug      = str_slug($request->title, '-');
        $sql->status    = $request->status;            

        if ($sql->update()) {
            return redirect('/admin/blog/tag')->with(['success' => 'Tag baru berhasil di ubah']);
        } else {
            return redirect('/admin/blog/tag')->with(['success' => 'Tag baru gagal di ubah']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlogTag  $blogTag
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sql = BlogTag::destroy('id', $id);

        if ($sql) {
            return redirect('/admin/blog/tag')->with(['success' => 'Tag blog berhasil di hapus']);
        } else {
            return redirect('/admin/blog/tag')->with(['error' => 'Tag blog gagal di hapus']);            
        }
    }

    /**
     * Removeall the specified resource from storage.
     *
     * @param  \App\BlogTag  $blogTag
     * @return \Illuminate\Http\Response
     */
    public function destroyAll(Request $request)
    {
        if ($request->input('id') != null) {
            $id     = $request->input('id');

            $sql = BlogTag::destroy($id);

            if ($sql) {
                return redirect('/admin/blog/tag')->with(['success' => 'Tag blog berhasil di hapus']);
            } else {
                return redirect('/admin/blog/tag')->with(['error' => 'Tag blog gagal di hapus']);            
            }
        } else {
            return redirect('/admin/blog/tag')->with(['info' => 'Silahkan pilih salah satu tag yang akan dihapus.']);            
        }
    }
}
