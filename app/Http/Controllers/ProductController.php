<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($categoryName = null)
    {
        $paginate = 25;
        if($categoryName != null){
            $clean = ucwords(str_replace("-", ' ', $categoryName));
            $title = preg_replace('/\d+/u', '', $clean);

            $active = array(
                'index' => '',
                'products' => 'uk-active',
                'about' => '',
                'blogs' => '',
                'testimonial' => '',
                'reseller' => '',
                'contact' => '',
                'meta-title' => $title.' | Kaheci'
            );
            $menuCategory = ProductCategory::where(['status' => 'active'])->orderBy('name')->get();

            $category = ProductCategory::where(['slug' => $categoryName,'status' => 'active'])->first();
            $products = DB::table('products')
                        ->join('product_categories', 'products.category_id', '=', 'product_categories.id')
                        ->selectRaw('products.*, product_categories.name as category_name')
                        ->where('products.status_product_available', '1')
                        ->where('products.category_id', empty($category->id) ? "" : $category->id)
                        ->where('products.status','!=', 'remove')
                        ->paginate($paginate);
            $get_product_type = get_product_type();
            $get_product_color = get_product_color();
            
            if ($category == null) {
                abort(404);
            }

            return view('clients.products',['active' => $active, 'pageName' => 'Produk', 'breadcrumb' => 'Kategori produk', 'category' => empty($menuCategory) ? array() : $menuCategory, 'products' => $products, 'paginate' => $paginate, 'get_product_type' => $get_product_type, 'get_product_color' => $get_product_color]);
        }
        else{
            $active = array(
                'index' => '',
                'products' => 'uk-active',
                'about' => '',
                'blogs' => '',
                'testimonial' => '',
                'reseller' => '',
                'contact' => '',
                'meta-title' => 'Semua Produk Kaheci | Kaheci'
            );
            $products = DB::table('products')
                        ->join('product_categories', 'products.category_id', '=', 'product_categories.id')
                        ->selectRaw('products.*, product_categories.name as category_name')
                        ->where('products.status_product_available', '1')
                        ->where('products.status','!=', 'remove')
                        ->paginate($paginate);
            
            $get_product_type = get_product_type();
            $get_product_color = get_product_color();

            $menuCategory = ProductCategory::where(['status' => 'active'])->orderBy('name')->get();
            return view('clients.products',['active' => $active, 'pageName' => 'Produk', 'breadcrumb' => 'Kategori produk', 'category' => $menuCategory, 'products' => $products, 'paginate' => $paginate, 'get_product_type' => $get_product_type, 'get_product_color' => $get_product_color ]);
        }
    }

    public function show($slug)
    {
        $active = array(
            'index' => '',
            'products' => 'uk-active',
            'about' => '',
            'blogs' => '',
            'testimonial' => '',
            'reseller' => '',
            'contact' => '',
        );

        if($slug == null){
            abort(404);
        }else{
            $product = DB::table('products')
                        ->join('product_categories', 'products.category_id', '=', 'product_categories.id')
                        ->selectRaw('products.*, product_categories.name as category_name')
                        ->where('products.slug', $slug)
                        ->where('products.status','!=', 'remove')
                        ->first();
            if($product == null){
                abort(404);
            }

            $active = array_merge($active,['meta-title' => $product->name.' | Kaheci']);
            $relatedProduct = DB::table('products')
                        ->join('product_categories', 'products.category_id', '=', 'product_categories.id')
                        ->selectRaw('products.*, product_categories.name as category_name')
                        ->where('products.slug', '!=', $slug)
                        ->where('products.status','!=', 'remove')
                        ->where('category_id', $product->category_id)
                        ->limit(4)
                        ->get();
        }
        
        return view('clients.product-detail', ['active' => $active, 'pageName' => 'Produk', 'breadcrumb' => 'Detail produk', 'product' => $product, 'relatedProduct' => $relatedProduct]);
    }


}
